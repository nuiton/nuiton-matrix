/*
Copyright  1999 CERN - European Organization for Nuclear Research.
Permission to use, copy, modify, distribute and sell this software and its documentation for any purpose 
is hereby granted without fee, provided that the above copyright notice appear in all copies and 
that both that copyright notice and this permission notice appear in supporting documentation. 
CERN makes no representations about the suitability of this software for any purpose. 
It is provided "as is" without expressed or implied warranty.
%%Ignore-License
 */
package cern.jet.random;

import cern.jet.random.engine.RandomEngine;

/**
 * Uniform distribution; <A HREF=
 * "http://www.cern.ch/RD11/rkb/AN16pp/node292.html#SECTION0002920000000000000000"
 * > Math definition</A> and <A
 * HREF="http://www.statsoft.com/Textbook/Statistics-Glossary/U/button/u#Uniform%20Distribution">
 * animated definition</A>.
 * <p>
 * Instance methods operate on a user supplied uniform random number generator;
 * they are unsynchronized.
 * <dl>
 * <dt>
 * Static methods operate on a default uniform random number generator; they are
 * synchronized.</dt>
 * </dl>
 * <p>
 * 
 * @author wolfgang.hoschek@cern.ch
 * @version 1.0, 09/24/99
 */
public class Uniform extends AbstractContinousDistribution {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3494509643171209578L;
	protected double min;
	protected double max;

	// The uniform random number generated shared by all <b>static</b> methods.
	protected static Uniform shared = new Uniform(makeDefaultGenerator());

	/**
	 * Constructs a uniform distribution with the given minimum and maximum,
	 * using a {@link cern.jet.random.engine.MersenneTwister} seeded with the
	 * given seed.
	 */
	public Uniform(double min, double max, int seed) {
		this(min, max, new cern.jet.random.engine.MersenneTwister(seed));
	}

	/**
	 * Constructs a uniform distribution with the given minimum and maximum.
	 */
	public Uniform(double min, double max, RandomEngine randomGenerator) {
		setRandomGenerator(randomGenerator);
		setState(min, max);
	}

	/**
	 * Constructs a uniform distribution with <code>min=0.0</code> and
	 * <code>max=1.0</code>.
	 */
	public Uniform(RandomEngine randomGenerator) {
		this(0, 1, randomGenerator);
	}

	/**
	 * Returns the cumulative distribution function (assuming a continous
	 * uniform distribution).
	 */
	public double cdf(double x) {
		if (x <= min)
			return 0.0;
		if (x >= max)
			return 1.0;
		return (x - min) / (max - min);
	}

	/**
	 * Returns a uniformly distributed random <code>boolean</code>.
	 */
	public boolean nextBoolean() {
		return randomGenerator.raw() > 0.5;
	}

	/**
	 * Returns a uniformly distributed random number in the open interval
	 * <code>(min,max)</code> (excluding <code>min</code> and <code>max</code>).
	 */
	public double nextDouble() {
		return min + (max - min) * randomGenerator.raw();
	}

	/**
	 * Returns a uniformly distributed random number in the open interval
	 * <code>(from,to)</code> (excluding <code>from</code> and <code>to</code>). Pre
	 * conditions: <code>from &lt;= to</code>.
	 */
	public double nextDoubleFromTo(double from, double to) {
		return from + (to - from) * randomGenerator.raw();
	}

	/**
	 * Returns a uniformly distributed random number in the open interval
	 * <code>(from,to)</code> (excluding <code>from</code> and <code>to</code>). Pre
	 * conditions: <code>from &lt;= to</code>.
	 */
	public float nextFloatFromTo(float from, float to) {
		return (float) nextDoubleFromTo(from, to);
	}

	/**
	 * Returns a uniformly distributed random number in the closed interval
	 * <code>[min,max]</code> (including <code>min</code> and <code>max</code>).
	 */
	public int nextInt() {
		return nextIntFromTo((int) Math.round(min), (int) Math.round(max));
	}

	/**
	 * Returns a uniformly distributed random number in the closed interval
	 * <code>[from,to]</code> (including <code>from</code> and <code>to</code>). Pre
	 * conditions: <code>from &lt;= to</code>.
	 */
	public int nextIntFromTo(int from, int to) {
		return (int) ((long) from + (long) ((1L + (long) to - (long) from) * randomGenerator
				.raw()));
	}

	/**
	 * Returns a uniformly distributed random number in the closed interval
	 * <code>[from,to]</code> (including <code>from</code> and <code>to</code>). Pre
	 * conditions: <code>from &lt;= to</code>.
	 */
	public long nextLongFromTo(long from, long to) {
		/*
		 * Doing the thing turns out to be more tricky than expected. avoids
		 * overflows and underflows. treats cases like from=-1, to=1 and the
		 * like right. the following code would NOT solve the problem: return
		 * (long) (Doubles.randomFromTo(from,to));
		 * 
		 * rounding avoids the unsymmetric behaviour of casts from double to
		 * long: (long) -0.7 = 0, (long) 0.7 = 0. checking for overflows and
		 * underflows is also necessary.
		 */

		// first the most likely and also the fastest case.
		if (from >= 0 && to < Long.MAX_VALUE) {
			return from + (long) (nextDoubleFromTo(0.0, to - from + 1));
		}

		// would we get a numeric overflow?
		// if not, we can still handle the case rather efficient.
		double diff = ((double) to) - (double) from + 1.0;
		if (diff <= Long.MAX_VALUE) {
			return from + (long) (nextDoubleFromTo(0.0, diff));
		}

		// now the pathologic boundary cases.
		// they are handled rather slow.
		long random;
		if (from == Long.MIN_VALUE) {
			if (to == Long.MAX_VALUE) {
				// return Math.round(nextDoubleFromTo(from,to));
				int i1 = nextIntFromTo(Integer.MIN_VALUE, Integer.MAX_VALUE);
				int i2 = nextIntFromTo(Integer.MIN_VALUE, Integer.MAX_VALUE);
				return ((i1 & 0xFFFFFFFFL) << 32) | (i2 & 0xFFFFFFFFL);
			}
			random = Math.round(nextDoubleFromTo(from, to + 1));
			if (random > to)
				random = from;
		} else {
			random = Math.round(nextDoubleFromTo(from - 1, to));
			if (random < from)
				random = to;
		}
		return random;
	}

	/**
	 * Returns the probability distribution function (assuming a continous
	 * uniform distribution).
	 */
	public double pdf(double x) {
		if (x <= min || x >= max)
			return 0.0;
		return 1.0 / (max - min);
	}

	/**
	 * Sets the internal state.
	 */
	public void setState(double min, double max) {
		if (max < min) {
			setState(max, min);
			return;
		}
		this.min = min;
		this.max = max;
	}

	/**
	 * Returns a uniformly distributed random <code>boolean</code>.
	 */
	public static boolean staticNextBoolean() {
		synchronized (shared) {
			return shared.nextBoolean();
		}
	}

	/**
	 * Returns a uniformly distributed random number in the open interval
	 * <code>(0,1)</code> (excluding <code>0</code> and <code>1</code>).
	 */
	public static double staticNextDouble() {
		synchronized (shared) {
			return shared.nextDouble();
		}
	}

	/**
	 * Returns a uniformly distributed random number in the open interval
	 * <code>(from,to)</code> (excluding <code>from</code> and <code>to</code>). Pre
	 * conditions: <code>from &lt;= to</code>.
	 */
	public static double staticNextDoubleFromTo(double from, double to) {
		synchronized (shared) {
			return shared.nextDoubleFromTo(from, to);
		}
	}

	/**
	 * Returns a uniformly distributed random number in the open interval
	 * <code>(from,to)</code> (excluding <code>from</code> and <code>to</code>). Pre
	 * conditions: <code>from &lt;= to</code>.
	 */
	public static float staticNextFloatFromTo(float from, float to) {
		synchronized (shared) {
			return shared.nextFloatFromTo(from, to);
		}
	}

	/**
	 * Returns a uniformly distributed random number in the closed interval
	 * <code>[from,to]</code> (including <code>from</code> and <code>to</code>). Pre
	 * conditions: <code>from &lt;= to</code>.
	 */
	public static int staticNextIntFromTo(int from, int to) {
		synchronized (shared) {
			return shared.nextIntFromTo(from, to);
		}
	}

	/**
	 * Returns a uniformly distributed random number in the closed interval
	 * <code>[from,to]</code> (including <code>from</code> and <code>to</code>). Pre
	 * conditions: <code>from &lt;= to</code>.
	 */
	public static long staticNextLongFromTo(long from, long to) {
		synchronized (shared) {
			return shared.nextLongFromTo(from, to);
		}
	}

	/**
	 * Sets the uniform random number generation engine shared by all
	 * <b>static</b> methods.
	 * 
	 * @param randomGenerator
	 *            the new uniform random number generation engine to be shared.
	 */
	public static void staticSetRandomEngine(RandomEngine randomGenerator) {
		synchronized (shared) {
			shared.setRandomGenerator(randomGenerator);
		}
	}

	/**
	 * Returns a String representation of the receiver.
	 */
	public String toString() {
		return this.getClass().getName() + "(" + min + "," + max + ")";
	}
}
