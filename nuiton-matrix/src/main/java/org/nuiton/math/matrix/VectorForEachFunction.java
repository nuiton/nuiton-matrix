/*
 * #%L
 * Nuiton Matrix :: API
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.math.matrix;

/**
 * Methode appelé lorsqu'on veut parcourir un vecteur
 * On garanti que le parcours est total, mais par forcément dans l'ordre des éléments
 * c-a-d qu'on peut parcourir: 3, 1, 0, 8, ...
 *
 * Created: 16 fev. 2024
 *
 * @author Benjamin Poussin &lt;poussin@codelutin.com&gt;
 * @version $Revision$
 *
 * Mise a jour: $Date$
 * par : $Author$
 */
public interface VectorForEachFunction { // MapFunction

    /**
     * Appelé lors du parcours d'un vecteur
     *
     * @param position la position dans le vecteur (coordonnée linéaire)
     * @param value la valeur associée à la position
     */
    void apply(long position, double value);

} // MapFunction
