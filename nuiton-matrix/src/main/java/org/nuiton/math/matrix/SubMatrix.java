/*
 * #%L
 * Nuiton Matrix :: API
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.math.matrix;

import java.io.Serializable;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * Pour l'instant une sous matrice a obligatoirement le meme nombre de dimension
 * que la matrice qu'elle contient. Elle permet juste de reduire le nombre
 * d'element d'une dimension.
 * 
 * C'est comme une "vue" réduite sur la vraie matrices.
 * 
 * Created: 29 oct. 2004
 *
 * @author Benjamin Poussin &lt;poussin@codelutin.com&gt;
 * @version $Revision$
 *
 * Mise a jour: $Date$
 * par : $Author$
 */
public class SubMatrix extends AbstractMatrixND { // SubMatrix

    /** serialVersionUID. */
    private static final long serialVersionUID = 4092234115185263506L;

    protected MatrixND matrix = null;

    protected DimensionConverter converter = null;

    public SubMatrix(MatrixND matrix, int dim, int start, int nb) {
        super(matrix.getFactory(), matrix.getName(), matrix.getSemantics(),
                matrix.getDimensionNames());
        this.matrix = matrix;

        converter = new ShiftConverter(dim, start, nb);
        setSemantic(dim, getSemantic(dim).subList(start, start + nb));
        getDim()[dim] = nb;
    }

    public SubMatrix(MatrixND matrix, int dim, int[] elem) {
        super(matrix.getFactory(), matrix.getName(), matrix.getSemantics(),
                matrix.getDimensionNames());
        this.matrix = matrix;

        converter = new MappingConverter(dim, elem);

        List<?> oldSemantic = getSemantic(dim);
        List<Object> newSemantic = new LinkedList<Object>();
        for (int i = 0; i < elem.length; i++) {
            newSemantic.add(oldSemantic.get(elem[i]));
        }
        setSemantic(dim, newSemantic);
        getDim()[dim] = elem.length;
    }

    /**
     * return value of inner matrix, this is not realy a good choice, but it's
     * better than 0.
     *
     * @return
     */
    @Override
    public long getNumberOfAssignedValue() {
        return matrix.getNumberOfAssignedValue();
    }

    @Override
    public MatrixIterator iterator() {
        return new SubMatrixIteratorImpl(this);
    }

    @Override
    public MatrixIterator iteratorNotZero() {
        return new SubMatrixExcludeIteratorImpl(this, 0);
    }

    @Override
    public double getValue(int[] coordinates) {
        return matrix.getValue(converter.convertCoordinates(coordinates));
    }

    @Override
    public void setValue(int[] coordinates, double d) {
        matrix.setValue(converter.convertCoordinates(coordinates), d);
    }

    protected class SubMatrixIteratorImpl implements MatrixIterator {

        protected SubMatrix subMatrix = null;
        protected int[] cpt = null;
        protected int[] last = null;

        protected Object[] posSems;

        public SubMatrixIteratorImpl(SubMatrix subMatrix) {
            this.subMatrix = subMatrix;
            posSems = new Object[subMatrix.getDimCount()];

            cpt = new int[subMatrix.getDimCount()];
            cpt[cpt.length - 1] = -1;

            last = new int[subMatrix.getDimCount()];
            for (int i = 0; i < last.length; i++) {
                last[i] = subMatrix.getDim(i) - 1;
            }

        }

        @Override
        public boolean hasNext() {
            return !Arrays.equals(cpt, last);
        }

        @Override
        public boolean next() {
            boolean result = hasNext();
            int ret = 1;
            int[] dim = getDim();
            for (int i = cpt.length - 1; i >= 0; i--) {
                cpt[i] = cpt[i] + ret;
                ret = cpt[i] / dim[i];
                cpt[i] = cpt[i] % dim[i];
            }
            return result;
        }

        @Override
        public int[] getCoordinates() {
            return cpt;
        }

        @Override
        public Object[] getSemanticsCoordinates() {
            int[] coordinates = getCoordinates();
            Object[] result = MatrixHelper.dimensionToSemantics(posSems, subMatrix
                    .getSemantics(), coordinates);
            return result;
        }

        @Override
        public double getValue() {
            return subMatrix.getValue(getCoordinates());
        }

        @Override
        public void setValue(double value) {
            subMatrix.setValue(getCoordinates(), value);
        }

    }
    protected class SubMatrixExcludeIteratorImpl implements MatrixIterator {

        protected double exclude;
        protected SubMatrix subMatrix;
        protected int[] dim;

        protected int[] last;

        // utilise pour copier cpt avant de l'exposer vers l'exterieurs, pour
        // prevenir une modification par l'exterieur de cpt
        protected int[] coordinates;
        protected int[] cpt;
        protected int[] nextCpt;

        protected double value;
        protected double nextValue;

        protected Object[] posSems;

        public SubMatrixExcludeIteratorImpl(SubMatrix subMatrix, double exclude) {
            this.subMatrix = subMatrix;
            this.exclude = exclude;

            posSems = new Object[subMatrix.getDimCount()];

            dim = subMatrix.getDim();

            coordinates = new int[dim.length];
            cpt = new int[dim.length];
            cpt[cpt.length - 1] = -1;

            nextCpt = new int[dim.length];
            nextCpt[nextCpt.length - 1] = -1;

            last = new int[dim.length];
            for (int i = 0; i < last.length; i++) {
                last[i] = dim[i] - 1;
            }

        }

        /**
         * retourne vrai si cpt1 est superieur ou egal a cpt2, on suppose que cpt1 et
         * cpt2 represente la meme chose (meme dimension, meme valeur max)
         * @param cpt1
         * @param cpt2
         * @return 
         */
        protected boolean arrayGreaterOrEquals(int[] cpt1, int[] cpt2) {
            boolean result = true;
            for (int i=0, max=cpt1.length; result && i<max; i++) {
                result = cpt1[i] >= cpt2[i];
            }
            return result;
        }

        protected void computeNextCpt() {
            int ret = 1;
            for (int i = nextCpt.length - 1; i >= 0; i--) {
                nextCpt[i] = nextCpt[i] + ret;
                ret = nextCpt[i] / dim[i];
                if (i != 0) {
                    // last must be keep, otherwize infinite loop (all are set to 0)
                    nextCpt[i] = nextCpt[i] % dim[i];
                }
            }
        }

        protected void computeNextPosAndValue() {
            if (arrayGreaterOrEquals(cpt, nextCpt)) {
                boolean validPos;
                do {
                    computeNextCpt();
                    validPos = arrayGreaterOrEquals(last, nextCpt);
                    if (validPos) {
                        nextValue = subMatrix.getValue(nextCpt);
                    }
                } while (validPos && nextValue == exclude);
            }
        }

        @Override
        public boolean hasNext() {
            computeNextPosAndValue();
            return arrayGreaterOrEquals(last, nextCpt);
        }

        @Override
        public boolean next() {
            boolean result = hasNext();
            System.arraycopy(nextCpt, 0, cpt, 0, nextCpt.length);
            value = nextValue;
            return result;

        }

        @Override
        public int[] getCoordinates() {
            System.arraycopy(cpt, 0, coordinates, 0, cpt.length);
            return coordinates;
        }

        @Override
        public Object[] getSemanticsCoordinates() {
            Object[] result = MatrixHelper.dimensionToSemantics(posSems, subMatrix
                    .getSemantics(), cpt);
            return result;
        }

        @Override
        public double getValue() {
            return value;
        }

        @Override
        public void setValue(double value) {
            this.value = value;
            subMatrix.setValue(cpt, value);
        }

    }

    /**
     * Permet de faire une conversion de la dimension demandé dans la sous
     * matrice avec la position reel de la matrice sous jacente.
     */
    protected interface DimensionConverter extends Serializable {
        public int[] convertCoordinates(int[] coordinates);
    }

    /**
     * La conversion est juste un decalage d'indice
     */
    protected static class ShiftConverter implements DimensionConverter {
        /** serialVersionUID. */
        private static final long serialVersionUID = 1L;

        protected int dim;
        protected int start;
        protected int nb;

        public ShiftConverter(int dim, int start, int nb) {
            this.dim = dim;
            this.start = start;
            this.nb = nb;
        }

        @Override
        public int[] convertCoordinates(int[] coordinates) {
            int[] result = null;
            if (coordinates[dim] < nb) {
                result = new int[coordinates.length];
                System.arraycopy(coordinates, 0, result, 0, result.length);
                result[dim] = result[dim] + start;
            } else {
                throw new NoSuchElementException(
                        "L'indice est supérieur au nombre d'élement de la sous matrice pour cette dimension.");
            }
            return result;
        }
    }

    /**
     * La conversion est le mapping d'un element vers un autre element.
     */
    protected static class MappingConverter implements DimensionConverter {

        /** serialVersionUID. */
        private static final long serialVersionUID = -6367416559713556559L;

        protected int dim;
        protected int[] elem = null;

        public MappingConverter(int dim, int[] elem) {
            this.dim = dim;
            this.elem = new int[elem.length];
            System.arraycopy(elem, 0, this.elem, 0, elem.length);
        }

        @Override
        public int[] convertCoordinates(int[] coordinates) {
            int[] result = null;
            if (coordinates[dim] < elem.length) {
                result = new int[coordinates.length];
                System.arraycopy(coordinates, 0, result, 0, result.length);
                result[dim] = elem[coordinates[dim]];

            } else {
                throw new NoSuchElementException(
                        "L'indice est supérieur au nombre d'élements de la sous matrice pour cette dimension.");
            }
            return result;
        }
    }

} // SubMatrix
