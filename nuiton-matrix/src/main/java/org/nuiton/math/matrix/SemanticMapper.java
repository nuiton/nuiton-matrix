/*
 * #%L
 * Nuiton Matrix :: API
 * %%
 * Copyright (C) 2011 Codelutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.math.matrix;

/**
 * Mapper used during import/export to map CSV file semantics to
 * real semantics value depending on execution context.
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class SemanticMapper {

    /**
     * Return class for type identified by typeName.
     * 
     * For example : "Population" can return "fr.ifremer.entities.Population.class"
     * 
     * Return {@code String} by default.
     * 
     * @param typeName type to get class.
     * @return type for typeId
     */
    public Class getType(String typeName) {
        return String.class;
    }

    /**
     * Return value identified by valueId and type {@code type}.
     * 
     * Return {@code valueId} by default;
     * 
     * @param type
     * @param valueId
     * @return value identified by {valueId}
     */
    public Object getValue(Class type, String valueId) {
        return valueId;
    }

    /**
     * Return type name for given type.
     * 
     * @param type type to get name
     * @return type name
     */
    public String getTypeName(Object type) {
        return type.getClass().getName();
    }

    /**
     * Get value id for value.
     * 
     * @param value value to get id
     * @return value id
     */
    public String getValueId(Object value) {
        return value.toString();
    }
}
