/*
 * #%L
 * Nuiton Matrix :: API
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.math.matrix;

import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * MatriceEncoder.
 *
 * Created: 21 oct. 2004
 *
 * @author Benjamin Poussin &lt;poussin@codelutin.com&gt;
 * @version $Revision$
 *
 * Mise a jour: $Date$
 * par : $Author$
 */
public class MatrixEncoder { // MatriceEncoder

    protected Writer out = null;

    public MatrixEncoder(Writer out) {
        this.out = out;
    }

    /**
     * Methode qui peut etre surcharge par les enfants Elle sert a mettre sous
     * forme XML les objets de semantiques Par defaut cette méthode converti en
     * représentation chaine les objets Un objet deviendra donc une chaine de
     * caractere lors de la lecture de la matrice à partir du XML si cette
     * méthode n'est pas surchargée. Le seul objet convenablement supporté sont
     * les représentation objet des types primitifs.
     * 
     * @param o 
     * @throws IOException
     * @return semntics as xml
     */
    protected String getSemanticsAsXml(Object o) throws IOException {
        String xml = null;
        
        // on ne fait rien car la valeur par defaut lorsqu'on relit
        // la matrice est null
        if (o != null) {
            // par defaut si on ne sait pas comment mettre en XML un objet
            // on dit qu'il est de type String et on appelle la methode
            // toString sur l'objet
            // A la relecture l'objet sera donc une string
            String type = String.class.getName();
            if (o instanceof Number || o instanceof Boolean) {
                type = o.getClass().getName();
            }
            xml = "<object type=\"" + type + "\">" + o.toString()
                    + "</object>";
        }
        return xml;
    }

    public void writeMatrice(MatrixND mat) throws IOException {
        // l'element que l'on defini comme element par defaut est celui
        // que l'on retrouve le plus souvent
        double defaultValue = mat.getMaxOccurrence();

        out.write("<matrix defaultValue=\"" + defaultValue + "\" name=\""
                + mat.getName() + "\" dimensions=\"");
        out.write(MatrixHelper.coordinatesToString(mat.getDim()));
        out.write("\" type=\"" + mat.getClass().getName() + "\">\n");

        // ecriture des noms des dimensions
        for (int i = 0; i < mat.getDimCount(); i++) {
            String dimName = mat.getDimensionName(i);
            // si le nom est non null et non vide on l'ecrit,
            // sinon c la valeur par defaut lorsqu'on relit la matrice
            // donc ca ne sert a rien de le mettre dans le XML
            if (dimName != null && !"".equals(dimName)) {
                out.write("  <dimensionName dim=\"" + i + "\">");
                out.write(mat.getDimensionName(i));
                out.write("</dimensionName>\n");
            }
        }

        // ecriture de la semantique
        for (int i = 0; i < mat.getDimCount(); i++) {
            List<?> sem = mat.getSemantic(i);
            List<Object> semAsXml = new ArrayList<Object>();
            boolean haveNotNull = false;
            for (Iterator<?> e = sem.iterator(); e.hasNext();) {
                String xml = getSemanticsAsXml(e.next());
                semAsXml.add(xml);
                haveNotNull = haveNotNull || xml != null;
            }
            if (haveNotNull) {
                out.write("  <semantics dim=\"" + i + "\">\n");
                for (int index = 0; index < semAsXml.size(); index++) {
                    String xml = (String) semAsXml.get(index);
                    if (xml != null) {
                        out.write("    <sem index=\"" + index + "\">" + xml
                                + "</sem>\n");
                    }
                }
                out.write("  </semantics>\n");
            }
        }

        // ecriture des valeur de la matrice
        for (MatrixIterator i = mat.iterator(); i.next();) {
            double val = i.getValue();
            if (val != defaultValue) {
                String path = MatrixHelper.coordinatesToString(i
                        .getCoordinates());
                out.write("\t<element path=\"" + path + "\" value=\"" + val
                        + "\"/>\n");
            }
        }

        out.write("</matrix>\n");
    }

} // MatriceEncoder

