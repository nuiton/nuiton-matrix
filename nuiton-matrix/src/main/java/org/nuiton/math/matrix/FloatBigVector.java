/*
 * #%L
 * Nuiton Matrix :: API
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.math.matrix;

import java.util.Arrays;

/**
 * FloatBigVector.
 *
 * Created: 6 octobre 2005 02:54:36 CEST
 *
 * @author Benjamin POUSSIN &lt;poussin@codelutin.com&gt;
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class FloatBigVector implements Vector { // FloatBigVector

    protected float[] data = null;

    public FloatBigVector() {
    }

    public FloatBigVector(int capacity) {
        init(capacity);
    }

    @Override
    public void init(long capacity) {
        if (capacity > Integer.MAX_VALUE) {
            throw new IllegalArgumentException("FloatBigVector don't support this capacity : " + capacity);
        }
        if (data == null) {
            data = new float[(int) capacity];
        }
    }

    @Override
    public String getInfo() {
        return "Float vector dense: " + size();
    }

    @Override
    public long getNumberOfAssignedValue() {
        return size();
    }

    @Override
    public long size() {
        return data.length;
    }

    @Deprecated
    @Override
    public double getMaxOccurence() {
        return getMaxOccurrence();
    }
    
    @Override
    public double getMaxOccurrence() {
        return MatrixHelper.maxOccurrence(data);
    }

    @Override
    public double getValue(long pos) {
        return data[(int) pos];
    }

    @Override
    public void setValue(long pos, double value) {
        data[(int) pos] = (float) value;
    }

    @Override
    public boolean equals(Object o) {
        boolean result = false;
        if (o instanceof FloatBigVector) {
            FloatBigVector other = (FloatBigVector) o;
            result = Arrays.equals(this.data, other.data);
        } else if (o instanceof Vector) {
            Vector other = (Vector) o;
            result = true;
            for (int i = 0; i < size() && result; i++) {
                result = getValue(i) == other.getValue(i);
            }
        }
        return result;
    }

    @Override
    public boolean isImplementedPaste(Vector v) {
        return v instanceof FloatBigVector;
    }

    @Override
    public boolean isImplementedMap() {
        return true;
    }

    @Override
    public void paste(Vector v) {
        FloatBigVector fbv = (FloatBigVector) v;
        System.arraycopy(fbv.data, 0, this.data, 0, (int)this.size());
    }

    @Override
    public void map(MapFunction f) {
        for (int i = 0; i < data.length; i++) {
            data[i] = (float) f.apply(data[i]);
        }
    }

    @Override
    public VectorIterator iterator() {
        return new VectorIteratorImpl(this);
    }

    @Override
    public VectorIterator iteratorNotZero() {
            return new VectorIteratorImpl(this, 0);
    }

} // FloatBigVector

