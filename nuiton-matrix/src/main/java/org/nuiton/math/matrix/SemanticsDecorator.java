/*
 * #%L
 * Nuiton Matrix :: API
 * %%
 * Copyright (C) 2012 CodeLutin, Poussin Benjamin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.math.matrix;

/**
 *
 * Permet de convertir automatiquement une semantics vers une autre representation
 * Par exemple d'une representation String vers une Entity
 *
 * @param <Decorated> type use for decorated value (often Object)
 * @param <Undecorated> type use for undecorated value (often String)
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public interface SemanticsDecorator<Decorated, Undecorated> {
    /**
     * Decore la valeur de la semantique
     * @param internalValue la valeur interne non decoree
     * @return la valeur decoree
     */
    public Decorated decorate(Undecorated internalValue);
    /**
     * supprime la decoration de la valeur en argument
     * @param decoratedValue la valeur decoree
     * @return la valeur decoree
     */
    public Undecorated undecorate(Decorated decoratedValue);

}
