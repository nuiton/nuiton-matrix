/*
 * #%L
 * Nuiton Matrix :: API
 * %%
 * Copyright (C) 2004 - 2020 CodeLutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.math.matrix;

import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.util.List;

/**
 * MatrixND interface for different matrix implementation.
 *
 * Created: 29 oct. 2004
 *
 * @author Benjamin Poussin &lt;poussin@codelutin.com&gt;
 * @version $Revision$
 *
 * Mise a jour: $Date$
 * par : $Author$
 */
public interface MatrixND extends Serializable, Cloneable { // MatrixND

    /**
     * Retourne la factory qui a permit de creer la matrice.
     *
     * @return la {@link MatrixFactory}
     *
     * @see MatrixFactory
     */
    public MatrixFactory getFactory();

    /**
     * Returne number of assigned value, assigned value is value stored in memory
     *
     * @since 2.4.2
     */
    public long getNumberOfAssignedValue();

    /**
     * Donne toutes les semantiques de la matrice.
     *
     * Si la matrice n'a pas de semantique retourne null.
     *
     * @return la liste des semantics
     */
    public List[] getSemantics();

    /**
     * Retourne la semantique pour une dimension.
     *
     * @param dim la dimension pour lequel on veut la semantique
     * @return la semantique de la dimension on null s'il n'y a pas de
     *         semantique
     *         
     * @deprecated (since 1.0.3) use #getSemantic(int) instead
     */
    @Deprecated
    public List getSemantics(int dim);

    /**
     * Retourne la semantique pour une dimension.
     *
     * @param dim la dimension pour lequel on veut la semantique
     * @return la semantique de la dimension on null s'il n'y a pas de
     *         semantique
     */
    public List getSemantic(int dim);

    /**
     * Modifie la semantique d'une dimension.
     * 
     * @param <E> data type
     * @param dim dimension to modify semantic
     * @param sem new semantic to set
     * 
     * @deprecated (since 1.0.3) use #setSemantic(int, List) instead
     */
    @Deprecated
    public <E> void setSemantics(int dim, List<E> sem);

    /**
     * Modifie la semantique d'une dimension.
     * 
     * @param <E> data type
     * @param dim dimension to modify semantic
     * @param sem new semantic to set
     * @since 1.0.3
     */
    public <E> void setSemantic(int dim, List<E> sem);

    /**
     * Permet de donner un nom à la matrice.
     *
     * @param name name to set
     */
    public void setName(String name);

    /**
     * Retourne le nom de la matrice.
     *
     * @return le nom de la matrice ou la chaine vide si pas de nom.
     */
    public String getName();

    /**
     * Permet de mettre des noms aux différentes dimension.
     *
     * @param names names to set
     * @deprecated since 1.0.3, use {@link #setDimensionNames(String[])} instead
     */
    @Deprecated
    public void setDimensionName(String[] names);

    /**
     * Permet de mettre des noms aux différentes dimension.
     *
     * @param names names to set
     * @since 1.0.3
     */
    public void setDimensionNames(String[] names);

    /**
     * Permet de recuperer les noms des dimension.
     *
     * @return tableau des noms de dimension.
     *
     * @deprecated since 1.0.3, use {@link #getDimensionNames()} instead
     */
    @Deprecated
    public String[] getDimensionName();

    /**
     * Permet de recuperer les noms des dimension.
     *
     * @return tableau des noms de dimension.
     * @since 1.0.3
     */
    public String[] getDimensionNames();

    /**
     * Permet de mettre un nom à une dimension.
     *
     * @param dim la dimension dont on veut changer le nom
     * @param name le nom à donner à la dimension
     */
    public void setDimensionName(int dim, String name);

    /**
     * Retourne le nom de la dimension demandée.
     *
     * @param dim la dimension dont on veut le nom
     * @return le nom de la dimension ou la chaine vide si la dimension n'a pas
     *         de nom @ si la dimension demandé n'est pas valide
     */
    public String getDimensionName(int dim);

    /**
     * Retourne la valeur la plus couramment rencontree dans un tableau. Si
     * plusieurs valeurs ont le même nombre d'occurrence la plus petite valeur
     * est retourné.
     *
     * @return la valeur la plus nombreuse dans le tableau
     * 
     * @deprecated since 2.1, use {@link #getMaxOccurrence} instead
     */
    @Deprecated
    public double getMaxOccurence();
    
    /**
     * Retourne la valeur la plus courrement rencontrer dans un tableau. Si
     * plusieurs valeurs ont le même nombre d'occurrence la plus petite valeur
     * est retourné.
     *
     * @return la valeur la plus nombreuse dans le tableau
     * @since 2.1
     */
    public double getMaxOccurrence();

    /**
     * Retourne le nombre de dimensions de la matrice.
     * 
     * @return le nombre de dimensions de la matrice.
     * @deprecated since 1.0.3, use {@link #getDimCount()} instead
     */
    public int getNbDim();

    /**
     * Retourne le nombre de dimensions de la matrice.
     * 
     * @return le nombre de dimensions de la matrice.
     * @since 1.0.3
     */
    public int getDimCount();

    /**
     * Retourne les dimensions de la matrice.
     * 
     * @return matrix dimension
     */
    public int[] getDim();

    /**
     * Retourne la dimension de la matrice dans la dimension d.
     * 
     * @param d dimension
     * @return matrix dimension
     */
    public int getDim(int d);

    /**
     * Retourne le nombre d'element dans la matrice [2, 3, 5] donnera 30
     *
     * @return le nombre d'element dans la matrice
     */
    public long size();

    /**
     * Retourne un iterator sur toute la matrice.
     *
     * @return matrix iterator
     */
    public MatrixIterator iterator();

    /**
     * Retourne un iterator pour toutes les valeurs de la matrices differente
     * de 0
     *
     * @return matrix iterator
     */
    public MatrixIterator iteratorNotZero();

    /**
     * Applique une fonction sur chaque valeur de la matrice.
     * 
     * @param f function to apply
     * @return {@code this}
     */
    public MatrixND map(MapFunction f);

    /**
     * Renvoie un element de la matrice demandée en fonction des dimensions
     * passé en paramètre.<br>
     *
     * Exemple: Si on a un matrice 3D.<br>
     * getValue(1,1,1) retourne un element de la matrice.<br>
     *
     * @param dimensions les différentes dimension à extraire. Le tableau doit
     *            contenir toutes les dimensions de la matrice, et seulement des
     *            nombres positif
     *
     * @return un entier double.
     */
    public double getValue(int[] dimensions);

    /**
     * Return a 1D matrix value.
     *
     * @param x first dimension index
     *
     * @return double value at specified dimensions
     */
    public double getValue(int x);

    /**
     * Return a 2D matrix value.
     *
     * @param x first dimension index
     * @param y second dimension index
     *
     * @return double value at specified dimensions
     */
    public double getValue(int x, int y);

    /**
     * Return a 3D matrix value.
     *
     * @param x first dimension index
     * @param y second dimension index
     * @param z third dimension index
     *
     * @return double value at specified dimensions
     */
    public double getValue(int x, int y, int z);

    /**
     * Return a 4D matrix value.
     *
     * @param x first dimension index
     * @param y second dimension index
     * @param z third dimension index
     * @param t fourth dimension index
     *
     * @return double value at specified dimensions
     */
    public double getValue(int x, int y, int z, int t);

    /**
     * Renvoie un element de la matrice demandée en fonction des dimensions
     * passé en paramètre.
     *
     * @param coordinates semantics values to get matrix value
     *
     * @return double value at specified dimensions
     */
    public double getValue(Object[] coordinates);

    /**
     * Return a 1D matrix value.
     *
     * @param x first dimension
     *
     * @return double value at specified dimensions
     */
    public double getValue(Object x);

    /**
     * Return a 2D matrix value.
     *
     * @param x first dimension
     * @param y second dimension
     *
     * @return double value at specified dimensions
     */
    public double getValue(Object x, Object y);

    /**
     * Return a 3D matrix value.
     *
     * @param x first dimension
     * @param y second dimension
     * @param z third dimension
     *
     * @return double value at specified dimensions
     */
    public double getValue(Object x, Object y, Object z);

    /**
     * Return a 4D matrix value.
     *
     * @param x first dimension
     * @param y second dimension
     * @param z third dimension
     * @param t fourth dimension
     *
     * @return double value at specified dimensions
     */
    public double getValue(Object x, Object y, Object z, Object t);

    /**
     * Modifie un element de la matrice en fonction des dimensions passé en
     * paramètre.<br>
     *
     * Exemple: Si on a un matrice 3D.<br>
     * set([1,1,1], m) modifie un element de la matrice.<br>
     *
     * @param dimensions dimension indices
     *
     * @param d new double value to set into matrix at specified dimensions
     */
    public void setValue(int[] dimensions, double d);

    /**
     * Modify a 1D matrix value.
     *
     * @param x first dimension index
     *
     * @param d new double value to set into matrix at specified dimensions
     */
    public void setValue(int x, double d);

    /**
     * Modify a 2D matrix value.
     *
     * @param x first dimension index
     * @param y second dimension index
     *
     * @param d new double value to set into matrix at specified dimensions
     */
    public void setValue(int x, int y, double d);

    /**
     * Modify a 3D matrix value.
     *
     * @param x first dimension index
     * @param y second dimension index
     * @param z third dimension index
     *
     * @param d new double value to set into matrix at specified dimensions
     */
    public void setValue(int x, int y, int z, double d);

    /**
     * Modify a 4D matrix value.
     *
     * @param x first dimension index
     * @param y second dimension index
     * @param z third dimension index
     * @param t fourth dimension index
     *
     * @param d new double value to set into matrix at specified dimensions
     */
    public void setValue(int x, int y, int z, int t, double d);

    /**
     * Modifie un element de la matrice en fonction des dimensions passé en
     * paramètre.
     *
     * @param coordinates semantics value to set matrix value
     *
     * @param d new double value to set into matrix at specified dimensions
     */
    public void setValue(Object[] coordinates, double d);

    /**
     * Modify a 1D matrix value.
     *
     * @param x first dimension index
     *
     * @param d new double value to set into matrix at specified dimensions
     */
    public void setValue(Object x, double d);

    /**
     * Modify a 2D matrix value.
     *
     * @param x first dimension
     * @param y second dimension
     *
     * @param d new double value to set into matrix at specified dimensions
     */
    public void setValue(Object x, Object y, double d);

    /**
     * Modify a 3D matrix value.
     *
     * @param x first dimension
     * @param y second dimension
     * @param z third dimension
     *
     * @param d new double value to set into matrix at specified dimensions
     */
    public void setValue(Object x, Object y, Object z, double d);

    /**
     * Modify a 4D matrix value.
     *
     * @param x first dimension
     * @param y second dimension
     * @param z third dimension
     * @param t fourth dimension
     *
     * @param d new double value to set into matrix at specified dimensions
     */
    public void setValue(Object x, Object y, Object z, Object t, double d);

    /**
     * Copy la matrice pour pouvoir la modifier sans perdre les donnees
     * initiales.
     * 
     * @return new matrix
     */
    public MatrixND copy();

    /**
     * Créer une nouvelle instance clonée de celle-ci
     *
     * @return new matrix
     */
    public MatrixND clone();

    /**
     * Somme toutes les valeurs de la matrice.
     * 
     * @return sum result
     */
    public double sumAll();

    /**
     * Somme la matrice sur une dimension donnée. La matrice résultat à le même
     * nombre de dimension, pas la dimension sommer, ne contient qu'une ligne.
     * <p>
     * par exemple pour la matrice suivante si on somme sur la dimension 1 cela
     * donnera
     *
     * <pre>
     * 1 2 3
     * 2 3 4
     * 3 4 5
     * </pre>
     *
     * <pre>
     * 6 9 12
     * </pre>
     *
     * @param dim la dimension sur lequel il faut faire la somme
     * @return new matrix
     */
    public MatrixND sumOverDim(int dim);

    /**
     * Somme la matrice mais la matrice reste de la même dimension. la somme
     * permet juste de regrouper dans une dimension un certain nombre de valeur.
     * <p>
     * pour la matrice suivante :
     *
     * <pre>
     * 1 2 3 4
     * 2 3 4 5
     * 3 4 5 6
     * 4 5 6 7
     * </pre>
     *
     * la somme sur la dimension 1 avec un pas de 2 donnera :
     *
     * <pre>
     * 3 5 7  9
     * 7 9 11 13
     * </pre>
     *
     * c'est à dire que la ligne 0 et la ligne 1 sont sommées. ainsi que la
     * ligne 2 avec la ligne 3.
     *
     * @param dim la dimension sur lequel il faut faire les sommes
     * @param step le pas qu'il faut utiliser pour regrouper les elements. Si le
     *            pas est inférieur à 0, le pas se comporte comme si on avait
     *            passé en argument la taille de la dimension. Un pas de 0 ou 1,
     *            retourne juste une copie de la matrice actuelle. si la
     *            division du pas avec la taille de la dimension ne donne pas un
     *            nombre entier, les elements restants ne sont pas pris en
     *            compte. Par exemple si la dimension a 10 élements et que l'on
     *            donne un pas de 3, dans la matrice resultat la dimension aura
     *            3 elements qui seront la somme par 3 des 9 premiers element de
     *            la matrice courante. Le 10eme element sera perdu.
     * @return une nouvelle matrice avec le meme nombre de dimension mais dont
     *         la dimension passé en paramètre aura comme taille, le resultat de
     *         la division entier de la taille actuelle par le step
     */
    public MatrixND sumOverDim(int dim, int step);

    public MatrixND sumOverDim(int dim, int start, int nb);
    
    /**
     * Return all matrix data mean value
     * 
     * @return mean value
     */
    public double meanAll();

    /**
     * Effectue la moyenne des valeurs sur une dimension donnée. La matrice
     * résultat à le même nombre de dimension, pas la dimension moyenisée, ne
     * contient qu'une ligne.
     * <p>
     * par exemple pour la matrice suivante si on fait la moyenne sur la
     * dimension 1 cela donnera
     *
     * <pre>
     * 1 2 3
     * 8 9 4
     * 7 6 5
     * </pre>
     *
     * <pre>
     * 5.33 4.66 4
     * </pre>
     *
     * @param dim la dimension sur lequel il faut faire la moyenne
     * @return new matrix
     */
    public MatrixND meanOverDim(int dim);

    /**
     * Effectue la moyenne des valeurs sur une dimension donnée. la moyenne
     * permet juste de regrouper dans une dimension un certain nombre de valeur.
     * <p>
     * pour la matrice suivante :
     *
     * <pre>
     * 1 2 3 4
     * 2 3 4 5
     * 3 4 5 6
     * 4 5 6 7
     * </pre>
     *
     * la moyenne sur la dimension 1 avec un pas de 2 donnera :
     *
     * <pre>
     * 1.5 3.5 4.5 4.5
     * 4.5 4.5 5.5 6.5
     * </pre>
     *
     * c'est à dire que sur la ligne 0 et la ligne 1 on fait la moyenne. ainsi
     * que la ligne 2 avec la ligne 3.
     *
     * @param dim la dimension sur lequel il faut faire les sommes
     * @param step le pas qu'il faut utiliser pour regrouper les elements. Si le
     *            pas est inférieur à 0, le pas se comporte comme si on avait
     *            passé en argument la taille de la dimension. Un pas de 0 ou 1,
     *            retourne juste une copie de la matrice actuelle. si la
     *            division du pas avec la taille de la dimension ne donne pas un
     *            nombre entier, les elements restants ne sont pas pris en
     *            compte. Par exemple si la dimension a 10 élements et que l'on
     *            donne un pas de 3, dans la matrice resultat la dimension aura
     *            3 elements qui seront la somme par 3 des 9 premiers element de
     *            la matrice courante. Le 10eme element sera perdu.
     * @return une nouvelle matrice avec le meme nombre de dimension mais dont
     *         la dimension passé en paramètre aura comme taille, le resultat de
     *         la division entier de la taille actuelle par le step
     */
    public MatrixND meanOverDim(int dim, int step);

    /**
     * Permet de supprimer des éléments de la matrice.
     * Par exemple, pour la matrice
     *
     * <pre>
     * 1 2 3 4
     * 2 3 4 5
     * 3 4 5 6
     * 4 5 6 7
     * </pre>
     *
     * un cut(1, [0,2]) donnera
     *
     * <pre>
     * 2 4
     * 3 5
     * 4 6
     * 5 7
     * </pre>
     *
     * @param dim la dimension dans lequel il faut supprimer des éléments
     * @param toCut les éléments à supprimer
     * @return une nouvelle matrice, la matrice actuelle n'est pas modifiée
     */
    public MatrixND cut(int dim, int[] toCut);

    /**
     * Copie une matrice dans la matrice actuelle. La matrice à copier à le même
     * nombre de dimension. Si la matrice à copier est trop grande seul les
     * éléments pouvant être copier le seront.
     *
     * @param mat la matrice à copier
     * @return return la matrice courante.
     */
    public MatrixND paste(MatrixND mat);

    /**
     * Copie une matrice dans la matrice actuelle. La matrice à copier à le même
     * nombre de dimension. Si la matrice à copier est trop grande seul les
     * éléments pouvant être copier le seront.
     *
     * @param origin le point à partir duquel il faut faire la copie
     * @param mat la matrice à copier
     * @return return la matrice courante.
     */
    public MatrixND paste(int[] origin, MatrixND mat);

    /**
     * Modifie la matrice actuel en metant les valeurs de mat passé en parametre
     * La copie se fait en fonction de la semantique, si un element dans une
     * dimension n'est pas trouvé, alors il est passé
     * 
     * @param mat matrix to paste
     * @return new matrix
     */
    public MatrixND pasteSemantics(MatrixND mat);

    /**
     * Permet de prendre une sous matrice dans la matrice courante. La sous
     * matrice a le même nombre de dimensions mais sur une des dimensions on ne
     * prend que certain élément.
     *
     * @param dim la dimension dans lequel on veut une sous matrice si dim est
     *            négatif alors la dimension est prise à partir de la fin par
     *            exemple si l'on veut la derniere dimension il faut passer -1
     *            pour dim
     * @param start la position dans dim d'ou il faut partir pour prendre la
     *            sous matrice.
     * @param nb le nombre d'élément à prendre dans la dimension. si nb est
     *            inférieur ou égal à 0 alors cela indique qu'il faut prendre
     *            tous les éléments jusqu'à la fin de la dimension.
     * @return un objet SubMatrix qui est une vu de la matrice initiale (toute
     * modification de la sous-matrice, modifie la matrice initiale)
     */
    public MatrixND getSubMatrix(int dim, Object start, int nb);

    /**
     * Permet de prendre une sous matrice dans la matrice courante. La sous
     * matrice a le même nombre de dimensions mais sur une des dimensions on ne
     * prend que certain élément.
     *
     * @param dim la dimension dans lequel on veut une sous matrice
     * @param start la position dans dim d'ou il faut partir pour prendre la
     *            sous matrice. 0 &lt;= start &lt; dim.size si start est négatif alors
     *            la position de départ est calculé par rapport à la fin de la
     *            dimension, pour avoir le dernier élément il faut passer -1
     * @param nb le nombre d'élément à prendre dans la dimension si nb est
     *            inférieur ou égal à 0 alors cela indique qu'il faut prendre
     *            tous les éléments jusqu'à la fin de la dimension.
     * @return un objet SubMatrix qui est une vu de la matrice initiale (toute
     * modification de la sous-matrice, modifie la matrice initiale)
     */
    public MatrixND getSubMatrix(int dim, int start, int nb);

    /**
     * Permet de prendre une sous matrice dans la matrice courante. La sous
     * matrice a le même nombre de dimensions mais sur une des dimensions on ne
     * prend que certain élément.
     *
     * @param dim la dimension dans lequel on veut une sous matrice
     * @param elem les éléments dans la dimension à conserver
     * @return un objet SubMatrix qui est une vu de la matrice initiale (toute
     * modification de la sous-matrice, modifie la matrice initiale)
     */
    public MatrixND getSubMatrix(int dim, Object... elem);

    /**
     * Permet de prendre une sous matrice dans la matrice courante. La sous
     * matrice a le même nombre de dimensions mais sur une des dimensions on ne
     * prend que certain élément.
     *
     * @param dim la dimension dans lequel on veut une sous matrice
     * @param elem les indices des éléments dans la dimension à conserver
     * @return un objet SubMatrix qui est une vu de la matrice initiale (toute
     * modification de la sous-matrice, modifie la matrice initiale)
     */
    public MatrixND getSubMatrix(int dim, int[] elem);

    /**
     * Permet de prendre une sous matrice dans la matrice courante.
     * 
     * Réalise plusieurs appels à {@link #getSubMatrix(int, Object...)} suivant
     * l'implémentation.
     *
     * @param elem les éléments dans la dimension à conserver
     * @return un objet SubMatrix qui est une vu de la matrice initiale (toute
     * modification de la sous-matrice, modifie la matrice initiale)
     */
    public MatrixND getSubMatrix(Object[]... elem);

    /**
     * Permet de prendre une sous matrice dans la matrice courante.
     * 
     * Réalise plusieurs appels a {@link #getSubMatrix(int, int[])} suivant
     * l'implementation.
     *
     * @param elems les indices des éléments pour chaque dimension à conserver
     * @return un objet SubMatrix qui est une vu de la matrice initiale (toute
     * modification de la sous-matrice, modifie la matrice initiale)
     */
    public MatrixND getSubMatrix(int[]... elems);

    /**
     * Addition la matrice courante avec la matrice passe en parametre et se
     * retourne elle meme.
     * 
     * @param m matrix to add
     * @return new matrix
     */
    public MatrixND add(MatrixND m);

    /**
     * Soustrai la matrice courante avec la matrice passe en parametre et ce
     * retourne elle meme.
     * 
     * @param m matrix to minus
     * @return new matrix
     */
    public MatrixND minus(MatrixND m);

    /**
     * retourne le transpose de la matrice.
     * 
     * @return transposed matrix
     */
    public MatrixND transpose();

    /**
     * Reduit la matrice de sorte que toutes les dimensions qui n'ont qu'un
     * élement soit supprimée. Au pire cette méthode retourne une matrice à une
     * seule dimension à un seul élément.
     *
     * @return une nouvelle matrice plus petite que la matrice actuelle ou egal
     *         s'il n'y a aucune dimension à supprimer
     */
    public MatrixND reduce();

    /**
     * Reduit la matrice de sorte que toutes les dimensions qui n'ont qu'un
     * élement soit supprimée. Au pire cette méthode retourne une matrice à une
     * seule dimension à un seul élément.
     *
     * @param minNbDim le nombre minimum de dimension que l'on souhaite pour la
     *            matrice résultat
     * @return une nouvelle matrice plus petite que la matrice actuelle ou egal
     *         s'il n'y a aucune dimension à supprimer
     */
    public MatrixND reduce(int minNbDim);

    /**
     * Reduit le matrice seulement sur les dimensions passées en argument. Si
     * une des dimensions passées en arguement n'a pas un seul élément, cette
     * dimension n'est pas prise en compte.
     *
     * @param dims les dimensions sur lequel il faut faire la reduction
     * @return une nouvelle matrice
     */
    public MatrixND reduceDims(int... dims);

    /**
     * Multiplication normal (produit matriciel) de 2 matrices 2D.
     * 
     * @param m matrix to mult
     * @return Retourne une nouvelle matrice.
     */
    public MatrixND mult(MatrixND m);

    /**
     * Multiplication d'une matrice par un scalaire.
     * 
     * @param d scalaire
     * @return new matrix
     */
    public MatrixND mults(double d);

    /**
     * Multiplication d'une matrice par un scalaire.
     * 
     * @param d scalaire
     * @return new matrix
     */
    public MatrixND divs(double d);

    /**
     * Addition d'un scalaire à une matrice.
     * 
     * @param d scalaire
     * @return new matrix
     */
    public MatrixND adds(double d);

    /**
     * Soustraction d'un scalaire à une matrice
     * 
     * @param d scalaire
     * @return new matrix
     */
    public MatrixND minuss(double d);

    /**
     * Donne la matrice sous forme de List de list ... de double
     *
     * @return list matrix representation
     */
    public List<?> toList();

    /**
     * Permet de charger une matrice a partir d'une representation List
     *
     * @param list la matrice sous forme de List de list ... de double
     */
    public void fromList(List<?> list);

    /**
     * Determine si la matrice supporte l'import et l'export CSV
     *
     * @return support du CSV
     * @deprecated since 2.2, always return {@code true}, CSV import/export is
     *      always supported
     */
    @Deprecated
    public boolean isSupportedCSV();

    /**
     * Import depuis un reader au format CSV des données dans la matrice.
     *
     * @param reader le reader à importer
     * @param origin le point à partir duquel il faut faire l'importation
     * @throws IOException
     */
    public void importCSV(Reader reader, int[] origin) throws IOException;
    
    /**
     * Import depuis un reader au format CSV des données dans la matrice.
     *
     * @param reader le reader à importer
     * @param origin le point à partir duquel il faut faire l'importation
     * @param matrixName le nom de la matrice
     * @throws IOException
     */
    public void importCSV(Reader reader, int[] origin, String matrixName) throws IOException;
    
    /**
     * Import depuis un reader au format CSV des données dans la matrice.
     * 
     * Call {importCSV(Reader, int[], String)} with file name as matrix name.
     * 
     * @param file file to read
     * @param origin le point à partir duquel il faut faire l'importation
     * @throws IOException
     */
    public void importCSV(File file, int[] origin) throws IOException;

    /**
     * Export dans un writer au format CSV de la matrice
     *
     * @param writer le writer ou copier la matrice
     * @param withSemantics export ou pas des semantiques de la matrice dans le
     *            writer
     * @throws IOException
     */
    public void exportCSV(Writer writer, boolean withSemantics)
            throws IOException;

    /**
     * Export dans un writer au format CSV de la matrice
     *
     * @param writer le writer ou copier la matrice
     * @param withSemantics export ou pas des semantiques de la matrice dans le
     *            writer
     * @throws java.io.IOException
     */
    public void exportCSVND(Writer writer, boolean withSemantics)
            throws IOException;


    /**
     * Verifie si les matrices sont egales en ne regardant que les valeurs et
     * pas les semantiques
     *
     * @param mat
     * @return equlaity on values
     */
    public boolean equalsValues(MatrixND mat);

} // MatrixND
