package org.nuiton.math.matrix;

/*
 * #%L
 * Nuiton Matrix :: API
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

/**
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class VectorIteratorImpl implements VectorIterator {

    protected boolean hasExclude = false;
    protected double exclude;
    protected Vector vector;
    protected long size;
    protected long pos = -1L;
    protected long nextPos = -1L;

    public VectorIteratorImpl(Vector vector) {
        this.vector = vector;
        this.size = vector.size();
    }

    public VectorIteratorImpl(Vector vector, double exclude) {
        this(vector);
        setExclude(exclude);
    }

    @Override
    public void setExclude(double exclude) {
        this.hasExclude = true;
        this.exclude = exclude;
    }

    protected void computeNextPos() {
        // compute nextPos only if not already computed (multiple call to hasNext must be possible.
        if (nextPos <= pos) {
            do {
                nextPos += 1;
            } while (nextPos < size && hasExclude && vector.getValue(nextPos) == exclude);
        }
    }

    @Override
    public boolean hasNext() {
        computeNextPos();
        return nextPos < size;
    }

    @Override
    public double next() {
        computeNextPos();
        pos = nextPos;
        return getValue();
    }

    @Override
    public double getValue() {
        return vector.getValue(pos);
    }

    @Override
    public void setValue(double value) {
        vector.setValue(pos, value);
    }

    @Override
    public long getPosition() {
        return pos;
    }

}
