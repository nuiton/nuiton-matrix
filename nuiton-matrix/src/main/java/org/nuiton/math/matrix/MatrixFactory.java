/*
 * #%L
 * Nuiton Matrix :: API
 * %%
 * Copyright (C) 2004 - 2023 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.math.matrix;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.lang3.StringUtils;


/**
 * Cette classe permet de creer des matrices, toutes les creations de matrice
 * doivent etre faite a travers cette classe. Cette classe permet de modifier la
 * representation interne des matrices de facon simple.
 * <p>
 * Created: 11 octobre 2005 20:15:20 CEST
 * 
 * @author Benjamin POUSSIN &lt;poussin@codelutin.com&gt;
 * @version $Revision$
 * 
 * Last update: $Date$ by :
 * $Author$
 */
public class MatrixFactory { // MatrixFactory


    final static private Pattern DIM_AND_DEFAULT_LINE = Pattern.compile(
            // match [1,2,3]
            // match [1,2,3] 3.14
            "^\\s*\\[((?:\\s*[0-9]+\\s*,?)+)\\]\\s*,?\\s*(" + AbstractMatrixND.NUMBER_REGEX + ")?\\s*$");

    final static private Pattern SEMANTICS_LINE = Pattern.compile(
            // match: <Class name>:value1, value2, value3, ...
            "^\\s*([^:]+)\\s*:\\s*(.+)\\s*$");

    final static private Pattern DATA_LINE = Pattern.compile(
            // match: x;y;z;...;<value>
            "^(.*)" + AbstractMatrixND.CSV_SEPARATOR + "(.*)$"
    );

    /**
     * If true, createVector return all time LazyVector to prevent memory
     * allocation when not necessary. LazyVector init real vector only when
     * necessary
     */
    protected static boolean defaultUseLazyVector = true;

    /** Valeur par defaut si aucun type de Vector n'est donné */
    protected static Class<?> defaultVectorClass = DoubleBigVector.class;
    protected static Class<?> defaultSparseVectorClass = DoubleSparseArrayVector.class;
    /** if we try to allocate matrix with more that this threshold, sparse matrix is used*/
    protected static int defaultThresholdSparse = 1000;

    protected Class<?> vectorClass = null;
    protected Class<?> sparseVectorClass = null;
    protected int thresholdSparse = 1000;
    protected boolean useLazyVector = true;

    protected static SemanticMapper defaultSemanticMapper = new SemanticMapper();

    private static ThreadLocal<MatrixFactory> matrixFactoryThreadLocal =
            new ThreadLocal<MatrixFactory>();


    protected MatrixFactory(Class<?> vectorClass, Class<?> sparseVectorClass, int thresholdSparse, boolean useLazyVector) {
        this.vectorClass = vectorClass;
        this.sparseVectorClass = sparseVectorClass;
        this.thresholdSparse = thresholdSparse;
        this.useLazyVector = useLazyVector;
    }

    public Class<?> getVectorClass() {
        return vectorClass;
    }

    public Class<?> getSparseVectorClass() {
        return sparseVectorClass;
    }

    public int getThresholdSparse() {
        return thresholdSparse;
    }

    public boolean isUseLazyVector() {
        return useLazyVector;
    }

    public static void setDefaultVectorClass(Class<?> vectorClass) {
        defaultVectorClass = vectorClass;
    }

    public static void setDefaultSparseVectorClass(Class<?> defaultSparseVectorClass) {
        MatrixFactory.defaultSparseVectorClass = defaultSparseVectorClass;
    }

    public static void setDefaultThresholdSparse(int defaultThresholdSparse) {
        MatrixFactory.defaultThresholdSparse = defaultThresholdSparse;
    }

    public static void setDefaultUseLazyVector(boolean useLazyVector) {
        MatrixFactory.defaultUseLazyVector = useLazyVector;
    }

    public static Class<?> getDefaultVectorClass() {
        return defaultVectorClass;
    }

    public static void setSemanticMapper(SemanticMapper semanticMapper) {
        defaultSemanticMapper = semanticMapper;
    }

    public static SemanticMapper getSemanticMapper() {
        return defaultSemanticMapper;
    }

    /**
     * Retourne une factory utilisant vectorClass comme classe de base a
     * l'implantation des matrices, aussi bien pour les matrices dense que
     * les matrices creuse. Par defaut on utilise les LazyVector
     *
     * @param vectorClass vector class implementation
     * @return factory
     */
    public static MatrixFactory getInstance(Class<?> vectorClass) {
        return new MatrixFactory(vectorClass, vectorClass, 0, defaultUseLazyVector);
    }

    /**
     * Retourne une factory utilisant, par defaut on utilise les LazyVector
     *
     * @param vectorClass vector class implementation for dense matrix
     * @param sparseVectorClass vector class implementation for sparse matrix
     * @param thresholdSparse threshold to determine usage of dense or sparse matrix
     *
     * @return factory
     */
    public static MatrixFactory getInstance(Class<?> vectorClass, Class<?> sparseVectorClass, int thresholdSparse) {
        return new MatrixFactory(vectorClass, sparseVectorClass, thresholdSparse, defaultUseLazyVector);
    }

    /**
     * Retourne une factory utilisant vectorClass comme classe de base a
     * l'implantation des matrices.
     *
     * @param vectorClass vector class implementation for dense matrix
     * @param sparseVectorClass vector class implementation for sparse matrix
     * @param thresholdSparse threshold to determine usage of dense or sparse matrix
     * @param useLazyVector if true use LazyVector
     *
     * @return factory
     */
    public static MatrixFactory getInstance(Class<?> vectorClass, Class<?> sparseVectorClass, int thresholdSparse, boolean useLazyVector) {
        return new MatrixFactory(vectorClass, sparseVectorClass, thresholdSparse, useLazyVector);
    }

    /**
     * Utilise par defaut {@link #defaultVectorClass}. Si une factory a ete
     * initialisee pour le thread local, alors celle-ci sera reutilise, sinon
     * une nouvelle est cree avec le backend par defaut
     * 
     * @return factory
     */
    public static MatrixFactory getInstance() {
        MatrixFactory result = matrixFactoryThreadLocal.get();
        if (result == null) {
            result = getInstance(defaultVectorClass, defaultSparseVectorClass, defaultThresholdSparse, defaultUseLazyVector);
        }
        return result;
    }

    /**
     * Initialise une factory pour le thread courant avec le vectorClass passe
     * en parametre.

* @param vectorClass vector class implementation for dense matrix
     * @param sparseVectorClass vector class implementation for sparse matrix
     * @param thresholdSparse threshold to determine usage of dense or sparse matrix
     * @param useLazyVector if true use LazyVector
     */
    public static void initMatrixFactoryThreadLocal(Class<?> vectorClass, Class<?> sparseVectorClass, int thresholdSparse, boolean useLazyVector) {
        matrixFactoryThreadLocal.set(getInstance(vectorClass, sparseVectorClass, thresholdSparse, useLazyVector));
    }

    /**
     * Retire la factory pour le thread courant
     */
    public static void removeMatrixFactoryThreadLocal() {
        matrixFactoryThreadLocal.remove();
    }

    /**
     * Create new Matrix from file.
     * File contains data as describe for export/import CSV ND
     *
     * @param file
     * @return
     * @throws IOException
     * @since 2.5.2
     */
    public MatrixND create(File file) throws IOException {
        Reader in = new BufferedReader(new InputStreamReader(new FileInputStream(file), StandardCharsets.UTF_8));
        try {
            String matrixName = file.getName();
            int extPos = matrixName.lastIndexOf('.');
            if (extPos != -1) { // remove extension
                matrixName = matrixName.substring(0, extPos);
            }

            MatrixND result = create(in);
            result.setName(matrixName);
            return result;
        } finally {
            in.close();
        }
    }

    /**
     * Create new Matrix from file.
     * File contains data as describe for export/import CSV ND
     * @param reader
     * @return
     * @throws IOException
     * @since 2.5.2
     */
    public MatrixND create(Reader reader) throws IOException {
        BufferedReader in = new BufferedReader(reader);

        String line = readLine(in);

        if (line == null) {
            throw new MatrixException("Bad file format, file is not Matrix");
        }

        // read metadata
        int[] dimensions = readDimensions(line);
        double defaultValue = readDefaultValue(line);
        List[] semantics = readSemantics(dimensions, in);

        MatrixND matrix = this.create(semantics);
        MatrixHelper.fill(matrix, defaultValue);

        // read data
        line = readLine(in);
        while (line != null) {
            Matcher m = DATA_LINE.matcher(line);
            if (m.matches()) {
                int[] coords = readCoordinates(m.group(1));
                double val = readDouble(m.group(2));
                matrix.setValue(coords, val);
            } else {
                throw new MatrixException("Bad file format, can't read data");
            }
            line = readLine(in);
        }

        return matrix;
    }

    private String readLine(BufferedReader in) throws IOException {
        String line = in.readLine();
        while (line != null && StringUtils.isBlank(line)) {
            line = in.readLine();
        }
        return line;
    }

    private int[] readDimensions(String line) throws IOException {
        int[] dimensions;

        Matcher m = DIM_AND_DEFAULT_LINE.matcher(line);

        if (m.matches()) {
            String dimString = m.group(1);
            try {
                String[] dimArrayString = dimString.split("\\s*,\\s*");
                dimensions = new int[dimArrayString.length];
                int i = 0;
                for (String d : dimArrayString) {
                    dimensions[i++] = Integer.parseInt(d);
                }
            } catch (Exception eee) {
                throw new MatrixException("Can't parse dimension value: " + dimString, eee);
            }
        } else {
            throw new MatrixException("Line doesn't match dimension and default value information:" + line);
        }

        return dimensions;
    }

    private double readDefaultValue(String line) throws IOException {
        double defaultValue = 0.0;

        Matcher m = DIM_AND_DEFAULT_LINE.matcher(line);

        if (m.matches()) {
            String defaultValueString = m.group(2);
            if (StringUtils.isNoneBlank(defaultValueString)) {
                try {
                    defaultValue = Double.parseDouble(defaultValueString);
                } catch (Exception eee) {
                    throw new MatrixException("Can't parse default value: " + defaultValueString, eee);
                }
            }
        } else {
            throw new MatrixException("Line doesn't match dimension and default value information:" + line);
        }
        return defaultValue;
    }

    /**
     * Read a line and convert line to semantic value.
     *
     * Use:
     *  - mapper to convert semantics values
     */
    private List[] readSemantics(int[] dimensions, BufferedReader in) throws IOException {
        List[] semantics = new List[dimensions.length];

        for (int indexDim = 0 ; indexDim < dimensions.length ; indexDim++) {
            String line = readLine(in);
            if (line == null) {
                throw new MatrixException("Bad file format, file is not Matrix (semantics missing)");
            }
            Matcher m = SEMANTICS_LINE.matcher(line);
            if (m.matches()) {
                String type = m.group(1).trim();
                String[] semString = m.group(2).split("\\s*,\\s*");

                List sems = new ArrayList();
                Class typeClass = MatrixFactory.getSemanticMapper().getType(type);
                for (String s : semString) {
                    Object value = MatrixFactory.getSemanticMapper().getValue(typeClass, s);
                    sems.add(value);
                }
                if (sems.size() != dimensions[indexDim]) {
                    throw new MatrixException(String.format("Semantics %d count not equals to semantics dimension, excepted %d, got %d",
                            indexDim, dimensions[indexDim], sems.size()));
                }
                semantics[indexDim] = sems;
            } else {
                throw new MatrixException("Bad file format, line is not a semantics declaration: " + line);
            }
        }

        return semantics;
    }

    private int[] readCoordinates(String s) {
        String[] coords = s.split("\\s*" + AbstractMatrixND.CSV_SEPARATOR + "\\s*");
        int[] result = new int[coords.length];
        for (int i = 0, max = coords.length; i < max; i++) {
            try {
                result[i] = Integer.parseInt(coords[i]);
            } catch (Exception eee) {
                throw new MatrixException("Can't parse coordinate value: " + s, eee);
            }
        }
        return result;
    }

    private double readDouble(String s) {
        try {
            double result = Double.parseDouble(s);
            return result;
        } catch (Exception eee) {
            throw new MatrixException("Can't parse value: " + s, eee);
        }
    }


    public MatrixND create(int[] dim) {
        return new MatrixNDImpl(this, dim);
    }

    /**
     * Convert a double array into matrix.
     * 
     * @param values The values to fill the matrix
     * @param dim An array representing the dimensions of the matrix
     * @return a 2D matrix filled with the values, null if the dimension is more
     *         than 2
     */
    public MatrixND create(double[] values, int[] dim) {

        if (dim.length > 2) {
            return null;
        }
        MatrixNDImpl matrix = new MatrixNDImpl(this, dim);

        if (dim.length == 2) {
            for (int i = 0; i < dim[0]; i++) {
                for (int j = 0; j < dim[1]; j++) {
                    int[] coordinates = { i, j };
                    matrix.setValue(coordinates, values[i * dim[1] + j]);
                }
            }
        }
        if (dim.length == 1) {
            for (int i = 0; i < dim[0]; i++) {
                int[] coordinates = { i };
                matrix.setValue(coordinates, values[i]);
            }
        }

        return matrix;
    }

    public MatrixND create(List<?>[] semantics) {
        return new MatrixNDImpl(this, semantics);
    }

    public MatrixND create(String name, int[] dim) {
        return new MatrixNDImpl(this, name, dim);
    }

    public MatrixND create(String name, int[] dim, String[] dimNames) {
        return new MatrixNDImpl(this, name, dim, dimNames);
    }

    /**
     * Create new matrix and force backend to be data.
     * 
     * @param name matrix's name (can be null)
     * @param dim matrix's dimension (must be provided)
     * @param dimNames dimensions' name (can be null)
     * @param data backend used to new matrix
     * @return  new Matrix with specified backend
     */
    public MatrixND create(String name, int[] dim, String[] dimNames, Vector data) {
        return new MatrixNDImpl(this, name, dim, dimNames, data);
    }

    public MatrixND create(String name, List<?>[] semantics) {
        return new MatrixNDImpl(this, name, semantics);
    }

    public MatrixND create(String name, List<?>[] semantics, String[] dimNames) {
        return new MatrixNDImpl(this, name, semantics, dimNames);
    }

    /**
     * Create new matrix and force backend to be data.
     *
     * @param name matrix's name (can be null)
     * @param semantics semantiques (must be provided)
     * @param dimNames dimensions' name (can be null)
     * @param data backend used to new matrix
     * @return new Matrix with specified backend
     */
    public MatrixND create(String name, List<?>[] semantics, String[] dimNames, Vector data) {
        return new MatrixNDImpl(this, name, semantics, dimNames, data);
    }

    /**
     * Create new matrix by copying matrix in argument
     * @param matrix
     * @return
     */
    public MatrixND create(MatrixND matrix) {
        return new MatrixNDImpl(this, matrix);
    }

    /**
     * Create new matrix by copying matrix in argument, and force backend
     * to be data.
     * @param matrix matrix to copied
     * @param data backend used to new matrix
     */
    public MatrixND create(MatrixND matrix, Vector data) {
        return new MatrixNDImpl(this, matrix, data);
    }

    /**
     * Crée une nouvelle matrice identité. Une matrice identité est une matrice
     * à 2 dimensions dont tous les éléments de la diagonal vaut 1
     * 
     * @param size la taille de la matrice
     * @return une nouvelle matrice identité
     */
    public MatrixND matrixId(int size) {
        MatrixND result = create(new int[] { size, size });
        for (int i = 0; i < size; i++) {
            result.setValue(i, i, 1);
        }
        return result;
    }

    /**
     * cree un nouveau Vector. Si la taille demandee est inferieur au seuil
     * alors on cree une matrice plaine, sinon on cree une matrice creuse.
     * @param length
     * @return
     */
    protected Vector createVector(long length) {
        try {
            Vector result;
            if (thresholdSparse <=0 || length <= thresholdSparse) {
                result = (Vector)vectorClass.newInstance();
            } else {
                result = (Vector)sparseVectorClass.newInstance();
            }
            if (useLazyVector) {
                result = new LazyVector(result, length);
            }
            result.init(length);
            return result;
        } catch (Exception eee) {
            throw new RuntimeException("Can't create vector", eee);
        }
    }
    
    public MatrixProxy createProxy(List<?>[] semantics, MatrixProvider matrixProvider) {
        MatrixProxy matrixProxy = new MatrixProxy(this, semantics);
        matrixProxy.setMatrixProvider(matrixProvider);
        return matrixProxy;
    }

    public MatrixProxy createProxy(String name, int[] dim, MatrixProvider matrixProvider) {
        MatrixProxy matrixProxy = new MatrixProxy(this, name, dim);
        matrixProxy.setMatrixProvider(matrixProvider);
        return matrixProxy;
    }

    public MatrixProxy createProxy(String name, int[] dim, String[] dimNames, MatrixProvider matrixProvider) {
        MatrixProxy matrixProxy = new MatrixProxy(this, name, dim, dimNames);
        matrixProxy.setMatrixProvider(matrixProvider);
        return matrixProxy;
    }

    public MatrixProxy createProxy(String name, List<?>[] semantics, MatrixProvider matrixProvider) {
        MatrixProxy matrixProxy = new MatrixProxy(this, name, semantics);
        matrixProxy.setMatrixProvider(matrixProvider);
        return matrixProxy;
    }

    public MatrixProxy createProxy(String name, List<?>[] semantics, String[] dimNames, MatrixProvider matrixProvider) {
        MatrixProxy matrixProxy = new MatrixProxy(this, name, semantics, dimNames);
        matrixProxy.setMatrixProvider(matrixProvider);
        return matrixProxy;
    }

} // MatrixFactory
