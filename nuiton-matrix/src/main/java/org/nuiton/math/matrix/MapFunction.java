/*
 * #%L
 * Nuiton Matrix :: API
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.math.matrix;

/**
 * Permet de faire un traitement sur des valeurs et d'en retourner 
 * des nouvelles.
 *
 * ATTENTION une function ne doit pas converser d'etat interne qui
 * modifierait son comportement a chaque execution, sinon cette methode
 * ne fonctionne pas, car pour les Vector implanter a base de Map
 * la fonction n'est appliqué qu'au defaultValue et au valeur deja existante
 *
 * Created: 27 oct. 2004
 *
 * @author Benjamin Poussin &lt;poussin@codelutin.com&gt;
 * @version $Revision$
 *
 * Mise a jour: $Date$
 * par : $Author$
 */
public interface MapFunction { // MapFunction

    /**
     * Permet de faire un traitement sur value et de retourne une nouvelle
     * valeur.
     * 
     * @param value la valeur courante sur lequel il faut faire le traitement
     * @return la nouvelle valeur à mettre dans la matrice à la place de
     *         l'ancienne.
     */
    double apply(double value);

} // MapFunction
