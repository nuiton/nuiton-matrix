/*
 * #%L
 * Nuiton Matrix :: API
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.math.matrix;

/**
 * Vector.
 *
 * Created: 6 octobre 2005 02:51:12 CEST
 *
 * @author Benjamin POUSSIN &lt;poussin@codelutin.com&gt;
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public interface Vector { // Vector

    /**
     * Init vector, before this method call, vector is in indetermined state.
     * multiple call to init method must be permit. Only first call must do
     * some work, extra call must do nothing
     * 
     * @param capacity 
     */
    public void init(long capacity);

    /**
     * return information on this vector. This information depends on implementation
     * example:
     * - size
     * - max occurence number
     * - ...
     * 
     * @return 
     * @since 2.4.2
     */
    public String getInfo();

    /**
     * Returne number of assigned value, assigned value is value stored in memory
     * @since 2.4.2
     */
    public long getNumberOfAssignedValue();

    /**
     * Retourne un objet Inc pret a etre utilisé pour boucler sur tous les
     * element de la matrice.
     *
     * @return un objet Inc pret à être utilisé
     */
    public VectorIterator iterator();

    /**
     * Retourne un objet Inc pret a etre utilisé pour boucler sur tous les
     * element different de 0 de la matrice.
     *
     * @return un objet Inc pret à être utilisé
     */
    public VectorIterator iteratorNotZero();

    /**
     * @deprecated since 2.1, use {@link #getMaxOccurrence()} instead
     */
    @Deprecated
    public double getMaxOccurence();

    /**
     * Retourne la valeur la plus utilise dans le vector
     * @return
     */
    public double getMaxOccurrence();

    public double getValue(long pos);

    public void setValue(long pos, double value);

    public long size();

    /**
     * Permet de savoir si paste est implanté par ce vector.
     * 
     * @param v vector to test
     * @return <code>true</code> if operation is supported
     */
    public boolean isImplementedPaste(Vector v);

    /**
     * Permet de savoir si add est implanté par ce vector.
     * 
     * @param v vector to test
     * @return <code>true</code> if operation is supported
     */
    default boolean isImplementedAdd(Vector v) {
        return true;
    }

    /**
     * Permet de savoir si minus est implanté par ce vector.
     * 
     * @param v vector to test
     * @return <code>true</code> if operation is supported
     */
    default boolean isImplementedMinus(Vector v) {
        return true;
    }

    /**
     * Permet de savoir si map est implanté par ce vector.

     * @return <code>true</code> if operation is supported
     */
    public boolean isImplementedMap();

    /**
     * Copie les valeurs du vector passé en argument dans ce vector.
     * 
     * @param source vector to paste
     */
    public void paste(Vector source);

    /**
     * Ajoute les valeurs du vector passé en argument a ce vector.
     * 
     * @param v vector to add
     */
    default void add(Vector v) {
        v.forEachNotZero((pos, val) -> {
            double value = getValue(pos);
            setValue(pos, value + val);
        });
    }

    /**
     * Soustrait les valeurs du vector passé en argument a ce vector.
     * 
     * @param v vector to minus
     */
    default void minus(Vector v) {
        v.forEachNotZero((pos, val) -> {
            double value = getValue(pos);
            setValue(pos, value - val);
        });
    }

    /**
     * applique a chaque valeur du vector la {@link MapFunction}.
     * 
     * ATTENTION cette function ne doit pas converser d'etat interne qui
     * modifierait son comportement a chaque execution, sinon cette methode
     * ne fonctionne pas, car pour les Vector implanter a base de Map
     * la fonction n'est appliqué qu'au defaultValue et au valeur deja existante
     * 
     * @param f funtion to apply
     */
    public void map(MapFunction f);

    default void forEach(VectorForEachFunction f) {
        for (long i = 0, max = size(); i < max; i++) {
            double val = getValue(i);
            f.apply(i, val);
        }
    }

    default void forEachNotZero(VectorForEachFunction f) {
        for (long i = 0, max = size(); i < max; i++) {
            double val = getValue(i);
            if (val != 0) {
                f.apply(i, val);
            }
        }
    }

} // Vector

