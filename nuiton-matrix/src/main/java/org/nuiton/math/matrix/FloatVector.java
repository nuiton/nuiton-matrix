/*
 * #%L
 * Nuiton Matrix :: API
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.math.matrix;

import org.apache.commons.collections.primitives.ArrayFloatList;

import java.util.Arrays;

/**
 * Permet de stocker des données à une position lineair et de la redemander
 * Cette classe ne gére que les données lineaire. L'avantage de cette classe est
 * de ne conserver que les elements differents de la valeur par defaut, ce qui
 * minimize la taille du tableau necessaire a conserver les données.
 * 
 * Created: 6 octobre 2005 01:29:23 CEST
 *
 * @author Benjamin POUSSIN &lt;poussin@codelutin.com&gt;
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class FloatVector implements Vector { // FloatVector

    /** maximum number of element, maximum pos value */
    protected long capacity = 0;

    /** la valeur par defaut */
    protected float defaultValue = 0;

    /** contient la position de l'element, le tableau est trie */
    protected long[] position;
    protected int positionSize = 0;

    /** contient la valeur de l'element */
    protected ArrayFloatList data;

    public FloatVector() {
    }

    public FloatVector(long capacity) {
        init(capacity);
    }

    public FloatVector(long capacity, float defaultValue) {
        this(capacity);
        this.defaultValue = defaultValue;
    }

    @Override
    public void init(long capacity) {
        if (capacity > Integer.MAX_VALUE) {
            throw new IllegalArgumentException("FloatBigVector don't support this capacity : " + capacity);
        }
       if (data == null) {
            this.capacity = capacity;
            position = new long[8];
            data = new ArrayFloatList();
            Arrays.fill(position, Integer.MAX_VALUE);
        }
    }

    @Override
    public String getInfo() {
        return "Float vector sparse: " + data.size() + "/" + size();
    }

    @Override
    public long getNumberOfAssignedValue() {
        return data.size();
    }

    @Override
    public long size() {
        return capacity;
    }

    @Deprecated
    @Override
    public double getMaxOccurence() {
        return getMaxOccurrence();
    }

    @Override
    public double getMaxOccurrence() {
        float result = defaultValue;

        float[] tmp = data.toArray();

        // si potentiellement il y a plus d'element identique dans data
        // que de valeur par defaut, on recherche la valeur possible
        if (this.capacity < 2 * tmp.length) {
            Arrays.sort(tmp);

            // le nombre de fois que l'on a rencontrer la valeur la plus
            // nombreuse
            int max = 1;
            // le nombre de fois que l'on a rencontrer la valeur courante
            int count = 1;
            // la valeur la plus rencontrer
            result = tmp[0];
            // la valeur que l'on vient de traiter précédement
            float old = tmp[0];
            // la valeur courante lu dans le tableaux
            float current = tmp[0];
            // tant que l'on peut encore trouve un element plus nombreux dans le
            // tableau on le parcours
            for (int i = 1; max < tmp.length - i + count && i < tmp.length; i++) {
                current = tmp[i];

                if (current == old) {
                    count++;
                } else {
                    if (count > max) {
                        max = count;
                        result = old;
                    }
                    count = 1;
                    old = current;
                }
            }
            if (count > max) {
                max = count;
                result = current;
            }

            if (max <= capacity - tmp.length) {
                // en fin de compte, il n'y a pas plus d'element identique
                // dans data que de defaultValue
                result = defaultValue;
            }
        }

        return result;
    }

    protected void checkPos(long pos) {
        if (pos < 0 || pos >= capacity) {
            throw new IllegalArgumentException("pos " + pos + " is not in [0, "
                    + capacity + "]");
        }
    }

    @Override
    public double getValue(long pos) {
        checkPos(pos);

        float result = defaultValue;
        int index = findIndex(pos);
        if (index >= 0) {
            result = data.get(index);
        }
        return result;
    }

    @Override
    public void setValue(long pos, double dValue) {
        checkPos(pos);

        float value = (float) dValue;
        int index = findIndex(pos);
        if (index >= 0) {
            if (value == defaultValue) {
                // il etait present, on supprime l'element
                removeElementAt(index);
                data.removeElementAt(index);
            } else {
                // il etait deja present, on modifie la valeur
                data.set(index, value);
            }
        } else {
            // il n'etait pas present
            if (value != defaultValue) {
                // il faut ajouter dans position et dans data
                index = -index - 1;

                addElementAt(index, pos);
                data.add(index, value);
            }
        }
    }

    @Override
    public boolean equals(Object o) {
        boolean result = false;
        if (o instanceof FloatVector && defaultValue == ((FloatVector)o).defaultValue) {
            FloatVector other = (FloatVector) o;
            result = Arrays.equals(this.position, other.position)
                    && data.equals(other.data);
        } else if (o instanceof Vector) {
            Vector other = (Vector) o;
            result = size() == other.size();
            for (int i = 0; i < size() && result; i++) {
                result = getValue(i) == other.getValue(i);
            }
        }
        return result;
    }

    /**
     * retourne la position dans le tableau position de la position lineaire
     * 
     * @param pos
     * @return la position ou &lt; 0 donnant la position de l'element s'il etait
     *         present
     */
    protected int findIndex(long pos) {
        return Arrays.binarySearch(position,(int) pos);
    }

    protected void ensureCapacity(int mincap) {
        if (mincap > position.length) {
            int newcap = (position.length * 3) / 2 + 1;
            long olddata[] = position;
            position = new long[newcap >= mincap ? newcap : mincap];
            System.arraycopy(olddata, 0, position, 0, positionSize);
            for (int i = positionSize; i < position.length; i++) {
                position[i] = Integer.MAX_VALUE;
            }
        }
    }

    protected void addElementAt(int index, long element) {
        ensureCapacity(positionSize + 1);
        int numtomove = positionSize - index;
        System.arraycopy(position, index, position, index + 1, numtomove);
        position[index] = element;
        positionSize++;
    }

    protected long removeElementAt(int index) {
        long oldval = position[index];
        int numtomove = positionSize - index - 1;
        if (numtomove > 0) {
            System.arraycopy(position, index + 1, position, index, numtomove);
        }
        positionSize--;
        position[positionSize] = Integer.MAX_VALUE;
        return oldval;
    }

    @Override
    public boolean isImplementedPaste(Vector v) {
        return v instanceof FloatVector;
    }

    @Override
    public boolean isImplementedMap() {
        return true;
    }

    /**
     * On recopie tous les attributs pour que le vector ressemble exactement a
     * celui passé en argument
     */
    @Override
    public void paste(Vector v) {
        FloatVector fbv = (FloatVector) v;
        this.capacity = fbv.capacity;
        this.defaultValue = fbv.defaultValue;
        this.positionSize = fbv.positionSize;
        this.position = new long[fbv.position.length];
        System.arraycopy(fbv.position, 0, this.position, 0,
                this.position.length);
        this.data.clear();
        this.data.addAll(fbv.data);
    }

    /**
     * on applique sur chaque donnée existante et sur default
     */
    @Override
    public void map(MapFunction f) {
        // on commence toujours par modifier la valeur par defaut
        // car les valeurs suivante pourrait prendre cette valeur
        // et donc disparaitre des tableaux si besoin
        defaultValue = (float) f.apply(defaultValue);
        // on fait la boucle a l'envers au cas ou on supprime des valeurs
        for (int i = data.size() - 1; i >= 0; i--) {
            double value = f.apply(data.get(i));
            if (value == defaultValue) {
                // il etait present, on supprime l'element
                removeElementAt(i);
                data.removeElementAt(i);
            } else {
                // il etait deja present, on modifie la valeur
                data.set(i, (float) value);
            }
        }
    }

    @Override
    public VectorIterator iterator() {
        return new VectorIteratorImpl(this);
    }

    @Override
    public VectorIterator iteratorNotZero() {
        return new VectorIteratorImpl(this, 0);
    }

    @Override
    public void forEachNotZero(VectorForEachFunction f) {
        if (defaultValue == 0) {
            for (long i = 0, max = getNumberOfAssignedValue(); i < max; i++) {
                f.apply(i, getValue(i));
            }
        } else {
            Vector.super.forEachNotZero(f);
        }
    }

} // FloatVector

