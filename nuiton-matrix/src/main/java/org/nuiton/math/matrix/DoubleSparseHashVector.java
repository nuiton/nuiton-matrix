/*
 * #%L
 * Nuiton Matrix :: API
 * %%
 * Copyright (C) 2004 - 2022 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.math.matrix;

import cern.colt.function.LongProcedure;
import cern.colt.list.DoubleArrayList;
import cern.colt.list.LongArrayList;
import cern.colt.map.OpenLongDoubleHashMap;

import java.util.Arrays;

/**
 * Permet de stocker des données differente de la valeur par defaut (0.0) dans
 * une Map&lt;int, double&gt;. L'implantation pcj ne supporte pas la modification
 * de la valeur par defaut.
 *
 * On permet que la matrice est plus de {@link Integer#MAX_VALUE} cellules mais
 * on ne permet pas que la matrice est plus que {@link Integer#MAX_VALUE} valeur
 * différente que le default value.
 *
 * Created: 05 septembre 2012
 *
 * @author Benjamin POUSSIN &lt;poussin@codelutin.com&gt;
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class DoubleSparseHashVector implements SparseVector { // FloatVector
    protected double defaultValue = 0;

    protected OpenLongDoubleHashMap data;
    protected long capacity;

    public DoubleSparseHashVector() {
    }

    public DoubleSparseHashVector(long capacity) {
        this(capacity, 0);
    }

    public DoubleSparseHashVector(long capacity, double defaultValue) {
        init(capacity);
        this.defaultValue = defaultValue;
    }

    @Override
    public void init(long capacity) {
        if (data == null) {
            this.capacity = capacity;
            data = new OpenLongDoubleHashMap();
        }
    }

    @Override
    public String getInfo() {
        return "Double vector sparse(Colt): " + data.size() + "/" + size();
    }

    @Override
    public long getNumberOfAssignedValue() {
        return data.size();
    }

    @Override
    public long size() {
        return capacity;
    }

    @Deprecated
    @Override
    public double getMaxOccurence() {
        return getMaxOccurrence();
    }

    // poussin 20060827 TODO: verifier l'implantation, il semble quelle soit
    // fausse et ne puisse pas recherche le nombre max correctement
    @Override
    public double getMaxOccurrence() {
        double result = defaultValue;

        // si potentiellement il y a plus d'element identique dans data
        // que de valeur par defaut, on recherche la valeur possible
        if (this.capacity < 2 * data.size()) {
            DoubleArrayList list = new DoubleArrayList(data.size());
            data.values(list);
            list.sort();
             // !ATTENTION tmp est la representation exact de la liste, il y a possiblement plus de case dans le tableau
            double[] tmp = list.elements();
            int length = list.size();
//            Arrays.sort(tmp);

            // le nombre de fois que l'on a rencontrer la valeur la plus
            // nombreuse
            int max = 1;
            // le nombre de fois que l'on a rencontrer la valeur courante
            int count = 1;
            // la valeur la plus rencontrer
            result = tmp[0];
            // la valeur que l'on vient de traiter précédement
            double old = tmp[0];
            // la valeur courante lu dans le tableaux
            double current = tmp[0];
            // tant que l'on peut encore trouve un element plus nombreux dans le
            // tableau on le parcours
            for (int i = 1; max < length - i + count && i < length; i++) {
                current = tmp[i];

                if (current == old) {
                    count++;
                } else {
                    if (count > max) {
                        max = count;
                        result = old;
                    }
                    count = 1;
                    old = current;
                }
            }
            if (count > max) {
                max = count;
                result = current;
            }

            if (max <= capacity - length) {
                // en fin de compte, il n'y a pas plus d'element identique
                // dans data que de defaultValue
                result = defaultValue;
            }
        }

        return result;
    }

    protected void checkPos(long pos) {
        if (pos < 0 || pos >= capacity) {
            throw new IllegalArgumentException("pos " + pos + " is not in [0, "
                    + capacity + "]");
        }
    }

    @Override
    public double getValue(long pos) {
        checkPos(pos);
        double result = data.get(pos);
        // result = 0, s'il n'est pas trouvé dans la matrix
        // il faut donc faire la différence entre 0 (absent) et 0 valeur enregistrée
        // pour dans le cas de l'absence le remplacer par defaultValue,
        // sauf si defaultValue est lui-même 0
        if (result == 0 && defaultValue != 0 && !data.containsKey(pos)) {
            result = defaultValue;
        }
        return result;
    }

    @Override
    public void setValue(long pos, double value) {
        checkPos(pos);

        if (value == defaultValue) {
            // il est egal a default on supprime l'element
            data.removeKey(pos);
        } else {
            // sinon on l'ajoute
            data.put(pos, value);
        }
    }

    @Override
    public boolean equals(Object o) {
        boolean result = this == o;
        if (!result) {
            if (o instanceof DoubleSparseHashVector && defaultValue == ((DoubleSparseHashVector)o).defaultValue) {
                DoubleSparseHashVector other = (DoubleSparseHashVector) o;
                result = data.equals(other.data);
            } else if (o instanceof Vector) {
                Vector other = (Vector) o;
                result = true;
                for (int i = 0; i < size() && result; i++) {
                    result = getValue(i) == other.getValue(i);
                }
            }
        }
        return result;
    }

    @Override
    public int hashCode() {
        return Long.hashCode(size());
    }

    @Override
    public boolean isImplementedPaste(Vector v) {
        return size() == v.size();
    }

    @Override
    public boolean isImplementedMap() {
        return true;
    }

    @Override
    public void paste(Vector v) {
        // this et v on la meme taille
        if (v instanceof DoubleSparseHashVector) {
            DoubleSparseHashVector vHash = (DoubleSparseHashVector)v;
            defaultValue = vHash.defaultValue;
            data = (OpenLongDoubleHashMap)vHash.data.copy();
        } else {
            defaultValue = 0;
            for (VectorIterator i=v.iteratorNotZero(); i.hasNext();) {
                double val = i.next();
                long pos = i.getPosition();
                setValue(pos, val);
            }
        }
    }

    @Override
    public void map(final MapFunction f) {
        // on commence toujours par modifier la valeur par defaut
        // car les valeurs suivante pourrait prendre cette valeur
        // et donc disparaitre des tableaux si besoin
        defaultValue = f.apply(defaultValue);

        data.forEachKey(new LongProcedure() {
            @Override
            public boolean apply(long i) {
                double value = data.get(i);
                value = f.apply(value);
                if (value == defaultValue) {
                    data.removeKey(i);
                } else {
                    data.put(i, value);
                }
                return true;
            }
        });
    }

    @Override
    public long[] getAssignedPosition() {
        long[] result = data.keys().elements();
        Arrays.sort(result);
        return result;
    }

    @Override
    public double[] getAssignedValue() {
        double[] result = new double[data.size()];
        data.pairsSortedByKey(new LongArrayList(data.size()), new DoubleArrayList(result));
        return result;
    }

    @Override
    public double getDefaultValue() {
        return defaultValue;
    }

    @Override
    public VectorIterator iterator() {
        return new SparseHashVectorIterator(this);
    }

    @Override
    public VectorIterator iteratorNotZero() {
        return new SparseHashVectorIterator(this, 0);
    }

    @Override
    public void forEachNotZero(VectorForEachFunction f) {
        if (defaultValue == 0) {
            data.forEachPair((i, val) -> {
                f.apply(i, val);
                return true;
            });
        } else {
            SparseVector.super.forEachNotZero(f);
        }
    }

} 
