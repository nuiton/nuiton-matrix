/*
 * #%L
 * Nuiton Matrix :: API
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.math.matrix;

/**
 * Classe permettant de diminuer au maximum l'allocation de tableau de int pour
 * les dimensions. Cette classe n'est pas multithread, il faut créer une
 * instance par thread. Il faut bien réfléchir lorsque l'on souhaite utiliser
 * cette classe, par exemple il ne faut pas l'utiliser dans une méthode qui
 * appelle une autre methode réutilisant le meme objet, car il sera alors
 * modifier alors qu'il ne le faut pas pour la premiere methode appelée.
 * 
 * Created: 29 oct. 2004
 *
 * @author Benjamin Poussin &lt;poussin@codelutin.com&gt;
 * @version $Revision$
 *
 * Mise a jour: $Date$
 * par : $Author$
 */
public class DimensionHelper { // DimensionHelper

    protected int[] d1 = new int[1];
    protected int[] d2 = new int[2];
    protected int[] d3 = new int[3];
    protected int[] d4 = new int[4];

    protected Object[] o1 = new Object[1];
    protected Object[] o2 = new Object[2];
    protected Object[] o3 = new Object[3];
    protected Object[] o4 = new Object[4];

    public DimensionHelper() {
    }

    public int[] get(int x) {
        d1[0] = x;
        return d1;
    }

    public int[] get(int x, int y) {
        d2[0] = x;
        d2[1] = y;
        return d2;
    }

    public int[] get(int x, int y, int z) {
        d3[0] = x;
        d3[1] = y;
        d3[2] = z;
        return d3;
    }

    public int[] get(int x, int y, int z, int t) {
        d4[0] = x;
        d4[1] = y;
        d4[2] = z;
        d4[3] = t;
        return d4;
    }

    public Object[] get(Object x) {
        o1[0] = x;
        return o1;
    }

    public Object[] get(Object x, Object y) {
        o2[0] = x;
        o2[1] = y;
        return o2;
    }

    public Object[] get(Object x, Object y, Object z) {
        o3[0] = x;
        o3[1] = y;
        o3[2] = z;
        return o3;
    }

    public Object[] get(Object x, Object y, Object z, Object t) {
        o4[0] = x;
        o4[1] = y;
        o4[2] = z;
        o4[3] = t;
        return o4;
    }

} // DimensionHelper

