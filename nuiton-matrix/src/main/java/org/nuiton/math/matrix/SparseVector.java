package org.nuiton.math.matrix;

/*
 * #%L
 * Nuiton Matrix :: API
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

/**
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public interface SparseVector extends Vector {

    /**
     * Value to used if not un assigned value
     *
     * @return
     */
    double getDefaultValue();

    /**
     * Return an orderer array of position assigned, this array can be longer than
     * realy assigned value, you must used getNumberOfAssignedValue to know
     * number of significant position in array
     *
     * @return
     */
    long[] getAssignedPosition();

    /**
     * Return an array in same order that getAssignedPosition. This array contains values.
     * This array can be longer than realy assigned value, you must used
     * getNumberOfAssignedValue to know number of significant position in array
     * 
     * @return 
     */
    double[] getAssignedValue();
}
