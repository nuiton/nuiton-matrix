/*
 * #%L
 * Nuiton Matrix :: API
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.math.matrix;

import java.util.List;

/**
 * MatrixIteratorImpl.
 *
 * Created: 28 oct. 2004
 *
 * @author Benjamin Poussin &lt;poussin@codelutin.com&gt;
 * @version $Revision$
 *
 * Mise a jour: $Date$
 * par : $Author$
 */
public class MatrixIteratorImpl implements MatrixIterator { // MatrixIteratorImpl

    protected BasicMatrixIterator iterator = null;
    protected List<?>[] semantics = null;
    protected int pos = 0;
    protected Object[] posSems;

    /**
     * @param iterator la matrice sur lequel l'iterator doit travailler
     * @param semantics la semantique de matrix, si matrix n'a pas de semantique
     *            alors il faut passer null
     */
    public MatrixIteratorImpl(BasicMatrixIterator iterator, List<?>[] semantics) {
        this.iterator = iterator;
        this.semantics = semantics;
        pos = 0;
        if (semantics != null) {
            posSems = new Object[semantics.length];
        }
    }

    @Override
    public boolean hasNext() {
        return iterator.hasNext();
    }

    @Override
    public boolean next() {
        return iterator.next();
    }

    @Override
    public int[] getCoordinates() {
        return iterator.getCoordinates();
    }

    @Override
    public double getValue() {
        return iterator.getValue();
    }

    @Override
    public void setValue(double value) {
        iterator.setValue(value);
    }

    @Override
    public Object[] getSemanticsCoordinates() {
        Object[] result = null;
        if (semantics != null) {
            int[] coordinates = getCoordinates();
            result = MatrixHelper.dimensionToSemantics(posSems, semantics,
                    coordinates);
        }
        return result;
    }

} // MatrixIteratorImpl

