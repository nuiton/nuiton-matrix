/*
 * #%L
 * Nuiton Matrix :: API
 * %%
 * Copyright (C) 2012 - 2020 CodeLutin, Poussin Benjamin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.math.matrix;

import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.util.HashList;

/**
 * Permet d'interpreter les semantics, par exemple convertir un Id en une
 * entite via le SemanticMapper passe en argument
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class MatrixSemanticsDecorator implements MatrixND {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(MatrixSemanticsDecorator.class);
    
    protected MatrixND matrix;
    protected SemanticsDecorator decorator;
    protected List[] cacheSemantics;

    /**
     *
     * @param matrix La matrice dont il faut decorer les semantiques
     * @param decorator l'objet utilise pour la decoration
     */
    public MatrixSemanticsDecorator(MatrixND matrix, SemanticsDecorator decorator) {
        this.matrix = matrix;
        this.decorator = decorator;
        // initialise cache with array of null
        cacheSemantics = new ArrayList[matrix.getDimCount()];
    }

    @Override
    public long getNumberOfAssignedValue() {
        return matrix.getNumberOfAssignedValue();
    }

    /**
     * Les matrices retournees par cette classe doivent etre wrapper pour que
     * les semantiques continues de fonctionner
     * 
     * @param m
     * @return
     */
    protected MatrixND wrap(MatrixND m) {
        return new MatrixSemanticsDecorator(m, decorator);
    }

    @Override
    public String toString() {
        return matrix.toString();
    }

    @Override
    public MatrixFactory getFactory() {
        return matrix.getFactory();
    }

    @Override
    public List[] getSemantics() {
        // force cache creation for missing sems
        for (int i=0, maxi=cacheSemantics.length; i<maxi; i++) {
            if (cacheSemantics[i] == null) {
                // force cache creation
                getSemantic(i);
            }
        }
        return cacheSemantics;
    }

    @Override
    public List getSemantics(int dim) {
        return getSemantic(dim);
    }

    @Override
    public List getSemantic(int dim) {
        List result = cacheSemantics[dim];
        if (result == null) {
            List tmp = matrix.getSemantic(dim);
            cacheSemantics[dim] = result = new HashList(tmp.size());
            for (Object o : tmp) {
                o = decorator.decorate(o);
                result.add(o);
            }
        }
        return result;
    }

    @Override
    public <E> void setSemantics(int dim, List<E> sem) {
        setSemantic(dim, sem);
    }

    @Override
    public <E> void setSemantic(int dim, List<E> sem) {
        // remove from cache, cache recreated when needed, this prevent copy of sem never used
        cacheSemantics[dim] = null;

        List undecorate = new ArrayList(sem.size());
        for (Object o : sem) {
            o = decorator.undecorate(o);
            undecorate.add(o);
        }
        matrix.setSemantic(dim, undecorate);
    }

    @Override
    public void setName(String name) {
        matrix.setName(name);
    }

    @Override
    public String getName() {
        return matrix.getName();
    }

    @Override
    public void setDimensionName(String[] names) {
        matrix.setDimensionName(names);
    }

    @Override
    public void setDimensionNames(String[] names) {
        matrix.setDimensionNames(names);
    }

    @Override
    public String[] getDimensionName() {
        return matrix.getDimensionName();
    }

    @Override
    public String[] getDimensionNames() {
        return matrix.getDimensionNames();
    }

    @Override
    public void setDimensionName(int dim, String name) {
        matrix.setDimensionName(dim, name);
    }

    @Override
    public String getDimensionName(int dim) {
        return matrix.getDimensionName(dim);
    }

    @Override
    public double getMaxOccurence() {
        return matrix.getMaxOccurence();
    }

    @Override
    public double getMaxOccurrence() {
        return matrix.getMaxOccurrence();
    }

    @Override
    public int getNbDim() {
        return matrix.getNbDim();
    }

    @Override
    public int getDimCount() {
        return matrix.getDimCount();
    }

    @Override
    public int[] getDim() {
        return matrix.getDim();
    }

    @Override
    public int getDim(int d) {
        return matrix.getDim(d);
    }

    @Override
    public long size() {
        return matrix.size();
    }

    @Override
    public MatrixIterator iterator() {
        return new SemanticsDecoratorMatrixIterator(this, decorator, matrix.iterator());
    }

    @Override
    public MatrixIterator iteratorNotZero() {
        return new SemanticsDecoratorMatrixIterator(this, decorator, matrix.iteratorNotZero());
    }

    @Override
    public MatrixND map(MapFunction f) {
        return wrap(matrix.map(f));
    }

    @Override
    public double getValue(int[] dimensions) {
        return matrix.getValue(dimensions);
    }

    @Override
    public double getValue(int x) {
        return matrix.getValue(x);
    }

    @Override
    public double getValue(int x, int y) {
        return matrix.getValue(x, y);
    }

    @Override
    public double getValue(int x, int y, int z) {
        return matrix.getValue(x, y, z);
    }

    @Override
    public double getValue(int x, int y, int z, int t) {
        return matrix.getValue(x, y, z, t);
    }

    @Override
    public double getValue(Object[] coordinates) {
        Object[] tmp = new Object[coordinates.length];
        for (int i=0; i<tmp.length; i++) {
            tmp[i] = decorator.undecorate(coordinates[i]);
        }
        return matrix.getValue(tmp);
    }

    @Override
    public double getValue(Object x) {
        return matrix.getValue(decorator.undecorate(x));
    }

    @Override
    public double getValue(Object x, Object y) {
        return matrix.getValue(
                decorator.undecorate(x),
                decorator.undecorate(y));
    }

    @Override
    public double getValue(Object x, Object y, Object z) {
        return matrix.getValue(
                decorator.undecorate(x),
                decorator.undecorate(y),
                decorator.undecorate(z));
    }

    @Override
    public double getValue(Object x, Object y, Object z, Object t) {
        return matrix.getValue(
                decorator.undecorate(x),
                decorator.undecorate(y),
                decorator.undecorate(z),
                decorator.undecorate(t));
    }

    @Override
    public void setValue(int[] dimensions, double d) {
        matrix.setValue(dimensions, d);
    }

    @Override
    public void setValue(int x, double d) {
        matrix.setValue(x, d);
    }

    @Override
    public void setValue(int x, int y, double d) {
        matrix.setValue(x, y, d);
    }

    @Override
    public void setValue(int x, int y, int z, double d) {
        matrix.setValue(x, y, z, d);
    }

    @Override
    public void setValue(int x, int y, int z, int t, double d) {
        matrix.setValue(x, y, z, t, d);
    }

    @Override
    public void setValue(Object[] coordinates, double d) {
        Object[] tmp = new Object[coordinates.length];
        for (int i=0; i<tmp.length; i++) {
            tmp[i] = decorator.undecorate(coordinates[i]);
        }

        matrix.setValue(tmp, d);
    }

    @Override
    public void setValue(Object x, double d) {
        matrix.setValue(
                decorator.undecorate(x),
                d);
    }

    @Override
    public void setValue(Object x, Object y, double d) {
        matrix.setValue(
                decorator.undecorate(x),
                decorator.undecorate(y),
                d);
    }

    @Override
    public void setValue(Object x, Object y, Object z, double d) {
        matrix.setValue(
                decorator.undecorate(x),
                decorator.undecorate(y),
                decorator.undecorate(z),
                d);
    }

    @Override
    public void setValue(Object x, Object y, Object z, Object t, double d) {
        matrix.setValue(
                decorator.undecorate(x),
                decorator.undecorate(y),
                decorator.undecorate(z),
                decorator.undecorate(t),
                d);
    }

    @Override
    public MatrixND copy() {
        return wrap(matrix.copy());
    }

    @Override
    public MatrixND clone() {
        return wrap(matrix.clone());
    }

    @Override
    public double sumAll() {
        return matrix.sumAll();
    }

    @Override
    public MatrixND sumOverDim(int dim) {
        MatrixND tmp = matrix.sumOverDim(dim);
        MatrixND result = wrap(tmp);
        // on force la non decoration de la dimension modifier
        result.setSemantic(dim, tmp.getSemantic(dim));
        return result;
    }

    @Override
    public MatrixND sumOverDim(int dim, int step) {
        MatrixND tmp = matrix.sumOverDim(dim, step);
        MatrixND result = wrap(tmp);
        // on force la non decoration de la dimension modifier
        result.setSemantic(dim, tmp.getSemantic(dim));
        return result;
    }

    @Override
    public MatrixND sumOverDim(int dim, int start, int nb) {
        MatrixND tmp = matrix.sumOverDim(dim, start, nb);
        MatrixND result = wrap(tmp);
        // on force la non decoration de la dimension modifier
        result.setSemantic(dim, tmp.getSemantic(dim));
        return result;
    }

    @Override
    public double meanAll() {
        return matrix.meanAll();
    }

    @Override
    public MatrixND meanOverDim(int dim) {
        MatrixND tmp = matrix.meanOverDim(dim);
        MatrixND result = wrap(tmp);
        // on force la non decoration de la dimension modifier
        result.setSemantic(dim, tmp.getSemantic(dim));
        return result;
    }

    @Override
    public MatrixND meanOverDim(int dim, int step) {
        MatrixND tmp = matrix.meanOverDim(dim, step);
        MatrixND result = wrap(tmp);
        // on force la non decoration de la dimension modifier
        result.setSemantic(dim, tmp.getSemantic(dim));
        return result;
    }

    @Override
    public MatrixND cut(int dim, int[] toCut) {
        return wrap(matrix.cut(dim, toCut));
    }

    @Override
    public MatrixND paste(MatrixND mat) {
        matrix.paste(mat);
        return this;
    }

    @Override
    public MatrixND paste(int[] origin, MatrixND mat) {
        matrix.paste(origin, mat);
        return this;
    }

    @Override
    public MatrixND pasteSemantics(MatrixND mat) {
        if (mat != null) {
            for (MatrixIterator mi = mat.iterator(); mi.next();) {
                Object[] sems = mi.getSemanticsCoordinates();
                if (MatrixHelper.isValidCoordinates(getSemantics(), sems)) {
                    setValue(sems, mi.getValue());
                }
            }
        }
        return this;
    }

    @Override
    public MatrixND getSubMatrix(int dim, Object start, int nb) {
        start = decorator.undecorate(start);
        return wrap(matrix.getSubMatrix(dim, start, nb));
    }

    @Override
    public MatrixND getSubMatrix(int dim, int start, int nb) {
        return wrap(matrix.getSubMatrix(dim, start, nb));
    }

    @Override
    public MatrixND getSubMatrix(int dim, Object... elem) {
        Object[] tmp = new Object[elem.length];
        for (int i=0; i<tmp.length; i++) {
            tmp[i] = decorator.undecorate(elem[i]);
        }
        return wrap(matrix.getSubMatrix(dim, tmp));
    }

    @Override
    public MatrixND getSubMatrix(int dim, int[] elem) {
        return wrap(matrix.getSubMatrix(dim, elem));
    }

    @Override
    public MatrixND getSubMatrix(Object[]... elem) {
        Object[] tmp = new Object[elem.length];
        for (int i=0; i<tmp.length; i++) {
            tmp[i] = decorator.undecorate(elem[i]);
        }
        return wrap(matrix.getSubMatrix(tmp));
    }

    @Override
    public MatrixND getSubMatrix(int[]... elems) {
        return wrap(matrix.getSubMatrix(elems));
    }

    @Override
    public MatrixND add(MatrixND m) {
        matrix.add(m);
        return this;
    }

    @Override
    public MatrixND minus(MatrixND m) {
        matrix.minus(m);
        return this;
    }

    @Override
    public MatrixND transpose() {
        return wrap(matrix.transpose());
    }

    @Override
    public MatrixND reduce() {
        return wrap(matrix.reduce());
    }

    @Override
    public MatrixND reduce(int minNbDim) {
        return wrap(matrix.reduce(minNbDim));
    }

    @Override
    public MatrixND reduceDims(int... dims) {
        return wrap(matrix.reduceDims(dims));
    }

    @Override
    public MatrixND mult(MatrixND m) {
        matrix.mult(m);
        return this;
    }

    @Override
    public MatrixND mults(double d) {
        matrix.mults(d);
        return this;
    }

    @Override
    public MatrixND divs(double d) {
        matrix.divs(d);
        return this;
    }

    @Override
    public MatrixND adds(double d) {
        matrix.adds(d);
        return this;
    }

    @Override
    public MatrixND minuss(double d) {
        matrix.minuss(d);
        return this;
    }

    @Override
    public List<?> toList() {
        return matrix.toList();
    }

    @Override
    public void fromList(List<?> list) {
        matrix.fromList(list);
    }

    @Override
    public boolean isSupportedCSV() {
        return matrix.isSupportedCSV();
    }

    @Override
    public void importCSV(Reader reader, int[] origin) throws IOException {
        matrix.importCSV(reader, origin);
    }

    @Override
    public void importCSV(Reader reader, int[] origin, String matrixName) throws IOException {
        matrix.importCSV(reader, origin, matrixName);
    }

    @Override
    public void importCSV(File file, int[] origin) throws IOException {
        matrix.importCSV(file, origin);
    }

    @Override
    public void exportCSV(Writer writer, boolean withSemantics) throws IOException {
        matrix.exportCSV(writer, withSemantics);
    }

    @Override
    public void exportCSVND(Writer writer, boolean withSemantics) throws IOException {
        matrix.exportCSVND(writer, withSemantics);
    }

    @Override
    public boolean equalsValues(MatrixND mat) {
        return matrix.equalsValues(mat);
    }

    static public class SemanticsDecoratorMatrixIterator implements MatrixIterator {

        protected SemanticsDecorator decorator;
        protected MatrixIterator iterator;
        // use same array for coordinates to prevent array instanciation
        protected Object[] semArray;

        public SemanticsDecoratorMatrixIterator(MatrixND mat, SemanticsDecorator decorator, MatrixIterator iterator) {
            this.decorator = decorator;
            this.iterator = iterator;
            semArray = new Object[mat.getDimCount()];
        }

        @Override
        public Object[] getSemanticsCoordinates() {
            Object[] tmp = iterator.getSemanticsCoordinates();
            for (int i=0; i<tmp.length; i++) {
                semArray[i] = decorator.decorate(tmp[i]);
            }
            return semArray;
        }

        @Override
        public boolean hasNext() {
           return iterator.hasNext();
        }

        @Override
        public boolean next() {
            return iterator.next();
        }

        @Override
        public int[] getCoordinates() {
            return iterator.getCoordinates();
        }

        @Override
        public double getValue() {
            return iterator.getValue();
        }

        @Override
        public void setValue(double value) {
            iterator.setValue(value);
        }

    }
}
