package org.nuiton.math.matrix;

/*
 * #%L
 * Nuiton Matrix :: API
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

/**
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public abstract class SparseVectorIterator implements VectorIterator {

    protected boolean hasExclude = false;
    protected double exclude;
    protected SparseVector vector;
    protected long size;
    protected int assignedPos = 0;
    protected long assignedSize;
    protected double defaultValue;
    protected long pos = -1;
    protected long nextPos = -1;
    protected double value;
    protected double nextValue;

    public SparseVectorIterator(SparseVector vector) {
        this.vector = vector;
        this.size = vector.size();
        this.defaultValue = vector.getDefaultValue();
        this.assignedSize = vector.getNumberOfAssignedValue();
    }

    public SparseVectorIterator(SparseVector vector, double exclude) {
        this(vector);
        setExclude(exclude);
    }

    abstract long getAssignedPosition(int pos);
    abstract double getAssignedValues(int pos);

    @Override
    public void setExclude(double exclude) {
        this.hasExclude = true;
        this.exclude = exclude;
    }

    protected void computeNextPosAndValue() {
        // compute nextPos only if not already computed (multiple call to hasNext must be possible.
        if (nextPos <= pos) {
            if (hasExclude && exclude == defaultValue) {
                // return only values in assigned values
                if (assignedPos < assignedSize) {
                    nextPos = getAssignedPosition(assignedPos);
                    nextValue = getAssignedValues(assignedPos);
                    assignedPos += 1;
                } else {
                    // no more assigned value
                    nextPos = Long.MAX_VALUE;
                }
            } else {
                // return all values
                do {
                    nextPos += 1;
                    if (assignedPos < assignedSize-1 && getAssignedPosition(assignedPos) < nextPos) {
                        assignedPos += 1;
                    }
                    if (nextPos < size) {
                        nextValue = getValue(nextPos);
                    }
                } while (nextPos < size && hasExclude && nextValue == exclude);
            }
        }
    }

    protected double getValue(long pos) {
        double result = defaultValue;
        if (assignedPos < assignedSize && pos == getAssignedPosition(assignedPos)) {
            result = getAssignedValues(assignedPos);
        }
        return result;
    }

    @Override
    public boolean hasNext() {
        computeNextPosAndValue();
        return nextPos < size;
    }

    @Override
    public double next() {
        computeNextPosAndValue();
        pos = nextPos;
        value = nextValue;
        return value;
    }

    @Override
    public double getValue() {
        return value;
    }

    @Override
    public void setValue(double value) {
        this.value = value; // set value if user call getValue after set
        vector.setValue(pos, value);
    }

    @Override
    public long getPosition() {
        return pos;
    }

}
