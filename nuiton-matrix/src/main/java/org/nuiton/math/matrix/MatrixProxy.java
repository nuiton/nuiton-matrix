/*
 * #%L
 * Nuiton Matrix :: API
 * %%
 * Copyright (C) 2010 - 2012 Codelutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.math.matrix;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.ObjectUtils;

/**
 * Matrix proxy contains only dimension and semantics definition, but
 * does not allocate memory spaces for value.
 * Call to getSubMatrix method will return a really allocated memory
 * matrix and use a {@link MatrixProvider} to fill matrix value.
 * 
 * This is usefull for huge matrix that need to not be filled at first
 * use, but after some dimensions reductions.
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 * 
 * @see MatrixProvider
 */
public class MatrixProxy extends AbstractMatrixND {

    /** serialVersionUID. */
    private static final long serialVersionUID = 2338394722090201478L;

    protected MatrixProvider matrixProvider;

    protected MatrixProxy(MatrixFactory factory, int[] dim) {
        super(factory, dim);
    }

    protected MatrixProxy(MatrixFactory factory, List<?>[] semantics) {
        super(factory, semantics);
    }

    protected MatrixProxy(MatrixFactory factory, String name, int[] dim) {
        super(factory, name, dim);
    }

    protected MatrixProxy(MatrixFactory factory, String name, int[] dim,
            String[] dimNames) {
        super(factory, name, dim, dimNames);
    }

    protected MatrixProxy(MatrixFactory factory, String name,
            List<?>[] semantics) {
        super(factory, name, semantics);
    }

    protected MatrixProxy(MatrixFactory factory, String name,
            List<?>[] semantics, String[] dimNames) {
        super(factory, name, semantics, dimNames);
    }

    @Override
    public long getNumberOfAssignedValue() {
        return 0;
    }

    public MatrixProvider getMatrixProvider() {
        return matrixProvider;
    }

    public void setMatrixProvider(MatrixProvider matrixProvider) {
        this.matrixProvider = matrixProvider;
    }

    /*
     * @see org.nuiton.math.matrix.AbstractMatrixND#iterator()
     */
    @Override
    public MatrixIterator iterator() {
        throw new UnsupportedOperationException("Not implemented on matrix proxy");
    }

    /*
     * @see org.nuiton.math.matrix.AbstractMatrixND#iteratorNotZero()
     */
    @Override
    public MatrixIterator iteratorNotZero() {
        throw new UnsupportedOperationException("Not implemented on matrix proxy");
    }

    /*
     * @see org.nuiton.math.matrix.AbstractMatrixND#getValue(int[])
     */
    @Override
    public double getValue(int[] coordinates) {
        throw new UnsupportedOperationException("Not implemented on matrix proxy");
    }

    /*
     * @see org.nuiton.math.matrix.AbstractMatrixND#setValue(int[], double)
     */
    @Override
    public void setValue(int[] coordinates, double d) {
        throw new UnsupportedOperationException("Not implemented on matrix proxy");
    }

    @Override
    public MatrixND getSubMatrix(int dim, int start, int nb) {
        //TODO echatellier 20101216 could be implemented
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public MatrixND getSubMatrix(int dim, Object... elem) {
        //TODO echatellier 20101216 could be implemented
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public MatrixND getSubMatrix(Object[]... elems) {
        
        // convert semantics to indices
        int[][] elemIndices = new int[elems.length][];
        for (int i = 0 ; i < elemIndices.length ; ++i) {
            if (elems[i] != null) {
                elemIndices[i] = new int[elems[i].length];
                for (int j = 0; j < elems[i].length; j++) {
                    elemIndices[i][j] = MatrixHelper.indexOf(getSemantics(), i, elems[i][j]);
                }
            }
        }
        
        MatrixND subMatrix = getSubMatrix(elemIndices);
        return subMatrix;
    }

    @Override
    public MatrixND getSubMatrix(int dim, int[] elem) {
        //TODO echatellier 20101216 could be implemented
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public MatrixND getSubMatrix(int[]... elems) {
        
        // la reduction doit se faire sur le meme nombre de dimension
        if (elems.length != dim.length) {
            throw new IllegalArgumentException(String.format("Can't get sub matrix with different dimension count (expected: %d, got %d)", dim.length, elems.length));
        }
        
        // recopie seulement des sous semantiques voulues
        // a partir des semantics du parent (super)
        List<Object>[] subSem = new List[elems.length];
        for (int iDim = 0 ; iDim < elems.length ; iDim++) {
            subSem[iDim] = new ArrayList<Object>();
            List<?> semantics = super.semantics[iDim];
            
            for (int iElem = 0; iElem < semantics.size(); iElem++) {
                // si c'est null, on copies tout
                if (elems[iDim] == null || ArrayUtils.contains(elems[iDim], iElem)) {
                    subSem[iDim].add(semantics.get(iElem));
                }
            }
        }
        
        MatrixND subMatrix = factory.create(name, subSem, dimNames);
        matrixProvider.fillValues(subMatrix);
        return subMatrix;
    }

    /*
     * Idem, mais ne compare pas les valeurs.
     */
    @Override
    public boolean equals(MatrixND mat) {
        boolean result = true;
        // le nom doit être le même
        result = result && getName().equals(mat.getName());

        // les sémantiques doivent-être identique
        for (int i = 0; result && i < getDimCount(); i++) {
            String dimName1 = getDimensionName(i);
            String dimName2 = mat.getDimensionName(i);
            result = ObjectUtils.equals(dimName1, dimName2);

            List<?> sem1 = getSemantic(i);
            List<?> sem2 = mat.getSemantic(i);
            result = result && ObjectUtils.equals(sem1, sem2);

        }

        return result;
    }
}
