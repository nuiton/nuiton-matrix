/*
 * #%L
 * Nuiton Matrix :: API
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.math.matrix;

import java.util.Arrays;
import org.apache.commons.lang3.time.DurationFormatUtils;


/**
 * Permet de stocker des données à une position lineaire et de la redemander.
 * Cette classe ne gére que les données lineaire. L'avantage de cette classe est
 * de ne conserver que les elements differents de la valeur par defaut, ce qui
 * minimize la taille du tableau necessaire a conserver les données.
 *
 * On permet que la matrice est plus de {@link Integer#MAX_VALUE} cellules mais
 * on ne permet pas que la matrice est plus que {@link Integer#MAX_VALUE} valeur
 * différente que le default value.
 *
 * Created: 6 octobre 2005 01:29:23 CEST
 *
 * @author Benjamin POUSSIN &lt;poussin@codelutin.com&gt;
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class DoubleSparseArrayVector implements SparseVector { // DoubleSparseArrayVector

    /** la valeur par defaut */
    protected double defaultValue = 0;

    /** maximum number of element, maximum pos value */
    protected long capacity = 0;
    /** current number of assigned value */
    protected int assignedSize = 0;
    /** contient la position de l'element, le tableau est trié */
    protected long[] position;
    protected double[] values;

//    /** contient la valeur de l'element */
//    protected ArrayDoubleList data;

    public DoubleSparseArrayVector() {
    }

    
    public DoubleSparseArrayVector(long capacity) {
        init(capacity);
    }

    public DoubleSparseArrayVector(long capacity, double defaultValue) {
        this(capacity);
        this.defaultValue = defaultValue;
    }

    @Override
    public void init(long capacity) {
        if (this.values == null) {
            this.capacity = capacity;
            this.values = new double[8];
            this.position = new long[8];

            // all not used position must have MAX_VALUE to unsure that all not
            // used position are at the end of array (needed by binarySearch)
            Arrays.fill(position, Long.MAX_VALUE);
        }
    }

    @Override
    public String getInfo() {
        return "Double vector sparse: " + assignedSize + "/" + size();
    }

    @Override
    public double getDefaultValue() {
        return defaultValue;
    }

    @Override
    public long[] getAssignedPosition() {
        return position;
    }

    @Override
    public double[] getAssignedValue() {
        return values;
    }

    @Override
    public long getNumberOfAssignedValue() {
        return assignedSize;
    }

    @Override
    public long size() {
        return capacity;
    }

    @Deprecated
    @Override
    public double getMaxOccurence() {
        return getMaxOccurrence();
    }

    // poussin 20060827 TODO: verifier l'implantation, il semble quelle soit
    // fausse et ne puisse pas recherche le nombre max correctement
    @Override
    public double getMaxOccurrence() {
        // le nombre de fois que l'on a rencontrer la valeur la plus
        // nombreuse
        long max = capacity - assignedSize;

        double result = defaultValue;

        // si potentiellement il y a plus d'element identique dans data
        // que de valeur par defaut, on recherche la valeur possible
        if (this.capacity > 2 * max) {

            double[] tmp = Arrays.copyOf(values, assignedSize);

            Arrays.sort(tmp);

            // le nombre de fois que l'on a rencontrer la valeur courante
            int count = 1;
            // la valeur que l'on vient de traiter précédement
            double old = tmp[0];
            // la valeur courante lu dans le tableaux
            double current = tmp[0];
            // tant que l'on peut encore trouve un element plus nombreux dans le
            // tableau on le parcours
            for (int i = 1; max < tmp.length - i + count && i < tmp.length; i++) {
                current = tmp[i];

                if (current == old) {
                    count++;
                } else {
                    if (count > max) {
                        max = count;
                        result = old;
                    }
                    count = 1;
                    old = current;
                }
            }
            if (count > max) {
                max = count;
                result = current;
            }
        }

        return result;
    }

    protected void checkPos(long pos) {
        if (pos < 0 || pos >= capacity) {
            throw new IllegalArgumentException("pos " + pos + " is not in [0, "
                    + capacity + "]");
        }
    }

    @Override
    public double getValue(long pos) {
        checkPos(pos);

        double result = defaultValue;
        int index = findIndex(pos);
        if (index >= 0) {
            result = values[index];
        }
        return result;
    }

    @Override
    public void setValue(long pos, double value) {
        checkPos(pos);

        int index = findIndex(pos);
        if (index >= 0) {
            if (value == defaultValue) {
                // il etait present, on supprime l'element
                removeElementAt(index);
            } else {
                // il etait deja present, on modifie la valeur
                values[index] = value;
            }
        } else {
            // il n'etait pas present
            if (value != defaultValue) {
                // il faut ajouter dans position et dans data
                index = -index - 1;

                addElementAt(index, pos, value);
            }
        }
    }

    @Override
    public boolean equals(Object o) {
        boolean result = false;
        if (o instanceof DoubleSparseArrayVector && defaultValue == ((DoubleSparseArrayVector)o).defaultValue) {
            DoubleSparseArrayVector other = (DoubleSparseArrayVector) o;
            result = Arrays.equals(this.position, other.position)
                    && Arrays.equals(values, other.values);
        } else
            if (o instanceof Vector) {
            Vector other = (Vector) o;
            result = size() == other.size();
            for (int i = 0; i < size() && result; i++) {
                result = getValue(i) == other.getValue(i);
            }
        }
        return result;
    }

    @Override
    public int hashCode() {
        return Long.hashCode(size());
    }

    /**
     * retourne la position dans le tableau position de la position lineaire
     * 
     * @param pos
     * @return la position ou &lt; 0 donnant la position de l'element s'il etait
     *         present
     */
    protected int findIndex(long pos) {
        return Arrays.binarySearch(position, pos);
    }

    protected void ensureCapacity(int mincap) {
        if (mincap > position.length) {
            int newcap = (int) (position.length * 3L / 2L + 1L); // on fait le calcul en long pour éviter le dépassement lors de la multiplication

            long[] oldPosition = position;
            position = new long[newcap >= mincap ? newcap : mincap];
            System.arraycopy(oldPosition, 0, position, 0, assignedSize);
            for (int i = assignedSize; i < position.length; i++) {
                position[i] = Long.MAX_VALUE;
            }
            
            double[] oldValues = values;
            values = new double[newcap >= mincap ? newcap : mincap];
            System.arraycopy(oldValues, 0, values, 0, assignedSize);
        }
    }

    protected void addElementAt(int index, long element, double value) {
        ensureCapacity(assignedSize + 1);
        int numtomove = assignedSize - index;

        System.arraycopy(position, index, position, index + 1, numtomove);
        position[index] = element;

        System.arraycopy(values, index, values, index + 1, numtomove);
        values[index] = value;

        assignedSize++;
    }

    protected void removeElementAt(int index) {
        int numtomove = assignedSize - index - 1;
        if (numtomove > 0) {
            System.arraycopy(position, index + 1, position, index, numtomove);
            System.arraycopy(values, index + 1, values, index, numtomove);
        }
        assignedSize--;
        position[assignedSize] = Long.MAX_VALUE;
    }

    @Override
    public boolean isImplementedPaste(Vector v) {
        return v instanceof DoubleSparseArrayVector;
    }

    @Override
    public boolean isImplementedMap() {
        return true;
    }

    /**
     * On recopie tous les attributs pour que le vector ressemble exactement a
     * celui passé en argument
     */
    @Override
    public void paste(Vector v) {
        DoubleSparseArrayVector fbv = (DoubleSparseArrayVector) v;
        this.capacity = fbv.capacity;
        this.defaultValue = fbv.defaultValue;
        this.assignedSize = fbv.assignedSize;
        this.position = new long[this.assignedSize];
        this.values = new double[this.assignedSize];

        System.arraycopy(fbv.position, 0, this.position, 0,
                this.assignedSize);
        System.arraycopy(fbv.values, 0, this.values, 0,
                this.assignedSize);
    }

    /**
     * on applique sur chaque donnée existante et sur default
     */
    @Override
    public void map(MapFunction f) {
        // on commence toujours par modifier la valeur par defaut
        // car les valeurs suivante pourrait prendre cette valeur
        // et donc disparaitre des tableaux si besoin
        defaultValue = f.apply(defaultValue);
        // on fait la boucle a l'envers au cas ou on supprime des valeurs
        for (int i = assignedSize - 1; i >= 0; i--) {
            double value = f.apply(values[i]);
            if (value == defaultValue) {
                // il etait present, on supprime l'element
                removeElementAt(i);
            } else {
                // il etait deja present, on modifie la valeur
                values[i] = value;
            }
        }
    }

    @Override
    public VectorIterator iterator() {
        return new SparseArrayVectorIterator(this);
    }

    @Override
    public VectorIterator iteratorNotZero() {
            return new SparseArrayVectorIterator(this, 0);
    }

    @Override
    public void forEachNotZero(VectorForEachFunction f) {
        if (defaultValue == 0) {
            for (long i = 0, max = getNumberOfAssignedValue(); i < max; i++) {
                f.apply(i, getValue(i));
            }
        } else {
            SparseVector.super.forEachNotZero(f);
        }
    }

} // DoubleSparseArrayVector

