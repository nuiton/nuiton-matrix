package org.nuiton.math.matrix;

/*
 * #%L
 * Nuiton Matrix :: API
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

/**
 * Implantation pour les sparse qui peuvent facilement retourner des tableaux
 * trie des valeurs assignees
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class SparseArrayVectorIterator extends SparseVectorIterator {

    protected long[] assignedPosition;
    protected double[] assignedValues;

    public SparseArrayVectorIterator(SparseVector vector) {
        super(vector);
        this.assignedPosition = vector.getAssignedPosition();
        this.assignedValues = vector.getAssignedValue();
    }

    public SparseArrayVectorIterator(SparseVector vector, double exclude) {
        super(vector, exclude);
        this.assignedPosition = vector.getAssignedPosition();
        this.assignedValues = vector.getAssignedValue();
    }

    @Override
    protected long getAssignedPosition(int pos) {
        return assignedPosition[pos];
    }

    @Override
    protected double getAssignedValues(int pos) {
        return assignedValues[pos];
    }

}
