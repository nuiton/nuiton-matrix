/*
 * #%L
 * Nuiton Matrix :: API
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.math.matrix;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.RandomAccess;

/**
 * SemanticList.
 *
 * Created: 6 sept. 06 17:18:23
 *
 * @param <T> elements type
 * 
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class SemanticList<T> extends AbstractList<T> implements RandomAccess {

    protected ArrayList<T> datas = null;
    protected Map<Object, Integer> index = null;

    public SemanticList(Collection<T> c) {
        datas = new ArrayList<T>(c);
    }

    /*
     * @see java.util.AbstractList#get(int)
     */
    @Override
    public T get(int index) {
        T result = datas.get(index);
        return result;
    }

    /*
     * @see java.util.AbstractCollection#size()
     */
    @Override
    public int size() {
        int result = datas.size();
        return result;
    }

    /*
     * @see java.util.AbstractList#indexOf(java.lang.Object)
     */
    @Override
    public int indexOf(Object o) {
        Map<Object, Integer> index = getIndex();
        Integer result = index.get(o);
        int resultIndex = -1;
        if (result != null) {
            resultIndex = result.intValue();
        }
        return resultIndex;
    }

    protected Map<Object, Integer> getIndex() {
        if (index == null) {
            index = new HashMap<Object, Integer>();
            for (int i = 0; i < datas.size(); i++) {
                index.put(datas.get(i), Integer.valueOf(i));
            }
        }
        return index;
    }
}
