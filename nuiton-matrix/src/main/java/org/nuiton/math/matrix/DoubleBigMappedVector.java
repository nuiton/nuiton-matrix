/*
 * #%L
 * Nuiton Matrix :: API
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.math.matrix;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.DoubleBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * DoubleBigMappedVector.
 *
 * Utilise un fichier mapper en mémoire, qui ne supporte pas plus de Integer.MAX_VALUE
 * Donc lors de la création si la capacité demandée est supérieur une exception est levée.
 *
 * Created: 30 aout 2012 11:56:36 CEST
 *
 * @author Benjamin POUSSIN &lt;poussin@codelutin.com&gt;
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class DoubleBigMappedVector implements Vector { // DoubleBigMappedVector

    static private Log log = LogFactory.getLog(DoubleBigMappedVector.class);

    static final public int DOUBLE_SIZE = 8;

    /** le fichier temporaire creer pour la matrice, a effacer lorsque la matrice n'est plus utiliser */
    protected File file;
    protected long capacity;
    protected DoubleBuffer data = null;
    protected boolean readonly = false;

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        if (file != null) {
            file.delete();
        }
    }

    public DoubleBigMappedVector() {
    }

    /**
     * Create temp file, this file is deleted when you stop application or
     * when this instance is no more used.
     * @param capacity
     * @throws IOException
     */
    public DoubleBigMappedVector(long capacity) throws IOException {
        init(capacity);
    }

    @Override
    public String getInfo() {
        return "Double dense mapped: " + data.capacity();
    }

    @Override
    public long getNumberOfAssignedValue() {
        return data.capacity();
    }

    @Override
    public void init(long capacity) {
        if (capacity * DOUBLE_SIZE > Integer.MAX_VALUE) {
            throw new IllegalArgumentException(String.format("Mapped max capacity is '%s' asked '%s'", Integer.MAX_VALUE / DOUBLE_SIZE, capacity));
        }
        try {
            if (data == null) {
                this.capacity = capacity;
                this.file = File.createTempFile("matrix", ".mapped");
                this.file.deleteOnExit();
                RandomAccessFile raf = new RandomAccessFile(file, "rw");
                data = raf.getChannel()
                        .map(FileChannel.MapMode.READ_WRITE, 0, capacity * DOUBLE_SIZE)
                        .asDoubleBuffer();
            }
        } catch (IOException eee) {
            throw new MatrixException("Can't init vector", eee);
        }
    }

    /**
     * Utilise une partie du fichier pour stocker les informations
     * @param raf le fichier a utiliser
     * @param offset l'endroit ou l'on doit faire le stockage
     * @param capacity le nombre de double a y stocker
     * @throws IOException
     */
    public DoubleBigMappedVector(RandomAccessFile raf, long offset, long capacity) throws IOException {
        if (capacity * DOUBLE_SIZE > Integer.MAX_VALUE) {
            throw new IllegalArgumentException(String.format("Mapped max capacity is '%s' asked '%s'", Integer.MAX_VALUE / DOUBLE_SIZE, capacity));
        }

        this.capacity = capacity;
        try {
            this.data = raf.getChannel().map(
                    FileChannel.MapMode.READ_WRITE, offset, capacity*DOUBLE_SIZE).asDoubleBuffer();
        } catch (Exception eee) {
            // can't use mapped version, read array and create not mapped Buffer
            log.error("Can't use mapped file, only read available");
            this.readonly = true;
            byte[] tmp = new byte[(int) capacity * DOUBLE_SIZE];
            long currentOffset = raf.getFilePointer();
            raf.seek(offset);
            raf.readFully(tmp);
            raf.seek(currentOffset);
            ByteBuffer buf = ByteBuffer.wrap(tmp);
            this.data = buf.asDoubleBuffer();
        }
    }

    public DoubleBigMappedVector(MappedByteBuffer bytes, long capacity) {
        this(bytes.asDoubleBuffer(), capacity);
    }

    public DoubleBigMappedVector(DoubleBuffer data, long capacity) {
        this.capacity = capacity;
        this.data = data;
    }

    @Override
    public long size() {
        return capacity;
    }

    @Deprecated
    @Override
    public double getMaxOccurence() {
        return getMaxOccurrence();
    }

    @Override
    public double getMaxOccurrence() {
        // on se place au debut avant de faire la reecherche
        data.position(0);

        double[] tmp = new double[(int)capacity];
        data.get(tmp);
        return MatrixHelper.maxOccurrence(tmp);
    }

    @Override
    public double getValue(long pos) {
        return data.get((int)pos);
    }

    @Override
    public void setValue(long pos, double value) {
        if (readonly) {
            throw new MatrixException("This object is Read only, perhaps because your system (Windows?) doesn't support large mapped file");
        }
        data.put((int)pos, value);
    }

    @Override
    public boolean equals(Object o) {
        boolean result = this == o;
        if (!result) {
            if (o instanceof DoubleBigMappedVector) {
                DoubleBigMappedVector other = (DoubleBigMappedVector) o;
                // on se place au debut avant de faire la comparaison
                other.data.position(0);
                data.position(0);

                result = this.data.equals(other.data);
            } else if (o instanceof Vector) {
                Vector other = (Vector) o;
                result = true;
                for (int i = 0; i < size() && result; i++) {
                    result = getValue(i) == other.getValue(i);
                }
            }
        }
        return result;
    }

    @Override
    public boolean isImplementedPaste(Vector v) {
        return v instanceof DoubleBigMappedVector;
    }

    @Override
    public boolean isImplementedMap() {
        return true;
    }

    @Override
    public void paste(Vector src) {
        if (readonly) {
            throw new MatrixException("This object is Read only, perhaps because your system (Windows?) doesn't support large mapped file");
        }
        DoubleBigMappedVector dbmSrc = (DoubleBigMappedVector) src;
        // on se place au debut avant de faire la copie
        dbmSrc.data.position(0);
        data.position(0);

        data.put(dbmSrc.data);
    }

    @Override
    public void add(Vector v) {
        if (readonly) {
            throw new MatrixException("This object is Read only, perhaps because your system (Windows?) doesn't support large mapped file");
        }
        Vector.super.add(v);
    }

    @Override
    public void minus(Vector v) {
        if (readonly) {
            throw new MatrixException("This object is Read only, perhaps because your system (Windows?) doesn't support large mapped file");
        }
        Vector.super.minus(v);
    }

    @Override
    public void map(MapFunction f) {
        if (readonly) {
            throw new MatrixException("This object is Read only, perhaps because your system (Windows?) doesn't support large mapped file");
        }
        for (int i = 0; i < capacity; i++) {
            double v = data.get(i);
            v = f.apply(v);
            data.put(i, v);
        }
    }

    @Override
    public VectorIterator iterator() {
        return new VectorIteratorImpl(this);
    }

    @Override
    public VectorIterator iteratorNotZero() {
            return new VectorIteratorImpl(this, 0);
    }

    @Override
    public void forEach(VectorForEachFunction f) {
        if (readonly) {
            throw new MatrixException("This object is Read only, perhaps because your system (Windows?) doesn't support large mapped file");
        }
        Vector.super.forEach(f);
    }

    @Override
    public void forEachNotZero(VectorForEachFunction f) {
        if (readonly) {
            throw new MatrixException("This object is Read only, perhaps because your system (Windows?) doesn't support large mapped file");
        }
        Vector.super.forEachNotZero(f);
    }

}
