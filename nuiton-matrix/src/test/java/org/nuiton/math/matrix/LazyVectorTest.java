package org.nuiton.math.matrix;

/*
 * #%L
 * Nuiton Matrix :: API
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */


import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class LazyVectorTest {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(LazyVectorTest.class);

    @Test
    public void testCopy() {
        LazyVector l1 = new LazyVector(new DoubleBigVector(), 10);
        LazyVector l2 = new LazyVector(new DoubleBigVector(), 10);

        l2.paste(l1);

        l1.setValue(5, 5);
        l2.setValue(5, -5);

        Assert.assertEquals(5, l1.getValue(5), 0);
        Assert.assertEquals(-5, l2.getValue(5), 0);

        LazyVector l3 = new LazyVector(new DoubleBigVector(), 10);
        l3.paste(l1);

        l1.setValue(5, -5);

        Assert.assertEquals(-5, l1.getValue(5), 0);
        Assert.assertEquals(5, l3.getValue(5), 0);

        l3.setValue(4, 4);

        Assert.assertEquals(-5, l1.getValue(5), 0);
        Assert.assertEquals(5, l3.getValue(5), 0);
        Assert.assertEquals(0, l1.getValue(4), 0);
        Assert.assertEquals(4, l3.getValue(4), 0);
    }
}
