/*
 * #%L
 * Nuiton Matrix :: API
 * %%
 * Copyright (C) 2004 - 2023 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.math.matrix;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.nuiton.i18n.I18n;
import org.nuiton.i18n.init.DefaultI18nInitializer;

/**
 * Test de l'import et export CSV.
 * 
 * @author ruchaud
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class ImportExportMatrixTest {

    /** One dimension matrix. */
    protected static MatrixND mat1D;

    /** Two dimension matrix. */
    protected static MatrixND mat2D;

    /** Two dimension matrix with semantics. */
    protected static MatrixND mat2DSemantics;

    /** Three dimension matrix. */
    protected static MatrixND mat3D;

    /** Four dimension matrix. */
    protected static MatrixND mat4D;

    /**
     * Return factory configured with double.
     * 
     * @return MatrixFactory
     */
    public static MatrixFactory getFactory() {
        return MatrixFactory.getInstance(DoubleSparseArrayVector.class);
    }

    /**
     * Setup all matrix with value.
     * 
     * @throws Exception
     */
    @BeforeClass
    public static void init() throws Exception {
        
        I18n.init(new DefaultI18nInitializer("nuiton-matrix", null, "i18n/"), Locale.FRANCE);

        // Avant d'aller plus loin, on s'assure que I18n est bien chargé.
        Assert.assertEquals("L'élément demandé %s n'existe pas dans la matrice %s", I18n.t("nuitonmatrix.invalid.element"));

        // One dimension matrix.
        mat1D = new MatrixNDImpl(getFactory(), new int[] { 10 });
        mat1D.setValue(new int[] { 0 }, 0);
        mat1D.setValue(new int[] { 1 }, 1);
        mat1D.setValue(new int[] { 2 }, 2);
        mat1D.setValue(new int[] { 3 }, 3);
        mat1D.setValue(new int[] { 4 }, -4.0E-7);

        // Two dimension matrix.
        mat2D = new MatrixNDImpl(getFactory(), new int[] { 20, 10 });
        mat2D.setValue(new int[] { 0, 0 }, 0);
        mat2D.setValue(new int[] { 0, 1 }, 1);
        mat2D.setValue(new int[] { 0, 2 }, 2);
        mat2D.setValue(new int[] { 0, 3 }, 3);
        mat2D.setValue(new int[] { 0, 4 }, 4);
        mat2D.setValue(new int[] { 0, 5 }, 4);
        mat2D.setValue(new int[] { 0, 6 }, 4);
        mat2D.setValue(new int[] { 0, 7 }, 4);
        mat2D.setValue(new int[] { 1, 8 }, 4);
        mat2D.setValue(new int[] { 2, 9 }, -4.0E-7);

        // Two dimension matrix with semantics.
        List sem1 = Arrays.asList(new String[] { "toto", "titi", "tutu", "trtr" });
        List sem2 = Arrays.asList(new String[] { "tata", "tete", "tyty" });
        mat2DSemantics = new MatrixNDImpl(getFactory(), "name", new List[] {
                sem1, sem2 }, new String[] { "dim1", "dim2" });
        
        // Three dimension matrix.
        mat3D = new MatrixNDImpl(getFactory(), new int[] { 4, 5, 4 });
        mat3D.setSemantic(0, Arrays.asList(new String[]{"2009","2010","2011","2012"}));
        mat3D.setSemantic(1, Arrays.asList(new String[]{"Nantes","Lille","Paris","Lyon","Marseille"}));
        mat3D.setSemantic(2, Arrays.asList(new String[]{"Informatique","Administration","Developpement","Recherche"}));
        mat3D.setValue(new int[] { 0, 0, 0 }, 0);
        mat3D.setValue(new int[] { 0, 0, 1 }, 1);
        mat3D.setValue(new int[] { 0, 2, 0 }, 2);
        mat3D.setValue(new int[] { 3, 0, 0 }, -4.0E-7);
        mat3D.setValue(new int[] { 1, 1, 1 }, 42.0);
        mat3D.setValue(new int[] { 1, 2, 3 }, 4);
        mat3D.setValue(new int[] { 3, 2, 1 }, 4);
        mat3D.setValue(new int[] { 2, 2, 2 }, 2);
        mat3D.setValue(new int[] { 3, 4, 3 }, 0.004);

        // Four dimension matrix.
        mat4D = new MatrixNDImpl(getFactory(), new int[] { 4, 5, 4, 3});
        mat4D.setSemantic(0, Arrays.asList(new String[]{"2009","2010","2011","2012"}));
        mat4D.setSemantic(1, Arrays.asList(new String[]{"Nantes","Lille","Paris","Lyon","Marseille"}));
        mat4D.setSemantic(2, Arrays.asList(new String[]{"Informatique","Administration","Developpement","Recherche"}));
        mat4D.setSemantic(3, Arrays.asList(new String[]{"Test","Beta","Stable"}));
        mat4D.setValue(new int[] { 0, 0, 0, 0 }, 0);
        mat4D.setValue(new int[] { 0, 0, 0, 1 }, 1);
        mat4D.setValue(new int[] { 0, 2, 0, 2 }, 2);
        mat4D.setValue(new int[] { 3, 0, 0, 1 }, -4.0E-7);
        mat4D.setValue(new int[] { 1, 1, 1, 1 }, 42.0);
        mat4D.setValue(new int[] { 1, 2, 3, 2 }, 4);
        mat4D.setValue(new int[] { 3, 2, 1, 0 }, 4);
        mat4D.setValue(new int[] { 2, 2, 2, 2 }, 2);
        mat4D.setValue(new int[] { 3, 4, 3, 2 }, 0.004);
    }

    /**
     * Test d'import d'une matrice 2D.
     * 
     * @throws IOException
     */
    @Test
    public void testImport() throws IOException {
        String test = "5.0E-7;1.0;2.0;3.0;4.0;4.0;4.0;4.0;0.3;0.0\n"
                + "0.0;0.0;7.0;0.0;0.0;2.0;0.0;0.0;4.0;0.0\n"
                + "0.0;0.0;8.0;1.0;0.0;0.0;0.0;0.0;0.0;4.0\n";
        StringReader reader = new StringReader(test);

        mat2D.importCSV(reader, new int[] { 0, 0 });
        Assert.assertEquals(mat2D.getValue(0, 0), 5.0E-7, 0);
        Assert.assertEquals(mat2D.getValue(1, 2), 7.0, 0);
        Assert.assertEquals(mat2D.getValue(2, 2), 8.0, 0);
        Assert.assertEquals(mat2D.getValue(1, 8), 4.0, 0);

        reader = new StringReader(test);
        mat2D.importCSV(reader, new int[] { 1, 1 });
        Assert.assertEquals(mat2D.getValue(1, 1), 5.0E-7, 0);
        Assert.assertEquals(mat2D.getValue(2, 3), 7.0, 0);
        Assert.assertEquals(mat2D.getValue(3, 3), 8.0, 0);
        Assert.assertEquals(mat2D.getValue(2, 9), 4.0, 0);
    }

    /**
     * Test les sorties des exports suivit des export des imports.
     * 
     * @throws IOException
     */
    @Test
    public void testExport() throws IOException {
        exportCompare(mat1D, false);
        exportCompare(mat2D, false);

        exportCompare(mat1D, true);
        exportCompare(mat2D, true);

        exportCompare(mat2DSemantics, false);
        exportCompare(mat2DSemantics, true);
        
        exportCompare(mat3D, true);
        exportCompare(mat4D, false);
    }

    /**
     * L'export de la matrice et l'export apres l'import doivent
     * redonner le même résultat.
     * 
     * @param matrixND matrix to test
     * @param withSemantics with semantics
     * @throws IOException
     */
    protected void exportCompare(MatrixND matrixND, boolean withSemantics)
            throws IOException {
        StringWriter writer = new StringWriter();
        matrixND.exportCSV(writer, withSemantics);
        StringReader writerToReader = new StringReader(writer.toString());
        matrixND.importCSV(writerToReader, new int[] { 0, 0 });

        StringWriter readerToWriter = new StringWriter();
        matrixND.exportCSV(readerToWriter, withSemantics);

        Assert.assertEquals(writer.toString(), readerToWriter.toString());
    }

    /**
     * Test que l'import d'une matrice à partir d'une chaine produit
     * l'import voulu.
     * @throws IOException 
     */
    @Test
    public void testMatrix3DImport() throws IOException {
        
        String matrix3D = "[2, 2, 2]\n" +
                "java.lang.String:2009,2010\n" +
                "java.lang.String:Nantes,Lille\n" +
                "java.lang.String:Informatique,Administration\n" +
                "0;0;0;0.0\n" +
                "0;0;1;1.0\n" +
                "0;1;1;4.0\n" +
                "1;0;0;-4.0E-7\n" +
                "1;0;1;2.0\n" +
                "1;1;0;4.0\n" +
                "1;1;1;42.0\n";

        MatrixND m = MatrixFactory.getInstance().create(new List[]{
                Arrays.asList("2009", "2010"),
                Arrays.asList("Nantes", "Lille"),
                Arrays.asList("Informatique", "Administration")
        });
        m.importCSV(new StringReader(matrix3D), null);

        // dim
        Assert.assertArrayEquals(new int[]{2, 2, 2}, m.getDim());
        // default value
        Assert.assertEquals(0.0, m.getValue(0, 1, 0), 0.0001);
        // some test
        Assert.assertEquals(-4.0e-7, m.getValue(1, 0, 0), 1e-8);
        Assert.assertEquals(42, m.getValue(1, 1, 1), 0.0001);
    }
    
    /**
     * Test que l'import d'une matrice à partir d'une chaine produit
     * l'import voulu avec une valeur par defaut.
     * 
     * @throws IOException 
     */
    @Test
    public void testMatrix2DImportDefaultValue() throws IOException {
        
        String matrix3D = "[2, 2, 2] , 3.14159265 \n" +
                "java.lang.String:2009,2010\n" +
                "java.lang.String:Nantes,Lille\n" +
                "java.lang.String:Informatique,Administration\n" +
                "0;0;0;0.0\n" +
                "0;0;1;1.0\n" +
                "0;1;1;4.0\n" +
                "1;0;0;-4.0E-7\n" +
                "1;0;1;2.0\n" +
                "1;1;0;4.0\n" +
                "1;1;1;42.0";

        MatrixND m = MatrixFactory.getInstance().create(new StringReader(matrix3D));
        // default value
        Assert.assertEquals(Math.PI, m.getValue(0, 1, 0), 0.00001);
    }
    
    /**
     * Test que l'import d'une matrice à partir d'une chaine produit
     * l'import voulu avec error de nmbre de valeur de semantiques.
     * 
     * @throws IOException 
     */
    @Test(expected=MatrixException.class)
    public void testMatrix3DImportSemError() throws IOException {
        
        String matrix3D = "[2, 2, 2] , 3.14159265 \n" +
                "java.lang.String:2009,2010\n" +
                "java.lang.String:Nantes\n" +
                "java.lang.String:Informatique,Administration\n" +
                "0;0;0;0.0\n" +
                "0;0;1;1.0\n" +
                "0;1;1;4.0\n" +
                "1;0;0;-4.0E-7\n" +
                "1;0;1;2.0\n" +
                "1;1;0;4.0\n" +
                "1;1;1;42.0";

        MatrixND m = MatrixFactory.getInstance().create(new int[]{2, 2, 2});
        m.importCSV(new StringReader(matrix3D), null);
    }
    
    /**
     * Test le même import avec un Reader qui ne support pas l'operation mark():
     * FileReader.
     * 
     * @throws IOException 
     */
    @Test
    public void testMatrix3DImportReader() throws IOException {
        
        String matrix3D = "[2, 2, 2] , 3.14159265 \n" +
                "java.lang.String:2009,2010\n" +
                "java.lang.String:Nantes,Lille\n" +
                "java.lang.String:Informatique,Administration\n" +
                "0;0;0;0.0\n" +
                "0;0;1;1.0\n" +
                "0;1;1;4.0\n" +
                "1;0;0;-4.0E-7\n" +
                "1;0;1;2.0\n" +
                "1;1;0;4.0\n" +
                "1;1;1;42.0";
        File file = File.createTempFile("import-reader", ".txt");
        file.deleteOnExit();
        FileUtils.writeStringToFile(file, matrix3D, "utf-8");
        MatrixND m = MatrixFactory.getInstance().create(new int[]{2, 2, 2});
        m.importCSV(new InputStreamReader(new FileInputStream(file), StandardCharsets.UTF_8), null);
    }
    
    /**
     * Test que l'import d'une matrice à partir d'une chaine produit
     * l'import voulu avec error semantics vide.
     * 
     * @throws IOException 
     */
    @Test(expected=MatrixException.class)
    public void testMatrix3DImportSemError2() throws IOException {
        
        String matrix3D = "[2, 2, 2]\n" +
                "java.lang.String:2009,2010\n" +
                "java.lang.String:Nantes, Lille\n" +
                "\n" +
                "0;0;0;0.0\n" +
                "0;0;1;1.0\n" +
                "0;1;1;4.0\n" +
                "1;0;0;-4.0E-7\n" +
                "1;0;1;2.0\n" +
                "1;1;0;4.0\n" +
                "1;1;1;42.0";

        MatrixND m = MatrixFactory.getInstance().create(new int[]{2, 2, 2});
        m.importCSV(new StringReader(matrix3D), null);
    }
    
    /**
     * Test que l'import d'une matrice à partir d'une chaine produit
     * l'import voulu avec 0 semantics.
     * 
     * @throws IOException 
     */
    @Test(expected=MatrixException.class)
    public void testMatrix3DImportSemError3() throws IOException {
        
        String matrix3D = "[2, 2, 2] , 3.14159265 \n" +
                "0;0;0;0.0\n" +
                "0;0;1;1.0\n" +
                "0;1;1;4.0\n" +
                "1;0;0;-4.0E-7\n" +
                "1;0;1;2.0\n" +
                "1;1;0;4.0\n" +
                "1;1;1;42.0";

        MatrixND m = MatrixFactory.getInstance().create(new int[]{2, 2, 2});
        m.importCSV(new StringReader(matrix3D), null);
    }

    /**
     * Test l'import depuis un {@link File}.
     * 
     * @throws IOException
     */
    @Test
    public void testImportMatrixFromFile() throws IOException {
        MatrixND m = MatrixFactory.getInstance().create(new int[]{5, 6});
        
        File testFile = new File(StringUtils.replace("src/test/resources/matrix2dtest.csv", "/", File.separator));
        m.importCSV(testFile, new int[] {0, 0});
        Assert.assertEquals("matrix2dtest", m.getName());
        
        testFile = new File(StringUtils.replace("src/test/resources/matrix2dtest2", "/", File.separator));
        m.importCSV(testFile, new int[] {0, 0});
        Assert.assertEquals("matrix2dtest2", m.getName());
    }
    
    /**
     * Test l'import depuis un {@link File} windows.
     * 
     * @throws IOException
     */
    @Test
    public void testImportMatrixFromFileCrlf() throws IOException {
        MatrixND m = MatrixFactory.getInstance().create(new int[]{1,65,12});
        
        File testFile = new File(StringUtils.replace("src/test/resources/testimportcrlf.csv", "/", File.separator));
        m.importCSV(testFile, new int[] {0, 0, 0});
        Assert.assertEquals("testimportcrlf", m.getName());
    }
}
