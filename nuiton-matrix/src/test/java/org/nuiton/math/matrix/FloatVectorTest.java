/*
 * #%L
 * Nuiton Matrix :: API
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.math.matrix;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Test;

/**
 * FloatVectorTest.
 *
 * Created: 6 octobre 2005 01:54:49 CEST
 *
 * @author Benjamin POUSSIN &lt;poussin@codelutin.com&gt;
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class FloatVectorTest { // FloatVectorTest

    /** to use log facility, just put in your code: log.info(\"...\"); */
    private static Log log = LogFactory.getLog(FloatVectorTest.class);
    
    @Test
    public void testAll() throws Exception {
        FloatVector v = new FloatVector(16);
        Assert.assertEquals(0.0, v.getMaxOccurrence(), 0);

        v.setValue(0, 1);
        Assert.assertEquals(1.0, v.getValue(0), 0);

        v.setValue(15, 16);
        Assert.assertEquals(16.0, v.getValue(15), 0);

        Assert.assertEquals(2, v.positionSize);
        v.setValue(0, 0);
        Assert.assertEquals(0.0, v.getValue(0), 0);
        Assert.assertEquals(1, v.positionSize);

        v.setValue(0, 4);
        v.setValue(1, 4);
        v.setValue(2, 4);
        v.setValue(3, 4);
        v.setValue(4, 4);
        v.setValue(5, 4);
        v.setValue(6, 4);
        Assert.assertEquals(0.0, v.getMaxOccurrence(), 0);
        v.setValue(8, 4);
        Assert.assertEquals(4.0, v.getMaxOccurrence(), 0);
        v.setValue(0, 0);
        Assert.assertEquals(0.0, v.getMaxOccurrence(), 0);

        try {
            v.getValue(-1);
            Assert.fail("An exception must be thrown");
        } catch (IllegalArgumentException e) {
            if (log.isDebugEnabled()) {
                log.debug("Exception normally thrown", e);
            }
        }

        try {
            v.getValue(20);
            Assert.fail("An exception must be thrown");
        } catch (IllegalArgumentException e) {
            if (log.isDebugEnabled()) {
                log.debug("Exception normally thrown", e);
            }
        }

    }

} // FloatVectorTest
