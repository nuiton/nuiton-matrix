/*
 * #%L
 * Nuiton Matrix :: API
 * %%
 * Copyright (C) 2010 Codelutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.math.matrix;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Test;

/**
 * Test about matrix proxy and provider class.
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class MatrixProxyTest {

    private static final Log log = LogFactory.getLog(MatrixProxyTest.class);

    public MatrixFactory getFactory() throws Exception {
        return MatrixFactory.getInstance(DoubleSparseArrayVector.class);
    }

    static class MatrixTestProvider implements MatrixProvider {
        @Override
        public void fillValues(MatrixND matrix) {
            MatrixHelper.fill(matrix, 10.0);
        }
    }

    protected MatrixND getTestProxy(int dim) throws Exception {

        List<String> years = new ArrayList<String>();
        List<String> cities = new ArrayList<String>();
        List<String> quarter = new ArrayList<String>();
        for (int i = 0 ; i < dim ; i ++) {
            years.add("200" + i);
            cities.add("City " + i);
            quarter.add("Quarter " + i);
        }

        MatrixProxy matrix = getFactory().createProxy("test matrix", new List<?>[] { years, cities, quarter},
                new String[] {"Years", "Cities", "Quarter"}, new MatrixTestProvider());
        return matrix;
    }

    /**
     * Test de manipuler la matrice comme une matrice normale
     * et que ses valeurs soit setter par le provider.
     * 
     * Test par indices.
     * 
     * @throws Exception
     */
    @Test
    public void testReductionAndProvider() throws Exception {
        MatrixND matrix = getTestProxy(7);
        Assert.assertEquals(3, matrix.getDimCount());
        Assert.assertEquals(7, matrix.getDim(0));
        Assert.assertEquals("Cities", matrix.getDimensionName(1));

        // toutes la matrice, mais juste pour les années 2002/2003
        MatrixND subMatrix = matrix.getSubMatrix(new int[][]{new int[]{2,3}, null, null});
        if (log.isDebugEnabled()) {
            log.debug("subMatrix = " + subMatrix);
        }
        Assert.assertEquals(2, subMatrix.getDim(0));
        Assert.assertEquals(10.0, subMatrix.getValue(0, 0, 0), 0.0001);
    }

    /**
     * Test de manipuler la matrice comme une matrice normale
     * et que ses valeurs soit setter par le provider.
     * 
     * Test par objet de semantique.
     * 
     * @throws Exception
     */
    @Test
    public void testReductionAndProviderObject() throws Exception {
        MatrixND matrix = getTestProxy(7);
        Assert.assertEquals(3, matrix.getDimCount());
        Assert.assertEquals(7, matrix.getDim(0));
        Assert.assertEquals("Cities", matrix.getDimensionName(1));

        // toutes la matrice, mais juste pour les années 2002/2003
        MatrixND subMatrix = matrix.getSubMatrix(new Object[][]{new String[]{"2002","2003"}, null, null});
        Assert.assertEquals(2, subMatrix.getDim(0));
        Assert.assertEquals(10.0, subMatrix.getValue(0, 0, 0), 0.0001);
    }
    
    
}
