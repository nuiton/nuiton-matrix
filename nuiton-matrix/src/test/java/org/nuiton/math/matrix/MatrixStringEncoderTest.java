/*
 * #%L
 * Nuiton Matrix :: API
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.math.matrix;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * MatrixHelperTest.
 *
 * Created: 29 oct. 2004
 *
 * @author Benjamin Poussin &lt;poussin@codelutin.com&gt;
 * @version $Revision$
 *
 * Mise a jour: $Date$
 * par : $Author$
 */
public class MatrixStringEncoderTest { // MatrixHelperTest

    /** to use log facility, just put in your code: log.info(\"...\"); */
    private static Log log = LogFactory.getLog(MatrixStringEncoderTest.class);
    
    protected MatrixStringEncoder stringEncoder;
    
    @Before
    public void setUp() {
        stringEncoder = new MatrixStringEncoder();
    }
    
    public MatrixFactory getFactory() {
        return MatrixFactory.getInstance();
    }
    
    @Test
    public void testMatrixStringRepresentation() {
        
        MatrixND mat1 = getFactory().create(new int[] { 3, 2, 1 });
        mat1.setValue(0, 0, 0, -1.0E-7);
        mat1.setValue(1, 1, 0, 2);
        mat1.setValue(2, 1, 0, 5.0E-7);

        String rep = stringEncoder.getMatrixAsString(mat1);

        System.out.println(rep);
        
        Assert.assertTrue("Null() semantics missing", rep.indexOf("null()") > 0);
        Assert.assertTrue("Dimentation missing", rep.indexOf("3, 2, 1") > 0);
        Assert.assertTrue("Data missing", rep.indexOf("[2.0]") > 0);
    }

    @Test
    public void testMatrixFromString() {
        String representation = "[,[3, 2, 1],[\"\", \"\", \"\"],[[null(), null(), null()], [null(), null()], [null()]],[[[-1.0E-7], [0.0]], [[0.0], [2.0]], [[0.0], [5.0E-7]]]]";

        MatrixND matrix = stringEncoder.getMatrixFromString(representation);
        
        Assert.assertEquals(3, matrix.getDim()[0]);
        Assert.assertEquals(2, matrix.getDim()[1]);
        Assert.assertEquals(1, matrix.getDim()[2]);
        Assert.assertEquals(-1.0E-7, matrix.getValue(0, 0, 0), 0);
        Assert.assertEquals(2, matrix.getValue(1, 1, 0), 0);
        Assert.assertEquals(5.0E-7, matrix.getValue(2, 1, 0), 0);
    }
    
    @Test
    public void testMatrixStringRepresentationWithMoreInfos() {
        
        MatrixND mat1 = getFactory().create(new int[] { 2, 5 });
        mat1.setName("StringRepresentationWithMoreInfos");
        mat1.setValue(0, 0, -2);
        mat1.setValue(1, 1, 2.345);
        mat1.setValue(1, 4, -8.321);
        
        List<Serializable> sem0 = new ArrayList<Serializable>();
        sem0.add(new String("test"));
        sem0.add(new Double(3.453));
        mat1.setSemantic(0, sem0);
        List<Serializable> sem1 = new ArrayList<Serializable>();
        sem1.add(new Integer(7));
        sem1.add(new Character('e'));
        sem1.add(Byte.valueOf("1"));
        sem1.add(Short.valueOf("3"));
        sem1.add(new Integer(12));
        mat1.setSemantics(1, sem1);
        
        mat1.setDimensionNames(new String[] {"col1", "col2"});

        String rep = stringEncoder.getMatrixAsString(mat1);

        System.out.println(rep);
        
        Assert.assertTrue("java.lang.String semantics missing", rep.indexOf("java.lang.String") > 0);
        Assert.assertTrue("Dimentation missing", rep.indexOf("[2, 5]") > 0);
        Assert.assertTrue("Data missing", rep.indexOf("[0.0, 2.345, 0.0, 0.0, -8.321]") > 0);
        Assert.assertTrue("Columns name missing", rep.indexOf("[\"col1\", \"col2\"]") > 0);
        Assert.assertTrue("Name missing", rep.indexOf("StringRepresentationWithMoreInfos") > 0);
        
    }

    @Test
    public void testMatrixFromStringWithMoreInfos() {
        String representation = "[StringRepresentationWithMoreInfos,[2, 5],[\"col1\", \"col2\"],[[java.lang.String(test), java.lang.Double(3.453)], [java.lang.Integer(7), java.lang.Character(e), java.lang.Byte(1), java.lang.Short(3), java.lang.Integer(12)]],[[-2.0, 0.0, 0.0, 0.0, 0.0], [0.0, 2.345, 0.0, 0.0, -8.321]]]";

        MatrixND matrix = stringEncoder.getMatrixFromString(representation);
        
        Assert.assertEquals("StringRepresentationWithMoreInfos", matrix.getName());
        Assert.assertEquals(2, matrix.getDim()[0]);
        Assert.assertEquals(5, matrix.getDim()[1]);
        Assert.assertEquals("col1", matrix.getDimensionNames()[0]);
        Assert.assertEquals("col2", matrix.getDimensionNames()[1]);
        Assert.assertEquals("test", matrix.getSemantics(0).get(0));
        Assert.assertEquals(3.453, matrix.getSemantics(0).get(1));
        Assert.assertEquals(7, matrix.getSemantics(1).get(0));
        Assert.assertEquals('e', matrix.getSemantics(1).get(1));
        Assert.assertEquals(Byte.valueOf("1"), matrix.getSemantics(1).get(2));
        Assert.assertEquals(Short.valueOf("3"), matrix.getSemantics(1).get(3));
        Assert.assertEquals(12, matrix.getSemantics(1).get(4));
        Assert.assertEquals(-2, matrix.getValue(0, 0), 0);
        Assert.assertEquals(2.345, matrix.getValue(1, 1), 0);
        Assert.assertEquals(-8.321, matrix.getValue(1, 4), 0);
    }
    
    @Test
    public void testMatrixNamesSpecialChars() {
        
        MatrixND mat1 = getFactory().create(new int[] { 3, 2, 1 });
        mat1.setValue(0, 0, 0, -1.0E-7);
        mat1.setValue(1, 1, 0, 2);
        mat1.setValue(2, 1, 0, 5.0E-7);
        mat1.setDimensionNames(new String[]{"col \"1\", \"2\"","col \"3\", \"4\""});
        String rep = stringEncoder.getMatrixAsString(mat1);

        System.out.println(rep);
        
        Assert.assertTrue("Null() semantics missing", rep.indexOf("null()") > 0);
        Assert.assertTrue("Dimentation missing", rep.indexOf("3, 2, 1") > 0);
        Assert.assertTrue("Data missing", rep.indexOf("[2.0]") > 0);
        Assert.assertTrue("Wrong string encoding", rep.indexOf("\"col \\\"1\\\", \\\"2\\\"\", \"col \\\"3\\\", \\\"4\\\"\", \"\"") > 0);
    }

    @Test
    public void testMatrixFromNamesSpecialChars() {
        String representation = "[,[3, 2, 1],[\"col \\\"1\\\", \\\"2\\\"\", \"col \\\"3\\\", \\\"4\\\"\", \"\"],[[null(), null(), null()], [null(), null()], [null()]],[[[-1.0E-7], [0.0]], [[0.0], [2.0]], [[0.0], [5.0E-7]]]]";

        MatrixND matrix = stringEncoder.getMatrixFromString(representation);
        
        Assert.assertEquals(3, matrix.getDim()[0]);
        Assert.assertEquals(2, matrix.getDim()[1]);
        Assert.assertEquals(1, matrix.getDim()[2]);
        Assert.assertEquals(-1.0E-7, matrix.getValue(0, 0, 0), 0);
        Assert.assertEquals(2, matrix.getValue(1, 1, 0), 0);
        Assert.assertEquals(5.0E-7, matrix.getValue(2, 1, 0), 0);
        Assert.assertEquals("col \"1\", \"2\"", matrix.getDimensionNames()[0]);
        Assert.assertEquals("col \"3\", \"4\"", matrix.getDimensionNames()[1]);
    }
} // MatrixHelperTest

