/*
 * #%L
 * Nuiton Matrix :: API
 * %%
 * Copyright (C) 2004 - 2012 CodeLutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.math.matrix;

import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang3.time.DurationFormatUtils;
import org.junit.Test;
import org.nuiton.util.StringUtil;

/**
 * PerfDoubleBigVectorTest.java
 *
 * Created: 7 sept. 06 02:28:51
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class PerfDoubleBigVectorTest {

    protected static final char CMAX = 'j';
    protected static final int MAX = 10;

    protected Class<? extends Vector> getVectorClass() throws Exception {
        return DoubleBigVector.class;
    }

    protected List<String> getSem() {
        List<String> sem = new ArrayList<String>();
        for (char c = 'a'; c <= CMAX; c++) {
            for (int i = 1; i <= MAX; i++) {
                sem.add(c + " " + i);
            }
        }
        return sem;
    }

    @Test
    public void testDoubleVectorIterator() throws Exception {
        List<String> sem = getSem();
        
        // [ 2600 x 2600 x 2600 ]
        System.gc(); // force gc now to minimize gc call during timing
        long timestart = System.nanoTime();
        MatrixND mat = MatrixFactory.getInstance(getVectorClass()).create("Ma mat",
                new List[] { sem, sem, sem });
        long timeinit = System.nanoTime();
        // parcours avec iterator
        long cpt = 1;
        for (MatrixIterator i = mat.iterator(); i.hasNext();) {
            i.next();
            i.setValue(cpt++);
        }
        long timeend = System.nanoTime();

        long freeMem = Runtime.getRuntime().freeMemory();
        long totalMem = Runtime.getRuntime().totalMemory();

        long vectorSize = ((MatrixNDImpl)mat).getInternalMatrix().getInternalVector().size();

        System.out.println(
                "Perf " + getVectorClass().getSimpleName() + "("
                + vectorSize + ")"
                + "\tIterator\tinit: "
                + DurationFormatUtils.formatDuration(
                        (timeinit - timestart) / 1000000, "s'.'S")
                + " fill: "
                + DurationFormatUtils.formatDuration(
                        (timeend - timeinit) / 1000000, "s'.'S")
                + " memory: " + StringUtil.convertMemory(totalMem - freeMem) + "/"
                + StringUtil.convertMemory(totalMem));
    }

    @Test
    public void testDoubleVectorIndex() throws Exception {
        List<String> sem = getSem();
        int size = sem.size();

        // [ 2600 x 2600 x 2600 ]
        System.gc(); // force gc now to minimize gc call during timing
        long timestart = System.nanoTime();
        MatrixND mat = MatrixFactory.getInstance(getVectorClass()).create("Ma mat",
                new List[] { sem, sem, sem });
        long timeinit = System.nanoTime();
        // parcours avec iterator
        long cpt = 1;
        for (int x = 0; x < size; x++) {
            for (int y = 0; y < size; y++) {
                for (int z = 0; z < size; z++) {
                    mat.setValue(x, y, z, cpt++);
                }
            }
        }
        long timeend = System.nanoTime();

        long freeMem = Runtime.getRuntime().freeMemory();
        long totalMem = Runtime.getRuntime().totalMemory();

        long vectorSize = ((MatrixNDImpl)mat).getInternalMatrix().getInternalVector().size();

        System.out.println(
                "Perf " + getVectorClass().getSimpleName() + "("
                + vectorSize + ")"
                + "\tIndex\t\tinit: " + DurationFormatUtils.formatDuration(
                        (timeinit - timestart) / 1000000, "s'.'S")
                + " fill: "
                + DurationFormatUtils.formatDuration(
                        (timeend - timeinit) / 1000000, "s'.'S")
                + " memory: " + StringUtil.convertMemory(totalMem - freeMem) + "/"
                + StringUtil.convertMemory(totalMem));
    }

    @Test
    public void testDoubleVectorMap() throws Exception {
        List<String> sem = getSem();

        // [ 2600 x 2600 x 2600 ]
        System.gc(); // force gc now to minimize gc call during timing
        long timestart = System.nanoTime();
        MatrixND mat = MatrixFactory.getInstance(getVectorClass()).create("Ma mat",
                new List[] { sem, sem, sem });

        long cpt = 1;
        for (MatrixIterator i = mat.iterator(); i.hasNext();) {
            i.next();
            i.setValue(cpt++);
        }
                
        long timeinit = System.nanoTime();

        // application de la fonction
        mat.map(new MapFunction() {
            long cpt = 1;
            public double apply(double value) {
                return cpt++;
            }
        });

        long timeend = System.nanoTime();

        long freeMem = Runtime.getRuntime().freeMemory();
        long totalMem = Runtime.getRuntime().totalMemory();

        long vectorSize = ((MatrixNDImpl)mat).getInternalMatrix().getInternalVector().size();

        System.out.println(
                "Perf " + getVectorClass().getSimpleName() + "("
                + vectorSize + ")"
                + "\tMap func\tinit: "
                + DurationFormatUtils.formatDuration(
                        (timeinit - timestart) / 1000000, "s'.'S")
                + " fill: "
                + DurationFormatUtils.formatDuration(
                        (timeend - timeinit) / 1000000, "s'.'S")
                + " memory: " + StringUtil.convertMemory(totalMem - freeMem) + "/"
                + StringUtil.convertMemory(totalMem));
    }

    @Test
    public void testDoubleVectorSemantic() throws Exception {
        List<String> sem = getSem();
        int size = sem.size();

        // [ 2600 x 2600 x 2600 ]
        System.gc(); // force gc now to minimize gc call during timing
        long timestart = System.nanoTime();
        MatrixND mat = MatrixFactory.getInstance(getVectorClass()).create("Ma mat",
                new List[] { sem, sem, sem });
        long timeinit = System.nanoTime();
        // parcours avec iterator
        long cpt = 1;
        for (int x = 0; x < size; x++) {
            for (int y = 0; y < size; y++) {
                for (int z = 0; z < size; z++) {
                    String X = sem.get(x);
                    String Y = sem.get(y);
                    String Z = sem.get(z);
                    mat.setValue(X, Y, Z, cpt++);
                }
            }
        }
        long timeend = System.nanoTime();

        long freeMem = Runtime.getRuntime().freeMemory();
        long totalMem = Runtime.getRuntime().totalMemory();

        long vectorSize = ((MatrixNDImpl)mat).getInternalMatrix().getInternalVector().size();

        System.out.println(
                "Perf " + getVectorClass().getSimpleName() + "("
                + vectorSize + ")"
                + "\tSemantic\tinit: "
                + DurationFormatUtils.formatDuration(
                        (timeinit - timestart) / 1000000, "s'.'S")
                + " fill: "
                + DurationFormatUtils.formatDuration(
                        (timeend - timeinit) / 1000000, "s'.'S")
                + " memory: " + StringUtil.convertMemory(totalMem - freeMem) + "/"
                + StringUtil.convertMemory(totalMem));
    }

    @Test
    public void testSumOverDim() throws Exception {
                System.gc(); // force gc now to minimize gc call during timing
        long timestart = System.nanoTime();
        MatrixNDImpl mat = (MatrixNDImpl)MatrixFactory.getInstance(getVectorClass()).create("Ma mat",
                new int[] { 10, 20, 50, 200 });

        // rempli la matrice 1/7
        for(VectorIterator i=mat.getInternalMatrix().getInternalVector().iterator(); i.hasNext(); i.next()) {
            if (i.getPosition() % 7 == 0) {
                i.setValue(7);
            }
        }

        for (int i=0, maxi=mat.getDimCount(); i<maxi; i++) {
            long timeinit = System.nanoTime();
            MatrixND result = mat.sumOverDim(i);

            long timeend = System.nanoTime();

            long vectorSize = mat.getInternalMatrix().getInternalVector().size();

            System.out.println(
                    "Perf " + getVectorClass().getSimpleName() + "("
                            + vectorSize + ")"
                            + " sumOverDim : dim size " + mat.getDim(i) + " -> "
                            + DurationFormatUtils.formatDuration(
                                    (timeend - timeinit) / 1000000, "s'.'S"));
        }
    }
}
