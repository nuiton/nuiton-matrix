/*
 * #%L
 * Nuiton Matrix :: API
 * %%
 * Copyright (C) 2004 - 2012 CodeLutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.math.matrix;

import java.util.Arrays;
import java.util.NoSuchElementException;

import org.apache.commons.lang3.time.DurationFormatUtils;
import org.junit.Assert;
import org.junit.Test;

/**
 * BasicMatrixTest.
 *
 * Created: 27 oct. 2004
 *
 * @author Benjamin Poussin &lt;poussin@codelutin.com&gt;
 * @version $Revision$
 *
 * Mise a jour: $Date$
 * par : $Author$
 */
public class BasicMatrixDoubleBigVectorTest { // BasicMatrixTest

    public MatrixFactory getFactory() throws Exception {
        return MatrixFactory.getInstance(DoubleBigVector.class);
    }

    @Test
    public void testNew() throws Exception {
        BasicMatrix mat = null;
        try {
            mat = new BasicMatrix(getFactory(), null);
            Assert.assertFalse(true); // on ne doit pas etre ici
        } catch (NullPointerException eee) {
            Assert.assertTrue(true); // mais on doit etre la
        }

        try {
            mat = new BasicMatrix(getFactory(), new int[] { 0 });
            Assert.assertFalse(true); // on ne doit pas etre ici
        } catch (IllegalArgumentException eee) {
            Assert.assertTrue(true); // mais on doit etre la
        }

        mat = new BasicMatrix(getFactory(), new int[] { 100 });
        mat = new BasicMatrix(getFactory(), new int[] { 10, 1 });
        Assert.assertEquals(0.0, mat.getMaxOccurrence(), 0);
        mat = new BasicMatrix(getFactory(), new int[] { 10, 10, 10, 10 });
        try {
            mat = new BasicMatrix(getFactory(), new int[] { -10 });
            Assert.assertFalse(true); // on ne doit pas etre ici
        } catch (IllegalArgumentException eee) {
            Assert.assertTrue(true); // mais on doit etre la
        }
        try {
            mat = new BasicMatrix(getFactory(), new int[] { 10, 20, -10, 20 });
            Assert.assertFalse(true); // on ne doit pas etre ici
        } catch (IllegalArgumentException eee) {
            Assert.assertTrue(true); // mais on doit etre la
        }
    }

    @Test
    public void testDimension() throws Exception {
        BasicMatrix mat = null;
        mat = new BasicMatrix(getFactory(), new int[] { 1, 10, 30, 5 });

        Assert.assertEquals(4, mat.getNbDim());
        Assert.assertEquals(1, mat.getDim(0));
        Assert.assertEquals(10, mat.getDim(1));
        Assert.assertEquals(30, mat.getDim(2));
        Assert.assertEquals(5, mat.getDim(3));

        try {
            mat.getDim(-3);
            Assert.assertFalse(true); // on ne doit pas etre ici
        } catch (IndexOutOfBoundsException eee) {
            Assert.assertTrue(true); // mais on doit etre la
        }
        try {
            mat.getDim(4);
            Assert.assertFalse(true); // on ne doit pas etre ici
        } catch (IndexOutOfBoundsException eee) {
            Assert.assertTrue(true); // mais on doit etre la
        }
    }

    @Test
    public void testGetSet() throws Exception {
        BasicMatrix mat = null;

        // test avec la plus petit BasicMatrix possible
        mat = new BasicMatrix(getFactory(), new int[] { 1 });
        // test la valeur par defaut doit etre 0
        Assert.assertEquals(0, mat.getValue(new int[] { 0 }), 0);
        mat.setValue(new int[] { 0 }, 30);
        Assert.assertEquals(30, mat.getValue(new int[] { 0 }), 0);

        // acces a un element qui n'existe pas
        try {
            mat.getValue(new int[] { 1 });
            Assert.assertFalse(true); // on ne doit pas etre ici
        } catch (NoSuchElementException eee) {
            Assert.assertTrue(true); // mais on doit etre la
        }

        mat = new BasicMatrix(getFactory(), new int[] { 1, 10, 5 });
        mat.setValue(new int[] { 0, 0, 0 }, 0);
        mat.setValue(new int[] { 0, 0, 1 }, 1);
        mat.setValue(new int[] { 0, 0, 2 }, 2);
        mat.setValue(new int[] { 0, 0, 3 }, 3);
        mat.setValue(new int[] { 0, 0, 4 }, 4);
        mat.setValue(new int[] { 0, 1, 0 }, 10);
        mat.setValue(new int[] { 0, 1, 1 }, 11);
        mat.setValue(new int[] { 0, 1, 2 }, 12);
        mat.setValue(new int[] { 0, 1, 3 }, 13);
        mat.setValue(new int[] { 0, 1, 4 }, 14);
        mat.setValue(new int[] { 0, 2, 0 }, 20);
        mat.setValue(new int[] { 0, 2, 1 }, 21);
        mat.setValue(new int[] { 0, 2, 2 }, 22);
        mat.setValue(new int[] { 0, 9, 4 }, 98);
        mat.setValue(new int[] { 0, 4, 2 }, 97);
        Assert.assertEquals(0, mat.getValue(new int[] { 0, 0, 0 }), 0);
        Assert.assertEquals(98, mat.getValue(new int[] { 0, 9, 4 }), 0);
        Assert.assertEquals(97, mat.getValue(new int[] { 0, 4, 2 }), 0);

        // System.out.println(mat.toString());

        // acces a un element qui n'existe pas
        try {
            mat.setValue(new int[] { 0, 9, 5 }, 44);
            Assert.assertFalse(true); // on ne doit pas etre ici
        } catch (NoSuchElementException eee) {
            Assert.assertTrue(true); // mais on doit etre la
        }

    }

    @Test
    public void testEquals() throws Exception {
        BasicMatrix m1 = new BasicMatrix(getFactory(), new int[] { 3, 3, 3, 3 });
        BasicMatrix m2 = new BasicMatrix(getFactory(), new int[] { 3, 3, 3, 3 });

        Assert.assertEquals(m1, m2);

        m1.setValue(new int[] { 1, 2, 1, 2 }, 123);
        m2.setValue(new int[] { 1, 2, 1, 2 }, 123);

        Assert.assertEquals(m1, m2);

        m1.setValue(new int[] { 1, 0, 1, 0 }, 321);
        Assert.assertFalse(m1.equals(m2));
    }

    @Test
    public void testIterator() throws Exception {

        int[][] val27 = new int[][] { { 0, 0, 0 }, { 0, 0, 1 }, { 0, 0, 2 },
                { 0, 1, 0 }, { 0, 1, 1 }, { 0, 1, 2 }, { 0, 2, 0 },
                { 0, 2, 1 }, { 0, 2, 2 }, { 1, 0, 0 }, { 1, 0, 1 },
                { 1, 0, 2 }, { 1, 1, 0 }, { 1, 1, 1 }, { 1, 1, 2 },
                { 1, 2, 0 }, { 1, 2, 1 }, { 1, 2, 2 }, { 2, 0, 0 },
                { 2, 0, 1 }, { 2, 0, 2 }, { 2, 1, 0 }, { 2, 1, 1 },
                { 2, 1, 2 }, { 2, 2, 0 }, { 2, 2, 1 }, { 2, 2, 2 }, };
        int[][] val1 = new int[][] { { 0 } };

        BasicMatrix m1 = new BasicMatrix(getFactory(), new int[] { 3, 3, 3 });
        // System.out.println("Matrice a 27 valeurs");
        int cpt = 0;
        for (BasicMatrixIterator iter = m1.iterator(); iter.next();) {
            Arrays.equals(val27[cpt], iter.getCoordinates());
            cpt++;
        }

        Assert.assertEquals(27, cpt);

        cpt = 0;
        BasicMatrix m2 = new BasicMatrix(getFactory(), new int[] { 1 });
        // System.out.println("Matrice a 1 valeurs");
        for (BasicMatrixIterator iter = m2.iterator(); iter.next();) {
            Arrays.equals(val1[cpt], iter.getCoordinates());
            cpt++;
        }

        Assert.assertEquals(1, cpt);
    }

    protected MapFunction f = new MapFunction() {
        public double apply(double value) {
            return value + 2;
        }
    };

    @Test
    public void testPerfLineaire() throws Exception {
        long time = System.nanoTime();
        BasicMatrix m1 = new BasicMatrix(getFactory(), new int[] { 30, 30, 30,
                30 });
        m1.map(f);
        long time1 = System.nanoTime();
        System.out.println("testPerfLineaire: "
                + DurationFormatUtils.formatDuration((time1 - time) / 1000000,
                        "s'.'S"));
    }

    @Test
    public void testPerfCoordonnee() throws Exception {
        long time = System.nanoTime();
        BasicMatrix m2 = new BasicMatrix(getFactory(), new int[] { 30, 30, 30,
                30 });

        BasicMatrixIterator inc = m2.iterator();
        while (inc.next()) {
            inc.setValue(inc.getValue() + 2);
        }
        long time1 = System.nanoTime();
        System.out.println("testPerfCoordonnee iter: "
                + DurationFormatUtils.formatDuration((time1 - time) / 1000000,
                        "s'.'S"));

        inc = m2.iterator();
        while (inc.next()) {
            inc.setValue(inc.getValue() + 2);
        }
        long time2 = System.nanoTime();
        System.out.println("testPerfCoordonnee iter: "
                + DurationFormatUtils.formatDuration((time2 - time1) / 1000000,
                        "s'.'S"));

        m2.map(f);
        long time3 = System.nanoTime();
        System.out.println("testPerfLineaire map: "
                + DurationFormatUtils.formatDuration((time3 - time2) / 1000000,
                        "s'.'S"));

    }

    @Test
    public void testPerfCoordonnee2() throws Exception {
        long time = System.nanoTime();
        BasicMatrix m2 = new BasicMatrix(getFactory(), new int[] { 30, 30, 30,
                30 });

        BasicMatrixIterator inc = m2.iterator();
        while (inc.next() && inc.next()) {
            inc.setValue(inc.getValue() + 2);
        }
        long time1 = System.nanoTime();
        System.out.println("testPerfCoordonnee2: "
                + DurationFormatUtils.formatDuration((time1 - time) / 1000000,
                        "s'.'S"));

        inc = m2.iterator();
        while (inc.next() && inc.next()) {
            inc.setValue(inc.getValue() + 2);
        }
        long time2 = System.nanoTime();
        System.out.println("testPerfCoordonnee2 re: "
                + DurationFormatUtils.formatDuration((time2 - time1) / 1000000,
                        "s'.'S"));

        m2.map(f);
        long time3 = System.nanoTime();
        System.out.println("testPerfLineaire2: "
                + DurationFormatUtils.formatDuration((time3 - time2) / 1000000,
                        "s'.'S"));

    }

    @Test
    public void testPerfCoordonnee4() throws Exception {
        long time = System.nanoTime();
        BasicMatrix m2 = new BasicMatrix(getFactory(), new int[] { 30, 30, 30,
                30 });

        BasicMatrixIterator inc = m2.iterator();
        while (inc.next() && inc.next() && inc.next() && inc.next()) {
            inc.setValue(inc.getValue() + 2);
        }
        long time1 = System.nanoTime();
        System.out.println("testPerfCoordonnee4: "
                + DurationFormatUtils.formatDuration((time1 - time) / 1000000,
                        "s'.'S"));

        inc = m2.iterator();
        while (inc.next() && inc.next() && inc.next() && inc.next()) {
            inc.setValue(inc.getValue() + 2);
        }
        long time2 = System.nanoTime();
        System.out.println("testPerfCoordonnee4 re: "
                + DurationFormatUtils.formatDuration((time2 - time1) / 1000000,
                        "s'.'S"));

        m2.map(f);
        long time3 = System.nanoTime();
        System.out.println("testPerfLineaire4: "
                + DurationFormatUtils.formatDuration((time3 - time2) / 1000000,
                        "s'.'S"));

    }

}
