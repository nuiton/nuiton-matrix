/*
 * #%L
 * Nuiton Matrix :: API
 * %%
 * Copyright (C) 2004 - 2012 CodeLutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.math.matrix;

import java.io.IOException;
import java.io.StreamTokenizer;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Stack;

import org.apache.commons.lang3.time.DurationFormatUtils;
import org.junit.Assert;
import org.junit.Test;
import org.nuiton.util.StringUtil;

/**
 * MatrixNDTest.java
 *
 * Created: 10 mai 2004
 *
 * @version $Revision$
 *
 * Mise a jour: $Date$
 * par : $Author$
 */
public class MatrixNDDoubleBigVectorTest {

    public MatrixFactory getFactory() throws Exception {
        return MatrixFactory.getInstance(DoubleBigVector.class);
    }

    @Test
    public void testNew() throws Exception {
        List<String> s1 = Arrays.asList(new String[] { "a", "b", "c" });
        List<String> s2 = Arrays.asList(new String[] { "e", "f", "g" });
        List<String> s3 = Arrays.asList(new String[] { "k", "l", "m" });

        MatrixND mat = null;

        mat = getFactory().create("Ma mat", new int[] { 3, 3, 3 });

        mat = getFactory().create("Ma mat", new List[] { s1, s2, s3 });

        mat = getFactory().create("Ma mat", new List[] { s1, s2, s3 },
                new String[] { "dim abc", "dim efg", "dim klm" });
        Assert.assertEquals(0.0, mat.getMaxOccurence(), 0);

    }

    @Test
    public void testSemantique() throws Exception {
        List<String> s1 = Arrays.asList(new String[] { "a", "b", "c" });
        List<String> s2 = Arrays.asList(new String[] { "e", "f", "g" });
        List<String> s3 = Arrays.asList(new String[] { "k", "l", "m" });

        MatrixND mat = null;

        mat = getFactory().create(new int[] { 3, 3, 3 });

        Assert.assertNull(mat.getSemantic(1).get(1));

        mat = getFactory().create("Ma mat", new List[] { s1, s2, s3 });

        // la matrice doit avoir ca propre copie des semantiques
        s2.set(1, "pas bon");
        Assert.assertEquals("f", mat.getSemantic(1).get(1));

        mat.setSemantics(1, s1);
        Assert.assertEquals("b", mat.getSemantic(1).get(1));
    }

    @Test
    public void testName() throws Exception {
        List<String> s1 = Arrays.asList(new String[] { "a", "b", "c" });
        List<String> s2 = Arrays.asList(new String[] { "e", "f", "g" });
        List<String> s3 = Arrays.asList(new String[] { "k", "l", "m" });

        MatrixND mat = null;

        mat = getFactory().create("Ma mat", new List[] { s1, s2, s3 },
                new String[] { "dim abc", "dim efg", "dim klm" });

        Assert.assertEquals("Ma mat", mat.getName());
        mat.setName("Renamed");
        Assert.assertEquals("Renamed", mat.getName());

        Assert.assertEquals("dim abc", mat.getDimensionName(0));
        mat.setDimensionName(0, "dim renamed");
        Assert.assertEquals("dim renamed", mat.getDimensionName(0));
    }

    @Test
    public void testGetSet() throws Exception {
        List<String> s1 = Arrays.asList(new String[] { "a", "b", "c" });
        List<String> s2 = Arrays.asList(new String[] { "e", "f", "g" });
        List<String> s3 = Arrays.asList(new String[] { "k", "l", "m" });

        MatrixND mat = null;

        mat = getFactory().create("Ma mat", new List[] { s1, s2, s3 },
                new String[] { "dim abc", "dim efg", "dim klm" });

        mat.setValue(1, 1, 1, 34);
        Assert.assertEquals(34, mat.getValue("b", "f", "l"), 0);

        mat.setValue("a", "f", "m", 22);
        Assert.assertEquals(22, mat.getValue(0, 1, 2), 0);
    }

    @Test
    public void testIterator() throws Exception {
        int[][] val27 = new int[][] { { 0, 0, 0 }, { 0, 0, 1 }, { 0, 0, 2 },
                { 0, 1, 0 }, { 0, 1, 1 }, { 0, 1, 2 }, { 0, 2, 0 },
                { 0, 2, 1 }, { 0, 2, 2 }, { 1, 0, 0 }, { 1, 0, 1 },
                { 1, 0, 2 }, { 1, 1, 0 }, { 1, 1, 1 }, { 1, 1, 2 },
                { 1, 2, 0 }, { 1, 2, 1 }, { 1, 2, 2 }, { 2, 0, 0 },
                { 2, 0, 1 }, { 2, 0, 2 }, { 2, 1, 0 }, { 2, 1, 1 },
                { 2, 1, 2 }, { 2, 2, 0 }, { 2, 2, 1 }, { 2, 2, 2 }, };

        String[][] vals27 = new String[][] { { "a", "e", "k" },
                { "a", "e", "l" }, { "a", "e", "m" }, { "a", "f", "k" },
                { "a", "f", "l" }, { "a", "f", "m" }, { "a", "g", "k" },
                { "a", "g", "l" }, { "a", "g", "m" }, { "b", "e", "k" },
                { "b", "e", "l" }, { "b", "e", "m" }, { "b", "f", "k" },
                { "b", "f", "l" }, { "b", "f", "m" }, { "b", "g", "k" },
                { "b", "g", "l" }, { "b", "g", "m" }, { "c", "e", "k" },
                { "c", "e", "l" }, { "c", "e", "m" }, { "c", "f", "k" },
                { "c", "f", "l" }, { "c", "f", "m" }, { "c", "g", "k" },
                { "c", "g", "l" }, { "c", "g", "m" }, };

        List<String> s1 = Arrays.asList(new String[] { "a", "b", "c" });
        List<String> s2 = Arrays.asList(new String[] { "e", "f", "g" });
        List<String> s3 = Arrays.asList(new String[] { "k", "l", "m" });

        MatrixND mat = null;

        mat = getFactory().create("Ma mat", new List[] { s1, s2, s3 },
                new String[] { "dim abc", "dim efg", "dim klm" });

        int cpt = 0;
        for (MatrixIterator i = mat.iterator(); i.hasNext();) {
            i.next();
            Assert.assertTrue(Arrays.equals(val27[cpt], i.getCoordinates()));
            Assert.assertTrue(Arrays.equals(vals27[cpt], i
                    .getSemanticsCoordinates()));
            cpt++;
        }
        Assert.assertEquals(27, cpt);
    }

    @Test
    public void testAdd() throws Exception {
        List<String> s1 = Arrays.asList(new String[] { "a", "b", "c" });
        List<String> s2 = Arrays.asList(new String[] { "e", "f", "g" });
        List<String> s3 = Arrays.asList(new String[] { "k", "l", "m" });

        MatrixND mat1 = getFactory().create("Ma mat",
                new List[] { s1, s2, s3 },
                new String[] { "dim abc", "dim efg", "dim klm" });
        MatrixND mat2 = getFactory().create("Ma mat",
                new List[] { s1, s2, s3 },
                new String[] { "dim abc", "dim efg", "dim klm" });

        MatrixHelper.fill(mat1, 3);
        MatrixHelper.fill(mat2, 26);

        mat1.add(mat2);

        Assert.assertEquals(29, mat1.getValue(0, 0, 0), 0);
        Assert.assertEquals(29, mat1.getValue(1, 2, 0), 0);
        Assert.assertEquals(29, mat1.getValue(2, 2, 2), 0);
    }

    @Test
    public void testMinus() throws Exception {
        List<String> s1 = Arrays.asList(new String[] { "a", "b", "c" });
        List<String> s2 = Arrays.asList(new String[] { "e", "f", "g" });
        List<String> s3 = Arrays.asList(new String[] { "k", "l", "m" });

        MatrixND mat1 = getFactory().create("Ma mat",
                new List[] { s1, s2, s3 },
                new String[] { "dim abc", "dim efg", "dim klm" });
        MatrixND mat2 = getFactory().create("Ma mat",
                new List[] { s1, s2, s3 },
                new String[] { "dim abc", "dim efg", "dim klm" });

        MatrixHelper.fill(mat1, 3);
        MatrixHelper.fill(mat2, 26);

        mat1.minus(mat2);

        Assert.assertEquals(-23, mat1.getValue(0, 0, 0), 0);
        Assert.assertEquals(-23, mat1.getValue(1, 2, 0), 0);
        Assert.assertEquals(-23, mat1.getValue(2, 2, 2), 0);
    }

    @Test
    public void testEquals() throws Exception {
        List<String> s1 = Arrays.asList(new String[] { "a", "b", "c" });
        List<String> s2 = Arrays.asList(new String[] { "e", "f", "g" });
        List<String> s3 = Arrays.asList(new String[] { "k", "l", "m" });

        MatrixND mat1 = getFactory().create("Ma mat",
                new List[] { s1, s2, s3 },
                new String[] { "dim abc", "dim efg", "dim klm" });
        MatrixND mat2 = getFactory().create("Ma mat",
                new List[] { s1, s2, s3 },
                new String[] { "dim abc", "dim efg", "dim klm" });
        MatrixND mat3 = getFactory().create("Ma mat",
                new List[] { s1, s2, s3 },
                new String[] { "dim abc", "dim efg", "dim klm" });

        MatrixHelper.fill(mat1, 3);
        MatrixHelper.fill(mat2, 26);
        MatrixHelper.fill(mat3, 29);

        mat1.add(mat2);

        Assert.assertEquals(mat1, mat3);
    }

    @Test
    public void testsumOverDim() throws Exception {
        List<String> s1 = Arrays.asList(new String[] { "a", "b", "c" });
        List<String> s2 = Arrays.asList(new String[] { "e", "f", "g" });
        List<String> s3 = Arrays.asList(new String[] { "k", "l", "m" });

        MatrixND mat = null;

        mat = getFactory().create(new int[] { 4, 4 });
        MatrixND mat2 = mat;
        int i = 0;
        for (MatrixIterator mi = mat.iterator(); mi.next();) {
            mi.setValue(++i);
        }

        Assert.assertEquals(mat, mat2);
        mat2 = mat.sumOverDim(1, 0);
        Assert.assertEquals(mat, mat2);
        mat2 = mat.sumOverDim(1, 1);
        Assert.assertEquals(mat, mat2);
        mat2 = mat.sumOverDim(1, 2);
        Assert.assertEquals(2, mat2.getDim(1));
        Assert.assertEquals(3, mat2.getValue(0, 0), 0);
        Assert.assertEquals(7, mat2.getValue(0, 1), 0);
        Assert.assertEquals(11, mat2.getValue(1, 0), 0);
        Assert.assertEquals(15, mat2.getValue(1, 1), 0);
        Assert.assertEquals(19, mat2.getValue(2, 0), 0);
        Assert.assertEquals(23, mat2.getValue(2, 1), 0);
        Assert.assertEquals(27, mat2.getValue(3, 0), 0);
        Assert.assertEquals(31, mat2.getValue(3, 1), 0);

        mat2 = mat.sumOverDim(1, 3);
        Assert.assertEquals(2, mat2.getDim(1));
        Assert.assertEquals(6, mat2.getValue(0, 0), 0);
        Assert.assertEquals(18, mat2.getValue(1, 0), 0);
        Assert.assertEquals(30, mat2.getValue(2, 0), 0);
        Assert.assertEquals(42, mat2.getValue(3, 0), 0);

        mat2 = mat.sumOverDim(1, 4);
        Assert.assertEquals(1, mat2.getDim(1));
        Assert.assertEquals(10, mat2.getValue(0, 0), 0);
        Assert.assertEquals(26, mat2.getValue(1, 0), 0);
        Assert.assertEquals(42, mat2.getValue(2, 0), 0);
        Assert.assertEquals(58, mat2.getValue(3, 0), 0);

        mat = getFactory().create("Ma mat", new List[] { s1, s2, s3 },
                new String[] { "dim abc", "dim efg", "dim klm" });

        MatrixHelper.fill(mat, 3);
        mat.setValue(0, 0, 0, 2);
        mat.setValue(2, 2, 2, 4);
        mat = mat.sumOverDim(1);
        Assert.assertTrue(MatrixHelper.sameDimension(new int[] { 3, 1, 3 }, mat
                .getDim()));

        Assert.assertEquals(8, mat.getValue(0, 0, 0), 0);
        Assert.assertEquals(9, mat.getValue(2, 0, 1), 0);
        Assert.assertEquals(10, mat.getValue(2, 0, 2), 0);

        mat = getFactory().create(new int[] { 6, 6, 6 });

        MatrixHelper.fill(mat, 3);
        mat.setValue(0, 0, 0, 0);
        mat.setValue(0, 1, 0, 1);
        mat.setValue(0, 2, 0, 2);
        mat.setValue(0, 3, 0, 3);
        mat.setValue(0, 4, 0, 4);
        mat.setValue(0, 5, 0, 5);

        mat = mat.sumOverDim(1, 3);
        Assert.assertTrue(MatrixHelper.sameDimension(new int[] { 6, 2, 6 }, mat
                .getDim()));
        Assert.assertEquals(3, mat.getValue(0, 0, 0), 0);
        Assert.assertEquals(12, mat.getValue(0, 1, 0), 0);
        Assert.assertEquals(9, mat.getValue(1, 1, 5), 0);
        Assert.assertEquals(9, mat.getValue(5, 0, 3), 0);
    }

    /**
     * Test les fonctions meanOverDim...
     * 
     * La matrice de test est
     * 1  2  3  4
     * 5  6  7  8
     * 9  10 11 12
     * 13 14 15 16
     * @throws Exception
     */
    @Test
    public void testMeanOverDim() throws Exception {
        
        // first test matrix :
        // 1  2  3  4
        // 5  6  7  8
        // 9  10 11 12
        // 13 14 15 16
        
        MatrixND mat = null;
        mat = getFactory().create(new int[] { 4, 4 });
        MatrixND mat2 = mat;
        int i = 0;
        for (MatrixIterator mi = mat.iterator(); mi.next();) {
            mi.setValue(++i);
        }

        Assert.assertEquals(mat, mat2);
        mat2 = mat.meanOverDim(1, 0);
        Assert.assertEquals(mat, mat2);
        mat2 = mat.meanOverDim(1, 1);
        Assert.assertEquals(mat, mat2);
        mat2 = mat.meanOverDim(1, 2);
        Assert.assertEquals(2, mat2.getDim(1));
        Assert.assertEquals(1.5, mat2.getValue(0, 0), 0);
        Assert.assertEquals(3.5, mat2.getValue(0, 1), 0);
        Assert.assertEquals(5.5, mat2.getValue(1, 0), 0);
        Assert.assertEquals(7.5, mat2.getValue(1, 1), 0);
        Assert.assertEquals(9.5, mat2.getValue(2, 0), 0);
        Assert.assertEquals(11.5, mat2.getValue(2, 1), 0);
        Assert.assertEquals(13.5, mat2.getValue(3, 0), 0);
        Assert.assertEquals(15.5, mat2.getValue(3, 1), 0);

        mat2 = mat.meanOverDim(1, 3);
        Assert.assertEquals(2, mat2.getDim(1));
        Assert.assertEquals(2.0, mat2.getValue(0, 0), 0);
        Assert.assertEquals(6.0, mat2.getValue(1, 0), 0);
        Assert.assertEquals(10.0, mat2.getValue(2, 0), 0);
        Assert.assertEquals(14.0, mat2.getValue(3, 0), 0);

        mat2 = mat.meanOverDim(1, 4);
        Assert.assertEquals(1, mat2.getDim(1));
        Assert.assertEquals(2.5, mat2.getValue(0, 0), 0);
        Assert.assertEquals(6.5, mat2.getValue(1, 0), 0);
        Assert.assertEquals(10.5, mat2.getValue(2, 0), 0);
        Assert.assertEquals(14.5, mat2.getValue(3, 0), 0);

        // meanAll
        double meanAll = mat.meanAll();
        Assert.assertEquals(8.5, meanAll, 0);

        // seconde matrice (en 3 dimension :D)
        // 2 3 3    3 3 3    3 3 3
        // 3 3 3    3 3 3    3 3 3
        // 3 3 3    3 3 3    3 3 4
        List<String> s1 = Arrays.asList(new String[] { "a", "b", "c" });
        List<String> s2 = Arrays.asList(new String[] { "e", "f", "g" });
        List<String> s3 = Arrays.asList(new String[] { "k", "l", "m" });
        mat = getFactory().create("Ma mat", new List[] { s1, s2, s3 },
                new String[] { "dim abc", "dim efg", "dim klm" });

        MatrixHelper.fill(mat, 3);
        mat.setValue(0, 0, 0, 2);
        mat.setValue(2, 2, 2, 4);
        mat = mat.meanOverDim(1);
        // donne : 
        // 2.66 3 3    3 3 3    3 3 3.33
        Assert.assertTrue(MatrixHelper.sameDimension(new int[] { 3, 1, 3 }, mat
                .getDim()));

        Assert.assertEquals(2.6666, mat.getValue(0, 0, 0), 0.0001);
        Assert.assertEquals(3, mat.getValue(2, 0, 1), 0);
        Assert.assertEquals(3.3333, mat.getValue(2, 0, 2), 0.0001);

        mat = getFactory().create(new int[] { 6, 6, 6 });

        MatrixHelper.fill(mat, 3);
        mat.setValue(0, 0, 0, 0);
        mat.setValue(0, 1, 0, 1);
        mat.setValue(0, 2, 0, 2);
        mat.setValue(0, 3, 0, 3);
        mat.setValue(0, 4, 0, 4);
        mat.setValue(0, 5, 0, 5);

        // meanAll
        meanAll = mat.meanAll();
        Assert.assertEquals(2.9861, meanAll, 0.0001);
        
        mat = mat.meanOverDim(1, 3);
        Assert.assertTrue(MatrixHelper.sameDimension(new int[] { 6, 2, 6 }, mat
                .getDim()));
        Assert.assertEquals(1.0, mat.getValue(0, 0, 0), 0);
        Assert.assertEquals(4.0, mat.getValue(0, 1, 0), 0);
        Assert.assertEquals(3.0, mat.getValue(1, 1, 5), 0);
        Assert.assertEquals(3.0, mat.getValue(5, 0, 3), 0);
    }

    /**
     * Test de reduction simple par indice.
     * 
     * @throws Exception
     */
    @Test
    public void testSubMatrixIntIntArray() throws Exception {
        List<String> s1 = Arrays.asList(new String[] { "a", "b", "c" });
        List<String> s2 = Arrays.asList(new String[] { "e", "f", "g" });
        List<String> s3 = Arrays.asList(new String[] { "k", "l", "m" });

        MatrixND mat = getFactory().create("Ma mat", new List[] { s1, s2, s3 },
                new String[] { "dim abc", "dim efg", "dim klm" });

        // reduction de "e", "f", "g" en "f", "g"
        MatrixND mat2 = mat.getSubMatrix(1, new int[]{1,2});
        Assert.assertEquals(Arrays.asList(new String[]{"f","g"}), mat2.getSemantic(1));
        mat2 = mat2.getSubMatrix(2, new int[]{2});
        Assert.assertEquals(Arrays.asList(new String[]{"m"}), mat2.getSemantic(2));
    }

    /**
     * Test de reduction simple par semantic.
     * 
     * @throws Exception
     */
    @Test
    public void testSubMatrixIntObjectArray() throws Exception {
        List<String> s1 = Arrays.asList(new String[] { "a", "b", "c" });
        List<String> s2 = Arrays.asList(new String[] { "e", "f", "g" });
        List<String> s3 = Arrays.asList(new String[] { "k", "l", "m" });

        MatrixND mat = getFactory().create("Ma mat", new List[] { s1, s2, s3 },
                new String[] { "dim abc", "dim efg", "dim klm" });

        // reduction de "e", "f", "g" en "f", "g"
        MatrixND mat2 = mat.getSubMatrix(1, "f", "g");
        Assert.assertEquals(Arrays.asList(new String[]{"f","g"}), mat2.getSemantic(1));
        mat2 = mat2.getSubMatrix(2, "m");
        Assert.assertEquals(Arrays.asList(new String[]{"m"}), mat2.getSemantic(2));
    }

    /**
     * Test de reduction multiple par indices.
     * 
     * @throws Exception
     */
    @Test
    public void testSubMatrixIntArrayArray() throws Exception {
        List<String> s1 = Arrays.asList(new String[] { "a", "b", "c" });
        List<String> s2 = Arrays.asList(new String[] { "e", "f", "g" });
        List<String> s3 = Arrays.asList(new String[] { "k", "l", "m" });

        MatrixND mat = getFactory().create("Ma mat", new List[] { s1, s2, s3 },
                new String[] { "dim abc", "dim efg", "dim klm" });

        // reduction de "e", "f", "g" en "f", "g" et "k", "l", "m" en "m"
        MatrixND mat2 = mat.getSubMatrix(new int[][]{new int[]{0, 1, 2}, new int[]{1,2}, new int[]{2}});
        Assert.assertEquals(Arrays.asList(new String[]{"f","g"}), mat2.getSemantic(1));
        Assert.assertEquals(Arrays.asList(new String[]{"m"}), mat2.getSemantic(2));
        
        // meme test avec des null
        mat2 = mat.getSubMatrix(new int[][]{null, new int[]{1,2}, new int[]{2}});
        Assert.assertEquals(Arrays.asList(new String[]{"f","g"}), mat2.getSemantic(1));
        Assert.assertEquals(Arrays.asList(new String[]{"m"}), mat2.getSemantic(2));
    }

    /**
     * Test de reduction sur le mauvais nombre de dimension.
     * 
     * @throws Exception
     */
    @Test(expected=IllegalArgumentException.class)
    public void testSubMatrixIntArrayArrayException() throws Exception {
        List<String> s1 = Arrays.asList(new String[] { "a", "b", "c" });
        List<String> s2 = Arrays.asList(new String[] { "e", "f", "g" });
        List<String> s3 = Arrays.asList(new String[] { "k", "l", "m" });

        MatrixND mat = getFactory().create("Ma mat", new List[] { s1, s2, s3 },
                new String[] { "dim abc", "dim efg", "dim klm" });

        // reduction de "e", "f", "g" en "f", "g"
        mat.getSubMatrix(new int[][]{null});
    }

    @Test
    public void testTranspose() throws Exception {
        MatrixND mat = null;

        mat = getFactory().create(new int[] { 2, 4 });
        mat.setValue(1, 3, 56);
        mat.setValue(0, 2, 34);
        mat.setValue(0, 1, 64);
        mat.setValue(1, 0, 46);

        mat = mat.transpose();
        Assert.assertEquals(56, mat.getValue(3, 1), 0);
        Assert.assertEquals(34, mat.getValue(2, 0), 0);
        Assert.assertEquals(64, mat.getValue(1, 0), 0);
        Assert.assertEquals(46, mat.getValue(0, 1), 0);

        mat = getFactory().create(new int[] { 4 });
        mat.setValue(1, 56);
        mat.setValue(2, 34);
        mat.setValue(3, 64);
        mat.setValue(0, 46);

        mat = mat.transpose();
        Assert.assertEquals(56, mat.getValue(0, 1), 0);
        Assert.assertEquals(34, mat.getValue(0, 2), 0);
        Assert.assertEquals(64, mat.getValue(0, 3), 0);
        Assert.assertEquals(46, mat.getValue(0, 0), 0);
    }

    @Test
    public void testMults() throws Exception {
        List<String> s1 = Arrays.asList(new String[] { "a", "b", "c" });
        List<String> s2 = Arrays.asList(new String[] { "e", "f", "g" });
        List<String> s3 = Arrays.asList(new String[] { "k", "l", "m" });

        MatrixND mat1 = getFactory().create("Ma mat",
                new List[] { s1, s2, s3 },
                new String[] { "dim abc", "dim efg", "dim klm" });
        MatrixND mat2 = getFactory().create("Ma mat",
                new List[] { s1, s2, s3 },
                new String[] { "dim abc", "dim efg", "dim klm" });

        MatrixHelper.fill(mat1, 3);
        MatrixHelper.fill(mat2, 26);

        mat1.mults(3);
        mat2.mults(0);

        Assert.assertEquals(9, mat1.getValue(0, 0, 0), 0);
        Assert.assertEquals(9, mat1.getValue(1, 2, 0), 0);
        Assert.assertEquals(0, mat2.getValue(2, 2, 2), 0);

        MatrixND mat0 = getFactory().create(new int[] { 4, 4 });
        MatrixND matId = getFactory().matrixId(4);
        matId.mults(0);
        Assert.assertEquals(mat0, matId);
    }

    @Test
    public void testDivs() throws Exception {
        List<String> s1 = Arrays.asList(new String[] { "a", "b", "c" });
        List<String> s2 = Arrays.asList(new String[] { "e", "f", "g" });
        List<String> s3 = Arrays.asList(new String[] { "k", "l", "m" });

        MatrixND mat1 = getFactory().create("Ma mat",
                new List[] { s1, s2, s3 },
                new String[] { "dim abc", "dim efg", "dim klm" });
        MatrixND mat2 = getFactory().create("Ma mat",
                new List[] { s1, s2, s3 },
                new String[] { "dim abc", "dim efg", "dim klm" });

        MatrixHelper.fill(mat1, 3);
        MatrixHelper.fill(mat2, 26);

        mat1.divs(3);
        mat2.divs(4);

        Assert.assertEquals(1, mat1.getValue(0, 0, 0), 0);
        Assert.assertEquals(6.5, mat2.getValue(1, 2, 0), 0);
        Assert.assertEquals(6.5, mat2.getValue(2, 2, 2), 0);
    }
    
    @Test
    public void testAdds() throws Exception {
        List<String> s1 = Arrays.asList(new String[] { "a", "b", "c" });
        List<String> s2 = Arrays.asList(new String[] { "e", "f", "g" });
        List<String> s3 = Arrays.asList(new String[] { "k", "l", "m" });

        MatrixND mat1 = getFactory().create("Ma mat",
                new List[] { s1, s2, s3 },
                new String[] { "dim abc", "dim efg", "dim klm" });
        MatrixND mat2 = getFactory().create("Ma mat",
                new List[] { s1, s2, s3 },
                new String[] { "dim abc", "dim efg", "dim klm" });

        MatrixHelper.fill(mat1, 3);
        MatrixHelper.fill(mat2, 26);

        mat1.adds(3);
        mat2.adds(4);

        Assert.assertEquals(6, mat1.getValue(0, 0, 0), 0);
        Assert.assertEquals(30, mat2.getValue(1, 2, 0), 0);
        Assert.assertEquals(30, mat2.getValue(2, 2, 2), 0);
    }
    
    @Test
    public void testMinuss() throws Exception {
        List<String> s1 = Arrays.asList(new String[] { "a", "b", "c" });
        List<String> s2 = Arrays.asList(new String[] { "e", "f", "g" });
        List<String> s3 = Arrays.asList(new String[] { "k", "l", "m" });

        MatrixND mat1 = getFactory().create("Ma mat",
                new List[] { s1, s2, s3 },
                new String[] { "dim abc", "dim efg", "dim klm" });
        MatrixND mat2 = getFactory().create("Ma mat",
                new List[] { s1, s2, s3 },
                new String[] { "dim abc", "dim efg", "dim klm" });

        MatrixHelper.fill(mat1, 3);
        MatrixHelper.fill(mat2, 26);

        mat1.minuss(3);
        mat2.minuss(4);

        Assert.assertEquals(0, mat1.getValue(0, 0, 0), 0);
        Assert.assertEquals(22, mat2.getValue(1, 2, 0), 0);
        Assert.assertEquals(22, mat2.getValue(2, 2, 2), 0);
    }

    @Test
    public void testPaste() throws Exception {

        MatrixND mat1 = getFactory().create(new int[] { 6, 7, 8 });
        MatrixND mat2 = getFactory().create(new int[] { 2, 2, 2 });

        MatrixHelper.fill(mat1, 3);
        MatrixHelper.fill(mat2, 26);

        mat1.paste(new int[] { 3, 0, 4 }, mat2);

        Assert.assertEquals(3, mat1.getValue(0, 0, 0), 0);
        Assert.assertEquals(26, mat1.getValue(3, 0, 4), 0);
        Assert.assertEquals(26, mat1.getValue(4, 1, 5), 0);
        Assert.assertEquals(3, mat1.getValue(5, 2, 6), 0);
    }

    @Test
    public void testSumOverDim() throws Exception {

        MatrixND mat = null;

        mat = getFactory().create(new int[] { 6, 7 });
        MatrixND result = getFactory().create(new int[] { 3, 7 });

        MatrixHelper.fill(mat, 3);

        MatrixND sub = mat.getSubMatrix(0, 0, 1);
        result.paste(new int[] { 0, 0 }, sub.sumOverDim(0));

        sub = mat.getSubMatrix(0, 1, 3);
        result.paste(new int[] { 1, 0 }, sub.sumOverDim(0));

        sub = mat.getSubMatrix(0, 4, 2);
        result.paste(new int[] { 2, 0 }, sub.sumOverDim(0));

        mat = getFactory().create(new int[] { 6, 7 });
        MatrixHelper.fill(mat, 3);

        mat = mat.sumOverDim(0, 0, 1);
        mat = mat.sumOverDim(0, 1, 3);
        mat = mat.sumOverDim(0, 2, 2);

        Assert.assertTrue(result.equalsValues(mat));
    }

    @Test
    public void testReduce() throws Exception {

        MatrixND mat = null;

        mat = getFactory().create(new int[] { 6, 7, 8 });
        MatrixHelper.fill(mat, 3);

        mat = mat.sumOverDim(1);
        Assert.assertEquals(21, mat.getValue(0, 0, 0), 0);
        mat = mat.reduce();
        Assert.assertEquals(2, mat.getNbDim());
        Assert.assertEquals(6, mat.getDim(0));
        Assert.assertEquals(8, mat.getDim(1));
        Assert.assertEquals(21, mat.getValue(0, 0), 0);
        Assert.assertEquals(21, mat.getValue(5, 7), 0);

        mat = getFactory().create(new int[] { 2, 2, 2 });
        MatrixHelper.fill(mat, 26);

        mat = mat.sumOverDim(1);
        mat = mat.sumOverDim(0);
        Assert.assertEquals(104, mat.getValue(0, 0, 0), 0);
        mat = mat.reduce();
        Assert.assertEquals(1, mat.getNbDim());
        Assert.assertEquals(2, mat.getDim(0));
        Assert.assertEquals(104, mat.getValue(0), 0);
        Assert.assertEquals(104, mat.getValue(1), 0);

        mat = getFactory().create(new int[] { 2, 2, 2 });
        MatrixHelper.fill(mat, 6);

        mat = mat.sumOverDim(1);
        mat = mat.sumOverDim(2);
        mat = mat.sumOverDim(0);
        Assert.assertEquals(48, mat.getValue(0, 0, 0), 0);
        mat = mat.reduce();
        Assert.assertEquals(1, mat.getNbDim());
        Assert.assertEquals(1, mat.getDim(0));
        Assert.assertEquals(48, mat.getValue(0), 0);

        mat = getFactory().create(new int[] { 6, 7, 8 });
        MatrixHelper.fill(mat, 3);

        mat = mat.sumOverDim(1);
        Assert.assertEquals(21, mat.getValue(0, 0, 0), 0);
        mat = mat.reduce(2);
        Assert.assertEquals(2, mat.getNbDim());
        Assert.assertEquals(6, mat.getDim(0));
        Assert.assertEquals(8, mat.getDim(1));
        Assert.assertEquals(21, mat.getValue(0, 0), 0);
        Assert.assertEquals(21, mat.getValue(5, 7), 0);

        mat = getFactory().create(new int[] { 2, 2, 2 });
        MatrixHelper.fill(mat, 26);

        mat = mat.sumOverDim(1);
        mat = mat.sumOverDim(0);
        Assert.assertEquals(104, mat.getValue(0, 0, 0), 0);
        mat = mat.reduce(2);
        Assert.assertEquals(2, mat.getNbDim());
        Assert.assertEquals(1, mat.getDim(0));
        Assert.assertEquals(2, mat.getDim(1));
        Assert.assertEquals(104, mat.getValue(0, 0), 0);
        Assert.assertEquals(104, mat.getValue(0, 1), 0);

        mat = getFactory().create(new int[] { 2, 2, 2 });
        MatrixHelper.fill(mat, 6);

        mat = mat.sumOverDim(1);
        mat = mat.sumOverDim(2);
        mat = mat.sumOverDim(0);
        Assert.assertEquals(48, mat.getValue(0, 0, 0), 0);
        mat = mat.reduce(5);
        Assert.assertEquals(3, mat.getNbDim());
        Assert.assertEquals(1, mat.getDim(0));
        Assert.assertEquals(1, mat.getDim(1));
        Assert.assertEquals(1, mat.getDim(2));
        Assert.assertEquals(48, mat.getValue(0, 0, 0), 0);

    }

    @Test
    public void testCreateFromDoubleArray() throws Exception {
        MatrixND mat = null;

        mat = getFactory().create(new double[] { 1, 2, 3, 4, 5, 6, 7, 8, 9 },
                new int[] { 3, 3 });

        Assert.assertEquals(2, mat.getDim().length);
        Assert.assertEquals(3, mat.getDim(0));
        Assert.assertEquals(3, mat.getDim(1));
        Assert.assertEquals(1, (int) mat.getValue(new int[] { 0, 0 }));
        Assert.assertEquals(2, (int) mat.getValue(new int[] { 0, 1 }));
        Assert.assertEquals(3, (int) mat.getValue(new int[] { 0, 2 }));
        Assert.assertEquals(4, (int) mat.getValue(new int[] { 1, 0 }));
        Assert.assertEquals(5, (int) mat.getValue(new int[] { 1, 1 }));
        Assert.assertEquals(6, (int) mat.getValue(new int[] { 1, 2 }));
        Assert.assertEquals(7, (int) mat.getValue(new int[] { 2, 0 }));
        Assert.assertEquals(8, (int) mat.getValue(new int[] { 2, 1 }));
        Assert.assertEquals(9, (int) mat.getValue(new int[] { 2, 2 }));
        
        mat = getFactory().create(new double[] { 1, 2, 3, 4, 5, 6},
                new int[] { 2, 3 });

        Assert.assertEquals(2, mat.getDim().length);
        Assert.assertEquals(2, mat.getDim(0));
        Assert.assertEquals(3, mat.getDim(1));
        Assert.assertEquals(1, (int) mat.getValue(new int[] { 0, 0 }));
        Assert.assertEquals(2, (int) mat.getValue(new int[] { 0, 1 }));
        Assert.assertEquals(3, (int) mat.getValue(new int[] { 0, 2 }));
        Assert.assertEquals(4, (int) mat.getValue(new int[] { 1, 0 }));
        Assert.assertEquals(5, (int) mat.getValue(new int[] { 1, 1 }));
        Assert.assertEquals(6, (int) mat.getValue(new int[] { 1, 2 }));
        
        mat = getFactory().create(new double[] { 1, 2, 3, 4, 5, 6},
                new int[] { 3, 2 });

        Assert.assertEquals(2, mat.getDim().length);
        Assert.assertEquals(3, mat.getDim(0));
        Assert.assertEquals(2, mat.getDim(1));
        Assert.assertEquals(1, (int) mat.getValue(new int[] { 0, 0 }));
        Assert.assertEquals(2, (int) mat.getValue(new int[] { 0, 1 }));
        Assert.assertEquals(3, (int) mat.getValue(new int[] { 1, 0 }));
        Assert.assertEquals(4, (int) mat.getValue(new int[] { 1, 1 }));
        Assert.assertEquals(5, (int) mat.getValue(new int[] { 2, 0 }));
        Assert.assertEquals(6, (int) mat.getValue(new int[] { 2, 1 }));

        mat = getFactory().create(new double[] { 1, 2, 3 }, new int[] { 3 });
        Assert.assertEquals(1, mat.getDim().length);
        Assert.assertEquals(3, mat.getDim(0));
        Assert.assertEquals(1, (int) mat.getValue(new int[] { 0 }));
        Assert.assertEquals(2, (int) mat.getValue(new int[] { 1 }));
        Assert.assertEquals(3, (int) mat.getValue(new int[] { 2 }));

        mat = getFactory().create(new double[] { 1, 2, 3, 4, 5, 6, 7, 8 },
                new int[] { 2, 2, 2 });
        Assert.assertNull(mat);

    }

    @Test
    public void testReduceDims() throws Exception {

        MatrixND mat = null;

        mat = getFactory().create(new int[] { 6, 7, 8 });
        MatrixHelper.fill(mat, 3);

        mat = mat.sumOverDim(1);
        Assert.assertEquals(21, mat.getValue(0, 0, 0), 0);
        mat = mat.reduceDims(1);
        Assert.assertEquals(2, mat.getNbDim());
        Assert.assertEquals(6, mat.getDim(0));
        Assert.assertEquals(8, mat.getDim(1));
        Assert.assertEquals(21, mat.getValue(0, 0), 0);
        Assert.assertEquals(21, mat.getValue(5, 7), 0);

        mat = getFactory().create(new int[] { 2, 2, 2 });
        MatrixHelper.fill(mat, 26);

        mat = mat.sumOverDim(1);
        mat = mat.sumOverDim(0);
        Assert.assertEquals(104, mat.getValue(0, 0, 0), 0);
        mat = mat.reduceDims(1, 0);
        Assert.assertEquals(1, mat.getNbDim());
        Assert.assertEquals(2, mat.getDim(0));
        Assert.assertEquals(104, mat.getValue(0), 0);
        Assert.assertEquals(104, mat.getValue(1), 0);

        mat = getFactory().create(new int[] { 2, 2, 2 });
        MatrixHelper.fill(mat, 6);

        mat = mat.sumOverDim(1);
        mat = mat.sumOverDim(2);
        mat = mat.sumOverDim(0);
        Assert.assertEquals(48, mat.getValue(0, 0, 0), 0);
        mat = mat.reduceDims(1, 2, 0);
        Assert.assertEquals(1, mat.getNbDim());
        Assert.assertEquals(1, mat.getDim(0));
        Assert.assertEquals(48, mat.getValue(0), 0);

        mat = getFactory().create(new int[] { 6, 7, 8 });
        MatrixHelper.fill(mat, 3);

        mat = mat.sumOverDim(1);
        Assert.assertEquals(21, mat.getValue(0, 0, 0), 0);
        mat = mat.reduceDims(0);
        Assert.assertEquals(3, mat.getNbDim());
        Assert.assertEquals(6, mat.getDim(0));
        Assert.assertEquals(1, mat.getDim(1));
        Assert.assertEquals(8, mat.getDim(2));
        Assert.assertEquals(21, mat.getValue(0, 0, 0), 0);
        Assert.assertEquals(21, mat.getValue(5, 0, 7), 0);

        mat = getFactory().create(new int[] { 2, 2, 2 });
        MatrixHelper.fill(mat, 26);

        mat = mat.sumOverDim(1);
        mat = mat.sumOverDim(0);
        Assert.assertEquals(104, mat.getValue(0, 0, 0), 0);
        mat = mat.reduceDims(1);
        Assert.assertEquals(2, mat.getNbDim());
        Assert.assertEquals(1, mat.getDim(0));
        Assert.assertEquals(2, mat.getDim(1));
        Assert.assertEquals(104, mat.getValue(0, 0), 0);
        Assert.assertEquals(104, mat.getValue(0, 1), 0);

        mat = getFactory().create(new int[] { 2, 2, 2 });
        MatrixHelper.fill(mat, 6);

        mat = mat.sumOverDim(1);
        mat = mat.sumOverDim(2);
        mat = mat.sumOverDim(0);
        Assert.assertEquals(48, mat.getValue(0, 0, 0), 0);
        mat = mat.reduceDims(0, 1, 2);
        Assert.assertEquals(1, mat.getNbDim());
        Assert.assertEquals(1, mat.getDim(0));
        Assert.assertEquals(48, mat.getValue(0), 0);

    }

    @Test
    public void testToList() throws Exception {
        MatrixND mat1 = getFactory().create(new int[] { 3, 2, 1 });
        mat1.setValue(0, 0, 0, 1.0E-7);
        mat1.setValue(1, 1, 0, 2);
        mat1.setValue(2, 1, 0, 5.0E-7);
        List l = mat1.toList();
//        System.out.println(l);

        MatrixND mat2 = getFactory().create(new int[] { 3, 2, 1 });
        mat2.fromList(l);

//        System.out.println(mat1);
//        System.out.println(mat2);

        Assert.assertEquals(mat1, mat2);

        String s = String.valueOf(l);
        List l2 = convertStringToList2(s);
        List l1 = convertStringToList1(s);
        List l0 = convertStringToList0(s);

        System.out.println(l);
        System.out.println(l2);
        System.out.println(l1);
        System.out.println(l0); // implatation fausse

        Assert.assertEquals(l, l2);

        int MAX = 10000;
        int dummy = 0;
        {
            long timeStart = System.nanoTime();
            for (int i = 0; i < MAX; i++) {
                List<Object> tmp = convertStringToList1(s);
                dummy += tmp.size();
            }
            long time = System.nanoTime() - timeStart;
            time = time / 1000000;
            System.out.println(MAX + " call to convertStringToList time: "
                    + DurationFormatUtils.formatDuration(time, "s'.'S"));
        }
        {
            long timeStart = System.nanoTime();
            for (int i = 0; i < MAX; i++) {
                List<Object> tmp = convertStringToList2(s);
                dummy += tmp.size();
            }
            long time = System.nanoTime() - timeStart;
            time = time / 1000000;
            System.out.println(MAX + " call to convertStringToList2 time: "
                    + DurationFormatUtils.formatDuration(time, "s'.'S"));
        }
        System.out.println("dummy: " + dummy);
    }

    /**
     * implantation peut performante.
     * 
     * @param s String to parse
     * @return converted list
     */
    public static List<Object> convertStringToList1(String s) {
        List<Object> result = new ArrayList<Object>();
        String locals = s.trim();
        if (locals.startsWith("[") && locals.endsWith("]")) {
            locals = locals.substring(1, locals.length() - 1);
        }
        String[] las = StringUtil.split(locals, ",");
        for (String l : las) {
            l = l.trim();
            if (l.startsWith("[")) {
                // another list, recursive call
                result.add(convertStringToList1(l));
            } else {
                // if no list, must be number
                Double d = Double.valueOf(l);
                result.add(d);
            }
        }
        return result;
    }

    /**
     * implantation utilisé actuellement dans MatrixHelper.
     * 
     * @param s string to parse
     * @return converted string
     */
    public static List<Object> convertStringToList2(String s) {
        List<Object> result = null;
        Stack<List<Object>> stack = new Stack<List<Object>>();
        StringBuffer number = new StringBuffer(20); // initial to 20 char

        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            if (c == ' ') {
                // skip space
            }
            if (c == '[') {
                stack.push(new ArrayList<Object>());
            } else if (c == ',') {
                if (number.length() != 0) {
                    // on a une ',' on doit donc avoir un nombre dans number
                    // a moins que ce ne soit une ',' entre deux listes
                    Double value = Double.valueOf(number.toString());
                    stack.peek().add(value);
                }
                number.setLength(0);
            } else if (c == ']') {
                // fin d'une liste, il doit rester un nombre dans number
                // a mois que la liste etait vide
                if (number.length() != 0) {
                    Double value = Double.valueOf(number.toString());
                    stack.peek().add(value);
                    number.setLength(0);
                }

                List<Object> current = stack.pop();
                if (stack.empty()) {
                    result = current;
                } else {
                    stack.peek().add(current);
                }
            } else {
                // pas un '[' ou ']', pas une ',' devrait etre
                // un bout du nombre, si c un espace qui traine n'importe ou
                // c pas grace car Double.valueOf gere les espaces
                number.append(c);
            }
        }
        return result;
    }

    /**
     * implantation fausse pour les nombres 5.0E-7.
     * 
     * @param s string to parse
     * @return converted list
     * @throws IOException
     */
    public static List<Object> convertStringToList0(String s) throws IOException {
        List<Object> result = null;
        Stack<List<Object>> stack = new Stack<List<Object>>();
        //StringBuffer number = new StringBuffer(20); // initial to 20 char

        StreamTokenizer tok = new StreamTokenizer(new StringReader(s));
        int v = tok.nextToken();
        while (v != StreamTokenizer.TT_EOF) {
            if (v == '[') {
                stack.push(new ArrayList<Object>());
            } else if (v == ']') {
                List<Object> current = stack.pop();
                if (stack.empty()) {
                    result = current;
                } else {
                    stack.peek().add(current);
                }
            } else if (v == StreamTokenizer.TT_NUMBER) {
                double value = tok.nval;
                stack.peek().add(value);
            }
            v = tok.nextToken();
        }
        return result;
    }

    @Test
    public void testSemanticsArraySynthax() throws Exception {

        List<String>[] semantics = new ArrayList[3];
        semantics[0] = new ArrayList<String>();
        semantics[0].add("a");
        semantics[1] = new ArrayList<String>();
        semantics[1].add("b");
        semantics[2] = new ArrayList<String>();
        semantics[2].add("c");
        MatrixND mat = getFactory().create(semantics);
        
        // this don't work in 2.0.0
        // List<String> a = mat.getSemantics()[0];
        mat.setSemantic(0, new ArrayList<String>());
    }
}
