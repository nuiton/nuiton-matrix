/*
 * #%L
 * Nuiton Matrix :: API
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.math.matrix;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Test;

/**
 * MatrixHelperTest.
 *
 * Created: 29 oct. 2004
 *
 * @author Benjamin Poussin &lt;poussin@codelutin.com&gt;
 * @version $Revision$
 *
 * Mise a jour: $Date$
 * par : $Author$
 */
public class MatrixHelperDoubleBigVectorTest { // MatrixHelperTest

    /** to use log facility, just put in your code: log.info(\"...\"); */
    private static Log log = LogFactory.getLog(MatrixHelperDoubleBigVectorTest.class);

    public MatrixFactory getFactory() {
        return MatrixFactory.getInstance(DoubleBigVector.class);
    }

    @Test
    public void testCoordinatesToString() {
        Assert.assertEquals("1", MatrixHelper
                .coordinatesToString(new int[] { 1 }));
        Assert.assertEquals("2,3,4,5", MatrixHelper
                .coordinatesToString(new int[] { 2, 3, 4, 5 }));
        Assert.assertEquals("2,3,4,5,234", MatrixHelper
                .coordinatesToString(new int[] { 2, 3, 4, 5, 234 }));

        Assert.assertEquals("a", MatrixHelper
                .coordinatesToString(new String[] { "a" }));
        Assert.assertEquals("a,b,n,m", MatrixHelper
                .coordinatesToString(new String[] { "a", "b", "n", "m" }));
        Assert.assertEquals("a,b,f,e,aze",
                MatrixHelper.coordinatesToString(new String[] { "a", "b", "f",
                        "e", "aze" }));
    }

    @Test
    public void testSameDimension() {
        Assert.assertTrue(MatrixHelper.sameDimension(new int[] { 1 },
                new int[] { 1 }));
        Assert.assertTrue(MatrixHelper.sameDimension(new int[] { 1, 2 },
                new int[] { 1, 2 }));
        Assert.assertTrue(MatrixHelper.sameDimension(new int[] { 1, 324, 3 },
                new int[] { 1, 324, 3 }));
    }

    @Test
    public void testDimensionToSemantics() {
        // TODO faire un test pour dimensionToSemantics
    }

    @Test
    public void testSemanticsToDimension() {
        // TODO faire un test pour semanticsToDimension
    }

    @Test
    public void testFill() {
        MatrixND mat = getFactory().create(new int[] { 3, 3 });
        MatrixHelper.fill(mat, 4);

        Assert.assertEquals(4, mat.getValue(1, 1), 0);
    }

    @Test
    public void testMatrixId() {
        MatrixND mat = getFactory().matrixId(4);
        Assert.assertTrue(MatrixHelper.sameDimension(new int[] { 4, 4 }, mat
                .getDim()));
        Assert.assertEquals(0, mat.getValue(1, 2), 0);
        Assert.assertEquals(1, mat.getValue(0, 0), 0);
        Assert.assertEquals(1, mat.getValue(1, 1), 0);
        Assert.assertEquals(1, mat.getValue(2, 2), 0);
        Assert.assertEquals(1, mat.getValue(3, 3), 0);
    }

    @Test
    public void testToList() {
        MatrixND mat1 = getFactory().create(new int[] { 3, 2, 1 });
        mat1.setValue(0, 0, 0, -1.0E-7);
        mat1.setValue(1, 1, 0, 2);
        mat1.setValue(2, 1, 0, 5.0E-7);

        List l = mat1.toList();
        String s = String.valueOf(l);
        List l2 = MatrixHelper.convertStringToList(s);

        System.out.println(l);
        System.out.println(l2);

        Assert.assertEquals(l, l2);

    }

    @Test
    public void testMaxOccurrence() {
        double[] val = new double[5];

        Assert.assertEquals(0, MatrixHelper.maxOccurrence(val), 0);

        val[2] = -1;
        Assert.assertEquals(0, MatrixHelper.maxOccurrence(val), 0);
        val[0] = -1;
        Assert.assertEquals(0, MatrixHelper.maxOccurrence(val), 0);
        val[1] = -1;
        Assert.assertEquals(-1, MatrixHelper.maxOccurrence(val), 0);
        val[4] = -3;
        Assert.assertEquals(-1, MatrixHelper.maxOccurrence(val), 0);
        val[3] = 3;
        Assert.assertEquals(-1, MatrixHelper.maxOccurrence(val), 0);

        val = new double[6];

        Assert.assertEquals(0, MatrixHelper.maxOccurrence(val), 0);

        val[2] = -1;
        Assert.assertEquals(0, MatrixHelper.maxOccurrence(val), 0);
        val[0] = -1;
        Assert.assertEquals(0, MatrixHelper.maxOccurrence(val), 0);
        val[1] = -1;
        Assert.assertEquals(-1, MatrixHelper.maxOccurrence(val), 0);
        val[4] = -3;
        Assert.assertEquals(-1, MatrixHelper.maxOccurrence(val), 0);
        val[3] = -3;
        Assert.assertEquals(-1, MatrixHelper.maxOccurrence(val), 0);
        val[5] = -3;
        Assert.assertEquals(-3, MatrixHelper.maxOccurrence(val), 0);

        val = new double[0];
        try {
            MatrixHelper.maxOccurrence(val);
            Assert.fail("An exception must be thrown");
        } catch (IllegalArgumentException e) {
            if (log.isDebugEnabled()) {
                log.debug("Exception normally thrown", e);
            }
        }

    }
    
}
