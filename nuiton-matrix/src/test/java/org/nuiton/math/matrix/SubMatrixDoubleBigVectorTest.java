/*
 * #%L
 * Nuiton Matrix :: API
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.math.matrix;

import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

/**
 * SubMatrixDoubleBigTest.
 * 
 * Created: 29 oct. 2004
 * 
 * @author Benjamin Poussin &lt;poussin@codelutin.com&gt;
 * @version $Revision$
 * 
 * Mise a jour: $Date$
 * par : $Author$
 */
public class SubMatrixDoubleBigVectorTest { // SubMatrixTest

    public MatrixFactory getFactory() throws Exception {
        return MatrixFactory.getInstance(DoubleBigVector.class);
    }

    @Test
    public void testNew() throws Exception {
        List s1 = Arrays.asList(new String[] { "a", "b", "c" });
        List s2 = Arrays.asList(new String[] { "e", "f", "g" });
        List s3 = Arrays.asList(new String[] { "k", "l", "m" });

        MatrixND mat = null;

        mat = getFactory().create("Ma mat", new List[] { s1, s2, s3 },
                new String[] { "dim abc", "dim efg", "dim klm" });

        MatrixND smat = mat.getSubMatrix(1, 0, 2);
        Assert.assertTrue(Arrays.equals(new int[] { 3, 2, 3 }, smat.getDim()));
    }

    @Test
    public void testIterator() throws Exception {
        int[][] val18 = new int[][] { { 0, 0, 0 }, { 0, 0, 1 }, { 0, 0, 2 },
                { 0, 1, 0 }, { 0, 1, 1 }, { 0, 1, 2 }, { 1, 0, 0 },
                { 1, 0, 1 }, { 1, 0, 2 }, { 1, 1, 0 }, { 1, 1, 1 },
                { 1, 1, 2 }, { 2, 0, 0 }, { 2, 0, 1 }, { 2, 0, 2 },
                { 2, 1, 0 }, { 2, 1, 1 }, { 2, 1, 2 }, };

        String[][] vals18 = new String[][] { { "a", "e", "k" },
                { "a", "e", "l" }, { "a", "e", "m" }, { "a", "f", "k" },
                { "a", "f", "l" }, { "a", "f", "m" }, { "b", "e", "k" },
                { "b", "e", "l" }, { "b", "e", "m" }, { "b", "f", "k" },
                { "b", "f", "l" }, { "b", "f", "m" }, { "c", "e", "k" },
                { "c", "e", "l" }, { "c", "e", "m" }, { "c", "f", "k" },
                { "c", "f", "l" }, { "c", "f", "m" }, };

        List s1 = Arrays.asList(new String[] { "a", "b", "c" });
        List s2 = Arrays.asList(new String[] { "e", "f", "g" });
        List s3 = Arrays.asList(new String[] { "k", "l", "m" });

        MatrixND mat = null;

        mat = getFactory().create("Ma mat", new List[] { s1, s2, s3 },
                new String[] { "dim abc", "dim efg", "dim klm" });

        MatrixND smat = mat.getSubMatrix(1, 0, 2);

        // System.out.println(MatrixHelper.coordinatesToString(smat.getDim()));

        int cpt = 0;
        for (MatrixIterator i = smat.iterator(); i.hasNext();) {
            i.next();

            // System.out.println(MatrixHelper.coordinatesToString(i.
            // getCoordinates()));
            // System.out.println(MatrixHelper.coordinatesToString(i.
            // getSemanticsCoordinates()));

            Assert.assertTrue(Arrays.equals(val18[cpt], i.getCoordinates()));
            Assert.assertTrue(Arrays.equals(vals18[cpt], i.getSemanticsCoordinates()));
            cpt++;
        }
        Assert.assertEquals(18, cpt);
    }

    @Test
    public void testSubSubMatrix() throws Exception {
        int[][] val6 = new int[][] { { 0, 0, 0 }, { 0, 0, 1 }, { 0, 0, 2 },
                { 0, 1, 0 }, { 0, 1, 1 }, { 0, 1, 2 }, };

        String[][] vals6 = new String[][] { { "c", "e", "k" },
                { "c", "e", "l" }, { "c", "e", "m" }, { "c", "f", "k" },
                { "c", "f", "l" }, { "c", "f", "m" }, };

        List s1 = Arrays.asList(new String[] { "a", "b", "c" });
        List s2 = Arrays.asList(new String[] { "e", "f", "g" });
        List s3 = Arrays.asList(new String[] { "k", "l", "m" });

        MatrixND mat = null;

        mat = getFactory().create("Ma mat", new List[] { s1, s2, s3 },
                new String[] { "dim abc", "dim efg", "dim klm" });

        MatrixND smat1 = mat.getSubMatrix(1, 0, 2);
        MatrixND smat2 = smat1.getSubMatrix(0, 2, 1);

        //System.out.println(" smat2.getDim: "+MatrixHelper.coordinatesToString(
        // smat2.getDim()));
        Assert.assertTrue(Arrays.equals(new int[] { 1, 2, 3 }, smat2.getDim()));

        // System.out.println(MatrixHelper.coordinatesToString(smat2.getDim()));

        int cpt = 0;
        for (MatrixIterator i = smat2.iterator(); i.hasNext();) {
            i.next();

            Assert.assertTrue(Arrays.equals(val6[cpt], i.getCoordinates()));
            Assert.assertTrue(Arrays.equals(vals6[cpt], i.getSemanticsCoordinates()));
            cpt++;
        }
        Assert.assertEquals(6, cpt);
    }

    @Test
    public void testSubSubGetSet() throws Exception {
        List s1 = Arrays.asList(new String[] { "a", "b", "c" });
        List s2 = Arrays.asList(new String[] { "e", "f", "g" });
        List s3 = Arrays.asList(new String[] { "k", "l", "m" });

        MatrixND mat = null;

        mat = getFactory().create("Ma mat", new List[] { s1, s2, s3 },
                new String[] { "dim abc", "dim efg", "dim klm" });

        MatrixND smat1 = mat.getSubMatrix(1, 0, 2);
        MatrixND smat2 = smat1.getSubMatrix(0, 2, 1);

        smat2.setValue(0, 0, 0, 34);
        Assert.assertEquals(34, smat2.getValue("c", "e", "k"), 0);
        smat2.setValue("c", "f", "l", 23);
        Assert.assertEquals(23, smat2.getValue(0, 1, 1), 0);
    }

    @Test
    public void testSubMapping() throws Exception {
        List s1 = Arrays.asList(new String[] { "a", "b", "c" });
        List s2 = Arrays.asList(new String[] { "e", "f", "g" });
        List s3 = Arrays.asList(new String[] { "k", "l", "m" });

        MatrixND mat = null;

        mat = getFactory().create("Ma mat", new List[] { s1, s2, s3 },
                new String[] { "dim abc", "dim efg", "dim klm" });

        MatrixND smat1 = mat.getSubMatrix(1, new int[] { 0, 1 });
        Assert.assertEquals(2, smat1.getDim(1));
        Assert.assertEquals(Arrays.asList(new String[] { "e", "f" }), smat1
                .getSemantics(1));
        MatrixND smat2 = smat1.getSubMatrix(0, new Object[] { "c" });
        Assert.assertEquals(1, smat2.getDim(0));
        Assert.assertEquals(Arrays.asList(new String[] { "c" }), smat2.getSemantics(0));

        mat.setValue("c", "f", "k", 12);
        Assert.assertEquals(12, smat1.getValue(2, 1, 0), 0);
        Assert.assertEquals(12, smat1.getValue("c", "f", "k"), 0);
        Assert.assertEquals(12, smat2.getValue(0, 1, 0), 0);
        Assert.assertEquals(12, smat2.getValue("c", "f", "k"), 0);

        smat2.setValue(0, 0, 0, 34);
        Assert.assertEquals(34, smat2.getValue("c", "e", "k"), 0);
        Assert.assertEquals(34, mat.getValue(2, 0, 0), 0);
        Assert.assertEquals(34, mat.getValue("c", "e", "k"), 0);
        smat2.setValue("c", "f", "l", 23);
        Assert.assertEquals(23, smat2.getValue(0, 1, 1), 0);
        Assert.assertEquals(23, mat.getValue(2, 1, 1), 0);
        Assert.assertEquals(23, mat.getValue("c", "f", "l"), 0);
    }

    @Test
    public void testSubAdd() throws Exception {
        List s1 = Arrays.asList(new String[] { "a", "b", "c" });
        List s2 = Arrays.asList(new String[] { "e", "f", "g" });
        List s3 = Arrays.asList(new String[] { "k", "l", "m" });

        MatrixND mat1 = getFactory().create("Ma mat",
                new List[] { s1, s2, s3 },
                new String[] { "dim abc", "dim efg", "dim klm" });

        MatrixND mat2 = getFactory().create("Ma mat",
                new List[] { s1, s2, s3 },
                new String[] { "dim abc", "dim efg", "dim klm" });

        MatrixHelper.fill(mat1, 5);
        MatrixHelper.fill(mat2, 3);

        MatrixND smat1 = mat1.getSubMatrix(1, "e", 1);
        MatrixND smat2 = mat2.getSubMatrix(1, "f", 1);

        smat1.add(smat2);

        Assert.assertEquals(5, mat1.getValue(0, 1, 0), 0);
        Assert.assertEquals(5, mat1.getValue(0, 1, 1), 0);
        Assert.assertEquals(5, mat1.getValue(0, 1, 2), 0);
        Assert.assertEquals(5, mat1.getValue(1, 1, 0), 0);
        Assert.assertEquals(5, mat1.getValue(1, 1, 1), 0);
        Assert.assertEquals(5, mat1.getValue(1, 1, 2), 0);
        Assert.assertEquals(5, mat1.getValue(2, 1, 0), 0);
        Assert.assertEquals(5, mat1.getValue(2, 1, 1), 0);
        Assert.assertEquals(5, mat1.getValue(2, 1, 2), 0);
        Assert.assertEquals(5, mat1.getValue(0, 2, 0), 0);
        Assert.assertEquals(5, mat1.getValue(0, 2, 1), 0);
        Assert.assertEquals(5, mat1.getValue(0, 2, 2), 0);
        Assert.assertEquals(5, mat1.getValue(1, 2, 0), 0);
        Assert.assertEquals(5, mat1.getValue(1, 2, 1), 0);
        Assert.assertEquals(5, mat1.getValue(1, 2, 2), 0);
        Assert.assertEquals(5, mat1.getValue(2, 2, 0), 0);
        Assert.assertEquals(5, mat1.getValue(2, 2, 1), 0);
        Assert.assertEquals(5, mat1.getValue(2, 2, 2), 0);

        Assert.assertEquals(8, mat1.getValue(0, 0, 0), 0);
        Assert.assertEquals(8, mat1.getValue(0, 0, 1), 0);
        Assert.assertEquals(8, mat1.getValue(0, 0, 2), 0);
        Assert.assertEquals(8, mat1.getValue(1, 0, 0), 0);
        Assert.assertEquals(8, mat1.getValue(1, 0, 1), 0);
        Assert.assertEquals(8, mat1.getValue(1, 0, 2), 0);
        Assert.assertEquals(8, mat1.getValue(2, 0, 0), 0);
        Assert.assertEquals(8, mat1.getValue(2, 0, 1), 0);
        Assert.assertEquals(8, mat1.getValue(2, 0, 2), 0);

    }

    @Test
    public void testSubSubMults() throws Exception {
        MatrixND mat = null;

        mat = getFactory().create("Ma mat", new int[] { 4, 4 });

        MatrixHelper.fill(mat, 4);

        mat = getFactory().create(mat);

        MatrixND smat1 = mat.getSubMatrix(1, 0, 2);
        MatrixND smat2 = smat1.getSubMatrix(0, 2, 1);

        System.out.println("mat:" + mat);
        System.out.println("smat1:" + smat1);
        System.out.println("smat2:" + smat2);

        smat2.mults(4);

        System.out.println("mat:" + mat);
        System.out.println("smat1:" + smat1);
        System.out.println("smat2:" + smat2);

    }

}
