<!--
  #%L
  Nuiton Matrix
  %%
  Copyright (C) 2004 - 2022 CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as 
  published by the Free Software Foundation, either version 3 of the 
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Lesser Public License for more details.
  
  You should have received a copy of the GNU General Lesser Public 
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/lgpl-3.0.html>.
  #L%
-->

# Matrix CSV Import/Export

Nuiton-matrix supports 2 format to import/export matrix as string representation
in CSV file :

- one and two dimensional matrix
- n dimensional matrix

## API

To use matrix import/export method use following API:

  - MatrixND#importCSV(Reader reader, int[] position)
  - MatrixND#exportCSV(Writer writer, boolean withSemantics)


## one and two dimensional matrix

One and two dimensional CSV file is a basic CSV file containing all
values separated by ';' character.

For example :

    10000.0;10000.0;10000.0;5000.0;10000.0;10000.0;10000.0;10000.0;10000.0;
    10000.0;10000.0;10000.0;5000.0;10000.0;10000.0;10000.0;10000.0;0.0;
    10000.0;10000.0;10000.0;5000.0;10000.0;10000.0;10000.0;10000.0;0.0;
    10000.0;10000.0;10000.0;5000.0;10000.0;10000.0;10000.0;10000.0;0.0;
    10000.0;10000.0;2000.0;5000.0;10000.0;10000.0;10000.0;10000.0;0.0;
    10000.0;10000.0;10000.0;5000.0;10000.0;10000.0;10000.0;10000.0;0.0;
    10000.0;10000.0;10000.0;5000.0;10000.0;10000.0;10000.0;10000.0;0.0;
    10000.0;10000.0;10000.0;5000.0;10000.0;10000.0;10000.0;10000.0;0.0;
    10000.0;10000.0;10000.0;5000.0;10000.0;10000.0;10000.0;10000.0;0.0;
    10000.0;10000.0;10000.0;5000.0;10000.0;10000.0;10000.0;10000.0;10000.0;

### With semantics

### Without semantics


## n dimensional matrix

With n dimension import, file format is a little bit more complicated. It's
must include:

- matrix dimensions
- dimensions semantics
- value for each dimension

For example, importing a 2*2*2 matrix::

    [2, 2, 2]
    java.lang.String:2009,2010
    java.lang.String:Nantes,Lille
    java.lang.String:Informatique,Administration
    0;0;0;0.0
    0;0;1;1.0
    0;1;1;4.0
    1;0;0;-4.0E-7
    1;0;1;2.0
    1;1;0;4.0
    1;1;1;42.0

### Using semantics mapper

Previous example was using only "String" to lookup for semantics. You can use
an additional SemanticMapper to use custom object.

For example:

    [2, 2, 2]
    Year:2009,2010
    City:Nantes,Lille
    Department:Informatique,Administration
    0;0;0;0.0
    0;0;1;1.0
    0;1;1;4.0
    1;0;0;-4.0E-7
    1;0;1;2.0
    1;1;0;4.0
    1;1;1;42.0

The SemanticMapper will be called to resolv object identified by ids such as '2009',
'2010'...

Here is a SemanticMapper sample code:

    public class IsisMatrixSemanticMapper extends SemanticMapper {
    
        public Class getType(String typeName) {
        
            // In simulation context :
            Class clazz = null;
            try {
                clazz = Class.forName("org.exemple." + typeName);
            } catch (Exception ex) {
                clazz = String.class;
            }
            return clazz;
        }
        
        public Object getValue(Class type, String valueId) {
        
            // In simulation context :
            Object value = null;
            try {
                // lookup in database object identified by valueId
                [...]
        
            } catch (Exception ex) {
                log.warn("Can't get value for " + valueId, ex);
                value = valueId;
            }
            return value;
        
            //return valueId;
        }
    }
