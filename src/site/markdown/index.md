<!--
  #%L
  Nuiton Matrix
  %%
  Copyright (C) 2004 - 2022 CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as 
  published by the Free Software Foundation, either version 3 of the 
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Lesser Public License for more details.
  
  You should have received a copy of the GNU General Lesser Public 
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/lgpl-3.0.html>.
  #L%
-->

# NuitonMatrix

## Présentation

NuitonMatrix est une librairie, pour la manipulation de matrice à n dimensions. 
Elle contient les fonctions mathématiques classiques sur les matrices, somme, 
multiplication, produit scalaire, produit matriciel...

Cette librairie dispose de plus un ensemble de composants pour faciliter son 
intégration dans les interfaces graphiques de type swing.

## Fonctionnalités

Les principales fonctionnalités de cette librairie sont :

  - gestion des matrices à multi-dimensions
  - gestion des matrices creuses
  - gestion du copier/coller d'une sous-matrice
  - itération sur les matrices
  - import/export CSV
  - composants graphiques
  - génération automatique de graphique basé sur JFreeChart

**Veuillez consulter la JavaDoc pour de plus amples détails.**
