<!--
  #%L
  Nuiton Matrix
  %%
  Copyright (C) 2004 - 2022 CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as 
  published by the Free Software Foundation, either version 3 of the 
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Lesser Public License for more details.
  
  You should have received a copy of the GNU General Lesser Public 
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/lgpl-3.0.html>.
  #L%
-->

# La serialization des matrices

Les matrices sont seriablisable si les éléments de la matrice les sémantiques
de la matrice le sont.

Les matrices on un numero serialVersionUID qui ne change jamais. Mais elles
ont en plus un matrixVersionUID qui varie lorsque les champs changes. De
cette façon on peut modifier la lecture du flux en fonction de
matrixVersionUID. La valeur de matrixVersionUID est la première chose que
l'on ecrit dans le flux.

Il faut aussi faire le meme travail de serialisation sur BasicMatrix et
DimensionConverter.

## Serialisation

Dans MatrixNDImpl et SubMatrix

ecriture

- matrixVersionUID (long)
- nom (String)
- defaultValue (double)
- dimensions (int[])
- dimNames (String[])
- semantics (List[])

pour MatrixNDImpl
ecriture

- matrix (BasicMatrix)

pour SubMatrix
ecriture

- matrix (MatrixND)
- converter (DimensionConverter)

## Deserialisation

Dans MatrixNDImpl et SubMatrix

lecture

- matrixVersionUID

La suite de la lecture peut-etre conditionné a la valeur de matrixVersionUID

- nom
- defaultValue (double)
- dimensions (int[])
- dimNames (String[])
- semantics (List[])

pour MatrixNDImpl
lecture

- matrix (BasicMatrix)

pour SubMatrix
lecture

- matrix (MatrixND)
- converter (DimensionConverter)
- ...

Il faut ensuite recreer une factory, un dimHelper.

La factory est recreer en fonction du type de Vector utilisé (FloatVector,
FloatBigVector, ...). 

Pour MatrixNDImpl: matrix.data.getClass()
Pour SubMatrix: [matrix]*.matrix.data.getClass()

Pour SubMatrix il faut peut-etre passé plusieurs MatrixND avant d'arriver a
la BasicMatrix sur lequel on peut prendre le vector.
