<!--
  #%L
  Nuiton Matrix
  %%
  Copyright (C) 2004 - 2022 CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as 
  published by the Free Software Foundation, either version 3 of the 
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Lesser Public License for more details.
  
  You should have received a copy of the GNU General Lesser Public 
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/lgpl-3.0.html>.
  #L%
-->

# MatrixPanelViewer component

This component goals are : 
 - display a list of matrix to manipulate
 - display dimension of selected matrix with semantic
 - render selected matrix with selected dimensions

![MatrixUI](images/matrixpanelviewer.png "MatrixUI")

## Simple matrix viewing

This component can be used to display only one matrix.

Use following code :

    MatrixViewerPanel matrixViewerPanel = new MatrixViewerPanel();
    matrixViewerPanel.setMatrix(myMatrix);

Then `MatrixViewerPanel` is a simple swing component, you can place it
wherever you want.


## Display a list of matrix

You can display multiples matrix in a `MatrixViewerPanel`. User will have
to select it in a `ComboBox`.

For performance reason, instantiating multiples huge matrix at init time can't
be done. The matrix list can only contains matrix name, and matrix data
will be loaded with a provider when user select it.

Code example:

    MatrixViewerPanel matrixViewerPanel = new MatrixViewerPanel();
    matrixViewerPanel.setComboBoxVisible(true);
    matrixViewerPanel.addMatrix("myMatrix1");
    matrixViewerPanel.addMatrix("myMatrix2");
    matrixViewerPanel.setMatrixLabelProvider(new MyMatrixLabelProvider());


## Use various renderer

MatrixPanelViewer can render matrix in various way, including your own (just
implements `MatrixRenderer` interface) :

    MatrixViewerPanel matrixViewerPanel = new MatrixViewerPanel();
    matrixViewerPanel.addMatrixRenderer(new MatrixChartRenderer());
    matrixViewerPanel.addMatrixRenderer(new MatrixInfoTableRenderer());
    matrixViewerPanel.addMatrixRenderer(new MyCustomMatrixRenderer());


## Use matrix filter

You can also add a matrix filter to modify matrix just after dimension
selection and before applying all renderers.

For example, you can use a filter to remove non significant values
from a matrix (0 at each bounds).

To do so:

    MatrixViewerPanel matrixViewerPanel = new MatrixViewerPanel();
    matrixViewerPanel.addMatrixFilter(new MyCustomFilter());


## Use matrix proxy and data provider

When matrix contains huge set of data, render then is not possible. Do solve
this, you can use a matrix "empty" just filled with semantics for each
dimension. At rendering phase, `MatrixViewerPanel` will call getSubMatrix()
with selected semantics on proxy witch is implemented by a call to
`MatrixProvider` to get filled data.

Example::

    MatrixND matrix = MatrixFactory.getInstance().createProxy("myMatrixName,
                  new List<?>[] { semantic1 , semantic2, semantic3, semantic4},
                  new String[] {"semantic1", "semantic2", "semantic3", "semantic4"},
                  new MyMatrixProvider());
    MatrixViewerPanel matrixViewerPanel = new MatrixViewerPanel();
    matrixViewerPanel.setMatrix(matrix);
