/*
 * #%L
 * Nuiton Matrix :: GUI
 * %%
 * Copyright (C) 2004 - 2011 CodeLutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.math.matrix.gui;

import static org.nuiton.i18n.I18n.t;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JFileChooser;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.JSeparator;
import javax.swing.filechooser.FileFilter;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.math.matrix.MatrixHelper;
import org.nuiton.math.matrix.MatrixND;
import org.nuiton.util.FileUtil;

/**
 * Ajout d'un menu contextuel sur la matrice dans l'editeur.
 * 
 * Created: 22 mars 2006 12:11:46
 *
 * @author ruchaud
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class MatrixPopupMenu extends JPopupMenu {

    /** serialVersionUID. */
    private static final long serialVersionUID = 3349189688987885915L;

    /** Logger for this class. */
    private static final Log log = LogFactory.getLog(MatrixPopupMenu.class);

    /** Matrix editor where this menu is attached. */
    protected MatrixEditor matrixEditor;
    
    /** File chooser. */
    protected JFileChooser fileChooser;

    /** Send to clip board menu item. */
    protected JMenu sendToClipBoard;

    /** Send to file menu item. */
    protected JMenu sendToFile;

    /** Export with semantics checkbox. */
    protected JCheckBoxMenuItem withSemantics;

    /** Copy all matrix action. */
    protected Action sendToClipBoardAllCopyAction;

    /** Paste all matrix action. */
    protected Action sendToClipBoardAllPasteAction;
    
    /** Copy selection matrix action. */
    protected Action sendToClipBoardSelectionCopyAction;
    
    /** Paste selection matrix action. */
    protected Action sendToClipBoardCurrentPasteAction;

    /** Export all matrix to file action. */
    protected Action sendToFileAllCopyAction;
    
    /** Import all matrix from file. */
    protected Action sendToFileAllPasteAction;
    
    /** Export selection to file. */
    protected Action sendToFileSelectionCopyAction;
    
    /** Import selection from file. */
    protected Action sendToFileCurrentPasteAction;

    /**
     * Init popop menu.
     * 
     * @param matrixEditor matrix editor where menu is attached
     */
    public MatrixPopupMenu(MatrixEditor matrixEditor) {
        this.matrixEditor = matrixEditor;

        sendToClipBoard = getSendToClipBoard();
        sendToFile = getSendToFile();

        withSemantics = new JCheckBoxMenuItem(
                t("nuitonmatrix.menu.option.semantics"), false);

        JMenu applyDataAction = getApplyDataAction();

        add(sendToClipBoard);
        add(sendToFile);
        add(withSemantics);
        add(new JSeparator());
        add(applyDataAction);
    }

    /**
     * Init send to clip board action.
     * 
     * @return retourne le menu d'action pour le bloc note
     */
    protected JMenu getSendToClipBoard() {
        if (sendToClipBoard == null) {
            sendToClipBoard = new JMenu(t("nuitonmatrix.menu.action"));
            JMenuItem sendToClipBoardAllCopy = new JMenuItem(
                    t("nuitonmatrix.menu.action.copy"));
            JMenuItem sendToClipBoardAllPaste = new JMenuItem(
                    t("nuitonmatrix.menu.action.paste"));
            JMenuItem sendToClipBoardSelectionCopy = new JMenuItem(
                    t("nuitonmatrix.menu.action.copy.selection"));
            JMenuItem sendToClipBoardCurrentPaste = new JMenuItem(
                    t("nuitonmatrix.menu.action.paste.position"));

            sendToClipBoard.add(sendToClipBoardAllCopy);
            sendToClipBoard.add(sendToClipBoardAllPaste);
            sendToClipBoard.add(new JSeparator());
            sendToClipBoard.add(sendToClipBoardSelectionCopy);
            sendToClipBoard.add(sendToClipBoardCurrentPaste);

            sendToClipBoardAllCopy
                    .addActionListener(getSendToClipBoardAllCopyAction());
            sendToClipBoardAllPaste
                    .addActionListener(getSendToClipBoardAllPasteAction());
            sendToClipBoardSelectionCopy
                    .addActionListener(getSendToClipBoardSelectionCopyAction());
            sendToClipBoardCurrentPaste
                    .addActionListener(getSendToClipBoardCurrentPasteAction());
        }

        return sendToClipBoard;
    }

    /**
     * Init export to file action.
     * 
     * @return retourne le menu d'action pour les fichiers CSV
     */
    protected JMenu getSendToFile() {
        if (sendToFile == null) {
            sendToFile = new JMenu(t("nuitonmatrix.menu.csv"));
            JMenuItem sendToFileAllCopy = new JMenuItem(
                    t("nuitonmatrix.menu.csv.export.file"));
            JMenuItem sendToFileAllPaste = new JMenuItem(
                    t("nuitonmatrix.menu.csv.import.file"));
            JMenuItem sendToFileSelectionCopy = new JMenuItem(
                    t("nuitonmatrix.menu.csv.export.selection"));
            JMenuItem sendToFileCurrentPaste = new JMenuItem(
                    t("nuitonmatrix.menu.csv.import.position"));

            sendToFile.add(sendToFileAllCopy);
            sendToFile.add(sendToFileAllPaste);
            sendToFile.add(new JSeparator());
            sendToFile.add(sendToFileSelectionCopy);
            sendToFile.add(sendToFileCurrentPaste);

            sendToFileAllCopy.addActionListener(getSendToFileAllCopyAction());
            sendToFileAllPaste.addActionListener(getSendToFileAllPasteAction());
            sendToFileSelectionCopy
                    .addActionListener(getSendToFileSelectionCopyAction());
            sendToFileCurrentPaste
                    .addActionListener(getSendToFileCurrentPasteAction());
        }

        return sendToFile;
    }
    
    /**
     * Init export to file action.
     * 
     * @return retourne le menu d'action pour les fichiers CSV
     */
    protected JMenu getApplyDataAction() {
        JMenu getApplyDataAction = new JMenu(t("nuitonmatrix.menu.data"));
        JMenuItem dataIdentityItem = new JMenuItem(
                t("nuitonmatrix.menu.data.identity"));
        JMenuItem dataFillValueItem = new JMenuItem(
                t("nuitonmatrix.menu.data.fillvalue"));

        getApplyDataAction.add(dataIdentityItem);
        getApplyDataAction.add(dataFillValueItem);

        dataIdentityItem.addActionListener(new AbstractAction() {
            private static final long serialVersionUID = -2263800006423028369L;
            @Override
            public void actionPerformed(ActionEvent e) {
                dataIdentityPerformed();
            }
        });
        dataFillValueItem.addActionListener(new AbstractAction() {
            private static final long serialVersionUID = -2263800006423028369L;
            @Override
            public void actionPerformed(ActionEvent e) {
                dataFillValuePerformed();
            }
        });

        return getApplyDataAction;
    }

    /**
     * Init export to file writer.
     * 
     * @return retourne un writer du fichier choisi dans le selecteur de fichier
     * @throws IOException
     */
    protected Writer getFileChooserWriter() throws IOException {
        int returnVal = getFileChooser().showOpenDialog(matrixEditor);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            File selectedFile = getFileChooser().getSelectedFile();

            // add csv extension if not already set in selected file
            if (!selectedFile.getName().endsWith(".csv")) {
                selectedFile = new File(selectedFile.getAbsolutePath() + ".csv");
            }
            return FileUtil.getWriter(selectedFile);
        }
        return null;
    }

    /**
     * Init export to clip board writer.
     * 
     * @return retourne un writer pour le bloc note
     */
    protected Writer getClipBoardWriter() {
        return new StringWriter();
    }

    /**
     * Init import from file reader.
     * 
     * @return retourne un reader du fichier choisi dans le selecteur de fichier
     * @throws IOException
     */
    protected Reader getFileChooserReader() throws IOException {
        int returnVal = getFileChooser().showOpenDialog(matrixEditor);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            File selectedFile = getFileChooser().getSelectedFile();
            return FileUtil.getReader(selectedFile);
        }
        return null;
    }

    /**
     * Init import from clip board reader.
     * 
     * @return retourne le contenu du bloc note sous la forme d'un reader
     * @throws IOException 
     * @throws UnsupportedFlavorException 
     */
    protected Reader getClipBoardReader() throws UnsupportedFlavorException, IOException {
        Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
        Transferable contents = clipboard.getContents(clipboard);
        if (contents != null) {
            String data = (String) contents
                    .getTransferData(DataFlavor.stringFlavor);
            return new StringReader(data);
        }
        return null;
    }

    /**
     * Desactive le menu si la matrice ne supporte pas le mode CSV.
     */
    @Override
    protected void firePopupMenuWillBecomeVisible() {
        if (!getMatrix().isSupportedCSV()) {
            sendToClipBoard.setEnabled(false);
            sendToFile.setEnabled(false);
        } else {
            sendToClipBoard.setEnabled(true);
            sendToFile.setEnabled(true);
        }
        super.firePopupMenuWillBecomeVisible();
    }

    /**
     * Get matrix in matrix editor.
     * 
     * @return matrice en cours de saisie dans l'editeur
     */
    protected MatrixND getMatrix() {
        return matrixEditor.getMatrix();
    }

    /**
     * Get selected matrix in editor.
     * 
     * @return la sous matrice en cours de saisie dans l'editeur c'est a dire la
     *         partie selectionnee
     */
    protected MatrixND getSelectedMatrix() {
        int beginSelectedColumn = matrixEditor.getTable().getSelectedColumn();
        int nbSelectedColumn = matrixEditor.getTable().getSelectedColumnCount();

        /* Prend en compte le décalage des lignes par rapport aux dimenssions */
        int nbColumnDimRow = matrixEditor.getMatrix().getDimCount() - 1;
        beginSelectedColumn -= nbColumnDimRow;
        if (beginSelectedColumn < 0) {
            beginSelectedColumn = 0;
            nbSelectedColumn -= nbColumnDimRow;
        }

        int beginSelectedRow = matrixEditor.getTable().getSelectedRow() - 1;
        int nbSelectedRow = matrixEditor.getTable().getSelectedRowCount();

        MatrixND result = null;
        if (getMatrix().getDimCount() == 1) {
            result = matrixEditor.getMatrix().getSubMatrix(0,
                    beginSelectedColumn, nbSelectedColumn);
        } else {
            result= matrixEditor.getMatrix().getSubMatrix(0, beginSelectedRow,
                    nbSelectedRow).getSubMatrix(1, beginSelectedColumn,
                    nbSelectedColumn);
        }
        return result;
    }

    /**
     * Get matrix first selection coordinates.
     * 
     * @return retourne les coordonnees de la première cellule selectionnee
     */
    protected int[] getCoordinatesFirstCellSelectedMatrix() {
        int selectedColumn = matrixEditor.getTable().getSelectedColumn();

        /* Prend en compte le décalage des lignes par rapport aux dimenssions */
        int nbColumnDimRow = matrixEditor.getMatrix().getDimCount() - 1;
        selectedColumn -= nbColumnDimRow;
        if (selectedColumn < 0) {
            selectedColumn = 0;
        }

        int selectedRow = matrixEditor.getTable().getSelectedRow() - 1;

        return new int[] { selectedRow, selectedColumn };
    }

    /**
     * Get file chooser to csv file (import/export).
     * 
     * @return selecteur de fichier CSV
     */
    protected JFileChooser getFileChooser() {
        if (fileChooser == null) {
            fileChooser = new JFileChooser();
            FileFilter filter = new FileFilter() {
                @Override
                public boolean accept(File pathname) {
                    if (pathname.isDirectory()) {
                        return true;
                    }

                    String extension = FileUtil.extension(pathname);
                    if (extension != null) {
                        if (extension.equals("csv")) {
                            return true;
                        } else {
                            return false;
                        }
                    }

                    return false;
                }

                @Override
                public String getDescription() {
                    return "Texte CSV (*.csv)";
                }
            };
            fileChooser.setFileFilter(filter);
        }
        return fileChooser;
    }

    /**
     * Init emport to clip board action.
     * 
     * @return retourne l'action du bloc note permettant la copie entiere de la
     *         matrice
     */
    protected Action getSendToClipBoardAllCopyAction() {
        if (sendToClipBoardAllCopyAction == null) {
            sendToClipBoardAllCopyAction = new AbstractAction() {
                private static final long serialVersionUID=1L;
                @Override
                public void actionPerformed(ActionEvent e) {
                    sendToClipBoardAllCopyPerformed();
                }
            };
        }
        return sendToClipBoardAllCopyAction;
    }

    /**
     * Init import from clip board action.
     * 
     * @return retourne l'action du bloc note permettant la recopie entere de la
     *         matrice depuis le bloc note
     */
    protected Action getSendToClipBoardAllPasteAction() {
        if (sendToClipBoardAllPasteAction == null) {
            sendToClipBoardAllPasteAction = new AbstractAction() {
                private static final long serialVersionUID = 1L;
                @Override
                public void actionPerformed(ActionEvent e) {
                    sendToClipBoardAllPastePerformed();
                }
            };
        }
        return sendToClipBoardAllPasteAction;
    }

    /**
     * Init emport selection to clip board action.
     * 
     * @return retourne l'action du bloc note permettant la copie de la partie
     *         selectionnee
     */
    protected Action getSendToClipBoardSelectionCopyAction() {
        if (sendToClipBoardSelectionCopyAction == null) {
            sendToClipBoardSelectionCopyAction = new AbstractAction() {
                private static final long serialVersionUID = 1L;
                @Override
                public void actionPerformed(ActionEvent e) {
                    sendToClipBoardSelectionCopyPerformed();
                }
            };
        }
        return sendToClipBoardSelectionCopyAction;
    }

    /**
     * Init import selection from clip board action.
     * 
     * @return retourne l'action du bloc note permettant la recopie de la partie
     *         selectionnee de la matrice depuis le bloc note
     */
    public Action getSendToClipBoardCurrentPasteAction() {
        if (sendToClipBoardCurrentPasteAction == null) {
            sendToClipBoardCurrentPasteAction = new AbstractAction() {
                private static final long serialVersionUID=1L;
                @Override
                public void actionPerformed(ActionEvent e) {
                    sendToClipBoardCurrentPastePerformed();
                }
            };
        }
        return sendToClipBoardCurrentPasteAction;
    }

    /**
     * Export matrix to clip board.
     */
    protected void sendToClipBoardAllCopyPerformed() {
        try {
            Writer writer = getClipBoardWriter();
            getMatrix().exportCSV(writer, withSemantics.getState());
            StringSelection contents = new StringSelection(writer.toString());
            Clipboard clipboard = Toolkit.getDefaultToolkit()
                    .getSystemClipboard();
            clipboard.setContents(contents, contents);
            writer.close();
            matrixEditor.repaint();
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(matrixEditor,
                    t("nuitonmatrix.error.clipboard.write"),
                    t("nuitonmatrix.error"), JOptionPane.ERROR_MESSAGE);
            if (log.isErrorEnabled()) {
                log.error(t("nuitonmatrix.error.clipboard.write"), ex);
            }
        }
    }

    /**
     * Import matrix from clip board.
     */
    protected void sendToClipBoardAllPastePerformed() {
        try {
            Reader reader = getClipBoardReader();
            getMatrix().importCSV(reader, new int[] { 0, 0 });
            reader.close();
            matrixEditor.fireEvent();
            matrixEditor.repaint();
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(matrixEditor,
                    t("nuitonmatrix.error.clipboard.read"),
                    t("nuitonmatrix.error"), JOptionPane.ERROR_MESSAGE);
            if (log.isErrorEnabled()) {
                log.error(t("nuitonmatrix.error.clipboard.read"), ex);
            }
        }
    }

    /**
     * Export selection to clip board.
     */
    protected void sendToClipBoardSelectionCopyPerformed() {
        try {
            Writer writer = getClipBoardWriter();
            getSelectedMatrix().exportCSV(writer, withSemantics.getState());
            StringSelection contents = new StringSelection(writer.toString());
            Clipboard clipboard = Toolkit.getDefaultToolkit()
                    .getSystemClipboard();
            clipboard.setContents(contents, contents);
            writer.close();
            matrixEditor.repaint();
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(matrixEditor,
                    t("nuitonmatrix.error.clipboard.write"),
                    t("nuitonmatrix.error"), JOptionPane.ERROR_MESSAGE);
            if (log.isErrorEnabled()) {
                log.error(t("nuitonmatrix.error.clipboard.write"), ex);
            }
        }
    }

    /**
     * Import from clip board.
     */
    protected void sendToClipBoardCurrentPastePerformed() {
        try {
            Reader reader = getClipBoardReader();
            getMatrix().importCSV(reader,
                    getCoordinatesFirstCellSelectedMatrix());
            reader.close();
            matrixEditor.fireEvent();
            matrixEditor.repaint();
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(matrixEditor,
                    t("nuitonmatrix.error.clipboard.read"),
                    t("nuitonmatrix.error"), JOptionPane.ERROR_MESSAGE);
            if (log.isErrorEnabled()) {
                log.error(t("nuitonmatrix.error.clipboard.read"), ex);
            }
        }
    }

    /**
     * Init export to file action.
     * 
     * @return retourne l'action du fichier permettant la copie entiere de la
     *         matrice
     */
    protected Action getSendToFileAllCopyAction() {
        if (sendToFileAllCopyAction == null) {
            sendToFileAllCopyAction = new AbstractAction() {
                private static final long serialVersionUID=1L;
                @Override
                public void actionPerformed(ActionEvent e) {
                    sendToFileAllCopyPerformed();
                }
            };
        }
        return sendToFileAllCopyAction;
    }

    /**
     * Init import from file action.
     * 
     * @return retourne l'action du fichier permettant la recopie entere de la
     *         matrice depuis le fichier
     */
    protected Action getSendToFileAllPasteAction() {
        if (sendToFileAllPasteAction == null) {
            sendToFileAllPasteAction = new AbstractAction() {
                private static final long serialVersionUID=1L;
                @Override
                public void actionPerformed(ActionEvent e) {
                    sendToFileAllPastePerformed();
                }
            };
        }
        return sendToFileAllPasteAction;
    }

    /**
     * Init export selection to file action.
     * 
     * @return retourne l'action du fichier permettant la copie de la partie
     *         selectionnee
     */
    protected Action getSendToFileSelectionCopyAction() {
        if (sendToFileSelectionCopyAction == null) {
            sendToFileSelectionCopyAction = new AbstractAction() {
                private static final long serialVersionUID=1L;
                @Override
                public void actionPerformed(ActionEvent e) {
                    sendToFileSelectionCopyPerformed();
                }
            };
        }
        return sendToFileSelectionCopyAction;
    }

    /**
     * Init import selection from file action.
     * 
     * @return retourne l'action du fichier permettant la recopie de la partie
     *         selectionnee de la matrice depuis le fichier
     */
    protected Action getSendToFileCurrentPasteAction() {
        if (sendToFileCurrentPasteAction == null) {
            sendToFileCurrentPasteAction = new AbstractAction() {
                private static final long serialVersionUID=1L;
                @Override
                public void actionPerformed(ActionEvent e) {
                    sendToFileCurrentPastePerformed();
                }
            };
        }
        return sendToFileCurrentPasteAction;
    }

    /**
     * Export to file.
     */
    protected void sendToFileAllCopyPerformed() {
        try {
            Writer writer = getFileChooserWriter();
            if (writer != null) {
                getMatrix().exportCSV(writer, withSemantics.getState());
                writer.close();
                matrixEditor.repaint();
            }
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(matrixEditor,
                    t("nuitonmatrix.error.file.write"), t("nuitonmatrix.error"),
                    JOptionPane.ERROR_MESSAGE);
            if (log.isErrorEnabled()) {
                log.error(t("nuitonmatrix.error.file.write"), ex);
            }
        }
    }

    /**
     * Import from file.
     */
    protected void sendToFileAllPastePerformed() {
        try {
            Reader reader = getFileChooserReader();
            if (reader != null) { // cancel
                getMatrix().importCSV(reader, new int[] { 0, 0 });
                reader.close();
                matrixEditor.fireEvent();
                matrixEditor.repaint();
            }
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(matrixEditor,
                    t("nuitonmatrix.error.file.read"), t("nuitonmatrix.error"),
                    JOptionPane.ERROR_MESSAGE);
            if (log.isErrorEnabled()) {
                log.error(t("nuitonmatrix.error.file.read"), ex);
            }
        }
    }

    /**
     * Export selection to file.
     */
    protected void sendToFileSelectionCopyPerformed() {
        try {
            Writer writer = getFileChooserWriter();
            if (writer != null) {
                getSelectedMatrix().exportCSV(writer, withSemantics.getState());
                writer.close();
                matrixEditor.repaint();
            }
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(matrixEditor,
                    t("nuitonmatrix.error.file.write"), t("nuitonmatrix.error"),
                    JOptionPane.ERROR_MESSAGE);
            if (log.isErrorEnabled()) {
                log.error(t("nuitonmatrix.error.file.write"), ex);
            }
        }
    }

    /**
     * Import selection from file.
     */
    protected void sendToFileCurrentPastePerformed() {
        try {
            Reader reader = getFileChooserReader();
            if (reader != null) { // cancel
                getMatrix().importCSV(reader,
                        getCoordinatesFirstCellSelectedMatrix());
                reader.close();
                matrixEditor.fireEvent();
                matrixEditor.repaint();
            }
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(matrixEditor,
                    t("nuitonmatrix.error.file.read"), t("nuitonmatrix.error"),
                    JOptionPane.ERROR_MESSAGE);
            if (log.isErrorEnabled()) {
                log.error(t("nuitonmatrix.error.file.read"), ex);
            }
        }
    }

    /**
     * Fill editor matrix with identity matrix.
     */
    protected void dataIdentityPerformed() {
        MatrixHelper.convertToId(getMatrix());
        matrixEditor.fireEvent();
        matrixEditor.repaint();
    }

    /**
     * Ask user for new value to fill into matrix.
     */
    protected void dataFillValuePerformed() {
        String stringValue = JOptionPane.showInputDialog(matrixEditor, t("nuitonmatrix.menu.data.fillvalue.ask"));
        if (stringValue != null) {
            try {
                double value = Double.parseDouble(stringValue);
                MatrixHelper.fill(getMatrix(), value);
                matrixEditor.fireEvent();
                matrixEditor.repaint();
            }
            catch (NumberFormatException ex) {
                // skip exception
            }
        }
    }
}
