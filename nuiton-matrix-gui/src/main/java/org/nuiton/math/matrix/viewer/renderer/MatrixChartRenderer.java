/*
 * #%L
 * Nuiton Matrix :: GUI
 * %%
 * Copyright (C) 2010 - 2020 Codelutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.math.matrix.viewer.renderer;

import static org.nuiton.i18n.I18n.t;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.Icon;
import javax.swing.JComboBox;
import javax.swing.JPanel;

import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.CategoryLabelPositions;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.renderer.category.CategoryItemRenderer;
import org.jfree.data.category.CategoryDataset;
import org.nuiton.math.matrix.MatrixND;
import org.nuiton.math.matrix.viewer.MatrixRenderer;
import org.nuiton.math.matrix.viewer.renderer.jfreechart.GraphComboRenderer;
import org.nuiton.math.matrix.viewer.renderer.jfreechart.GraphMatrixNDDataset;
import org.nuiton.math.matrix.viewer.renderer.jfreechart.GraphRendererComboBoxModel;
import org.nuiton.util.Resource;

/**
 * Matrix chart renderer (based on jfreechart).
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class MatrixChartRenderer implements MatrixRenderer {

    protected JComboBox chartRendererComboBox;
    protected JFreeChart chart;

    @Override
    public Component getComponent(MatrixND matrix) {
        JPanel panel = getJFreeChartPanel(matrix);
        return panel;
    }

    @Override
    public Icon getIcon() {
        return Resource.getIcon("/icons/fatcow/chart_curve.png");
    }

    @Override
    public String getName() {
        return t("nuitonmatrix.viewer.renderer.chart");
    }

    protected JPanel getJFreeChartPanel(MatrixND matrix) {
        JPanel panel = new JPanel(new BorderLayout());
        chartRendererComboBox = getChartRendererComboBox();
        panel.add(chartRendererComboBox, BorderLayout.NORTH);
        
        chart = getJFreeChart(matrix);
        panel.add(getChartPanel(chart), BorderLayout.CENTER);
        return panel;
    }

    /**
     * Chart renderers combo box.
     * 
     * @return chart renderers combo box
     * @see CategoryItemRenderer
     */
    protected JComboBox getChartRendererComboBox() {
        JComboBox rendererComboBox = new JComboBox();
        GraphRendererComboBoxModel rendererComboBoxModel = new GraphRendererComboBoxModel();
        rendererComboBox.setModel(rendererComboBoxModel);
        rendererComboBox.setRenderer(new GraphComboRenderer());

        // add listener combobox > chart
        rendererComboBox.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                CategoryItemRenderer renderer = (CategoryItemRenderer)e.getItem();
                chart.getCategoryPlot().setRenderer(renderer);
            }
        });

        return rendererComboBox;
    }

    /**
     * JFreechart data model.
     * 
     * @param matrix
     * @return jfreechart data model
     * @see CategoryDataset
     */
    protected CategoryDataset getCategoryDataset(MatrixND matrix) {
        CategoryDataset categoryDataset = new GraphMatrixNDDataset(matrix);
        return categoryDataset;
    }

    /**
     * Abscisse (nom + valeur à la verticale).
     * 
     * @param matrix
     * @return category axis
     */
    protected CategoryAxis getCategoryAxis(MatrixND matrix) {
        CategoryAxis horizontalAxis = new CategoryAxis();
        horizontalAxis.setCategoryLabelPositions(CategoryLabelPositions.UP_90);
        horizontalAxis.setLabel(t(matrix.getDimensionName(0)));
        return horizontalAxis;
    }

    /**
     * Ordonnées (valeur seule).
     * 
     * @param matrix
     * @return value axis
     */
    protected ValueAxis getValueAxis(MatrixND matrix) {
        ValueAxis verticalAxis = new NumberAxis();
        return verticalAxis;
    }

    /**
     * Category plot.
     * 
     * @param matrix
     * @return category plot
     */
    protected CategoryPlot getCategoryPlot(MatrixND matrix) {
        CategoryPlot categoryPlot = new CategoryPlot(getCategoryDataset(matrix),
                getCategoryAxis(matrix), getValueAxis(matrix),
                (CategoryItemRenderer)chartRendererComboBox.getSelectedItem());
        return categoryPlot;
    }

    /**
     * Build chart.
     * 
     * @return chart
     */
    protected JFreeChart getJFreeChart(MatrixND matrix) {
        JFreeChart chart = new JFreeChart(t(matrix.getName()), JFreeChart.DEFAULT_TITLE_FONT, getCategoryPlot(matrix), true);
        return chart;
    }

    /**
     * Build jfreechart panel.
     * 
     * @param chart chart to render
     * @return chart panel
     */
    protected ChartPanel getChartPanel(JFreeChart chart) {
        ChartPanel chartPanel = new ChartPanel(chart);
        return chartPanel;
    }
}
