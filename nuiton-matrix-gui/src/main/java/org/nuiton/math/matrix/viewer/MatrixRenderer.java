/*
 * #%L
 * Nuiton Matrix :: GUI
 * %%
 * Copyright (C) 2010 - 2011 Codelutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.math.matrix.viewer;

import java.awt.Component;

import javax.swing.Icon;

import org.nuiton.math.matrix.MatrixND;

/**
 * Matrix renderer plugin interface.
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public interface MatrixRenderer {

    /**
     * Renderer component for matrix.
     * 
     * @param matrix matrix to display (can be null with default rendering support)
     * @return component
     * @see MatrixViewerPanel#addMatrixRenderer(MatrixRenderer, boolean)
     */
    Component getComponent(MatrixND matrix);

    /**
     * Renderer icon (used in {@link MatrixRendererSolution#ICON} rendering).
     * 
     * @return plugin icon
     */
    Icon getIcon();

    /**
     * Renderer name (used in {@link MatrixRendererSolution#RADIO_BUTTON} rendering).
     * 
     * @return plugin name
     */
    String getName();
}
