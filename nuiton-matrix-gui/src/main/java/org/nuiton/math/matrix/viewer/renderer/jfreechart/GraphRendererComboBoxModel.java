/*
 * #%L
 * Nuiton Matrix :: GUI
 * %%
 * Copyright (C) 2010 - 2020 Codelutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.math.matrix.viewer.renderer.jfreechart;

import java.util.ArrayList;
import java.util.List;

import javax.swing.DefaultComboBoxModel;

import org.jfree.chart.labels.StandardCategoryToolTipGenerator;
import org.jfree.chart.renderer.category.AreaRenderer;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.chart.renderer.category.CategoryItemRenderer;
import org.jfree.chart.renderer.category.LineAndShapeRenderer;
import org.jfree.chart.renderer.category.MinMaxCategoryRenderer;
import org.jfree.chart.renderer.category.StackedAreaRenderer;
import org.jfree.chart.renderer.category.StackedBarRenderer;

/**
 * JfreeChart renderer combo box model.
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class GraphRendererComboBoxModel extends DefaultComboBoxModel {

    /** serialVersionUID. */
    private static final long serialVersionUID = 5084118137199817098L;

    protected List<CategoryItemRenderer> rendererList;

    public GraphRendererComboBoxModel() {
        rendererList = new ArrayList<CategoryItemRenderer>();
        loadRenderers();
    }

    protected void loadRenderers() {

        // rendu : vertical bar
        BarRenderer barRenderer = new BarRenderer();
        barRenderer.setDefaultToolTipGenerator(new StandardCategoryToolTipGenerator());
        rendererList.add(barRenderer);
    
        // rendu stacked bar
        StackedBarRenderer stackedBarRenderer = new StackedBarRenderer();
        stackedBarRenderer.setDefaultToolTipGenerator(new StandardCategoryToolTipGenerator());
        rendererList.add(stackedBarRenderer);
        
        // rendu aire
        AreaRenderer areaRenderer = new AreaRenderer();
        areaRenderer.setDefaultToolTipGenerator(new StandardCategoryToolTipGenerator());
        rendererList.add(areaRenderer);
        
        // rendu aire empilée
        StackedAreaRenderer stackedAreaRenderer = new StackedAreaRenderer();
        stackedAreaRenderer.setDefaultToolTipGenerator(new StandardCategoryToolTipGenerator());
        rendererList.add(stackedAreaRenderer);
        
        // rendu line aire
        LineAndShapeRenderer lineAndShapeRenderer = new LineAndShapeRenderer();
        lineAndShapeRenderer.setDefaultToolTipGenerator(new StandardCategoryToolTipGenerator());
        rendererList.add(lineAndShapeRenderer);
        
        // rendu mix/max
        MinMaxCategoryRenderer minMaxCategoryRenderer = new MinMaxCategoryRenderer();
        minMaxCategoryRenderer.setDefaultToolTipGenerator(new StandardCategoryToolTipGenerator());
        rendererList.add(minMaxCategoryRenderer);
        
        // auto select first
        setSelectedItem(barRenderer);
    }

    @Override
    public int getSize() {
        return rendererList.size();
    }

    @Override
    public Object getElementAt(int index) {
        return rendererList.get(index);
    }
}
