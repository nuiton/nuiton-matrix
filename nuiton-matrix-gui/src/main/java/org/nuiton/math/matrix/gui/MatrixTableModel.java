/*
 * #%L
 * Nuiton Matrix :: GUI
 * %%
 * Copyright (C) 2004 - 2011 CodeLutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.math.matrix.gui;

import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableModel;

import org.nuiton.math.matrix.MatrixND;

/**
 * {@link TableModel} that can display matrix in a {@link JTable}.
 *
 * Created: 22 mars 2006 12:53:22
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public interface MatrixTableModel extends TableModel {

    void setMatrix(MatrixND m);
    
    MatrixND getMatrix();

    TableCellRenderer getMatrixCellRenderer();

    /**
     * Get how many additional rows table model need to renderer matrix.
     * 
     * @return additional rows
     */
    int getAdditionalRows();
    
    /**
     * Get how many additional columns table model need to renderer matrix.
     * 
     * @return additional columns
     */
    int getAdditionalColumns();
}
