/*
 * #%L
 * Nuiton Matrix :: GUI
 * %%
 * Copyright (C) 2011 CodeLutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.math.matrix.gui;

import static org.nuiton.i18n.I18n.t;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableCellRenderer;

import org.nuiton.math.matrix.MatrixND;

/**
 * Model that take a delegate model to add additional lines during
 * rendering (such as row sum, column sum, row mean, column mean...)
 * 
 * Created: 21 mars 2006 19:01:27
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class MatrixTableModelOption implements MatrixTableModel {

    protected MatrixTableModel delegate;

    protected boolean sumOption;

    protected boolean meanOption;

    protected boolean transposeOption;

    public MatrixTableModelOption(MatrixTableModel delegate) {
        this(delegate, false, false, false);
    }

    public MatrixTableModelOption(MatrixTableModel delegate, boolean sumOption,
            boolean meanOption, boolean transposeOption) {
        this.delegate = delegate;
        this.sumOption = sumOption;
        this.meanOption = meanOption;
        this.transposeOption = transposeOption;
    }
    
    public boolean isSumOption() {
        return sumOption;
    }

    public void setSumOption(boolean sumOption) {
        this.sumOption = sumOption;
    }

    public boolean isMeanOption() {
        return meanOption;
    }

    public void setMeanOption(boolean meanOption) {
        this.meanOption = meanOption;
    }

    public boolean isTransposeOption() {
        return transposeOption;
    }

    public void setTransposeOption(boolean transposeOption) {
        this.transposeOption = transposeOption;
    }

    /*
     * @see javax.swing.table.TableModel#getRowCount()
     */
    @Override
    public int getRowCount() {
        int result = -1;
        if (transposeOption) {
            result = delegate.getColumnCount();
        }
        else {
            result = delegate.getRowCount();
        }
        if (sumOption) {
            result++;
        }
        if (meanOption) {
            result++;
        }
        return result;
    }

    /*
     * @see javax.swing.table.TableModel#getColumnCount()
     */
    @Override
    public int getColumnCount() {
        int result = -1;
        if (transposeOption) {
            result = delegate.getRowCount();
        }
        else {
            result = delegate.getColumnCount();
        }
        if (sumOption) {
            result++;
        }
        if (meanOption) {
            result++;
        }
        return result;
    }

    /*
     * @see javax.swing.table.TableModel#getColumnName(int)
     */
    @Override
    public String getColumnName(int columnIndex) {
        String result = null;
        if (sumOption && columnIndex == delegate.getColumnCount() + 0) {
            result = null; //t("nuitonmatrix.gui.model.sum");
        }
        else if (meanOption && columnIndex == delegate.getColumnCount() + 1) {
            result = null; //t("nuitonmatrix.gui.model.mean");
        }
        else {
            result = delegate.getColumnName(columnIndex);
        }
        return result;
    }

    /*
     * @see javax.swing.table.TableModel#getColumnClass(int)
     */
    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return delegate.getColumnClass(columnIndex);
    }

    /*
     * @see javax.swing.table.TableModel#isCellEditable(int, int)
     */
    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return delegate.isCellEditable(rowIndex, columnIndex);
    }

    /*
     * @see javax.swing.table.TableModel#getValueAt(int, int)
     */
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        int rowCount = delegate.getRowCount();
        int columnCount = delegate.getColumnCount();

        Object result = null;
        if (transposeOption) {
            result = getValueAtTranposable(columnIndex, rowIndex, rowCount, columnCount);
        }
        else {
            result = getValueAtTranposable(rowIndex, columnIndex, rowCount, columnCount);
        }
        return result;
    }

    /**
     * Get value qui fonctionne aussi en transposée.
     * 
     * Le principe est que des que les bornes sur modele delegé sont dépassées
     * on réalise des opération (mean/sum).
     * 
     * Actuellement le code n'est vraiment pas evident.
     * 
     * @param rowIndex rowIndex
     * @param columnIndex columnIndex
     * @param rowCount rowCount
     * @param columnCount columnCount
     * @return object at rowIndex/columnCount
     */
    protected Object getValueAtTranposable(int rowIndex, int columnIndex, int rowCount, int columnCount) {
        Object result = null;
        // operation sur la derniere ligne
        if (rowIndex >= rowCount) { 
            // une seule operation
            if (columnIndex == 0) {
                if (rowIndex == rowCount + 1) {
                    result = t("nuitonmatrix.gui.model.mean");
                }
                else {
                    if (sumOption) {
                        result = t("nuitonmatrix.gui.model.sum");
                    }
                    else {
                        result = t("nuitonmatrix.gui.model.mean");
                    }
                }
            }
            // deux operation (forcement mean) (cas somme des sommes)
            else if (columnIndex == columnCount + 1) {
                if  (rowIndex == rowCount + 1) { // pas de sens
                    result = delegate.getMatrix().meanAll();
                }
            }
            // une seule operation (cas somme des sommes)
            else if (columnIndex == columnCount) {
                if (rowIndex == rowCount) { // pas de sens
                    if (sumOption) {
                        result = delegate.getMatrix().sumAll();
                    }
                    else {
                        result = delegate.getMatrix().sumAll();
                    }
                }
            }
            // deux operation (forcement mean)
            else if (rowIndex == rowCount + 1) {
                result = getComputedValueForColumn(columnIndex, true); // mean
            }
            // une seule operation
            else {
                if (sumOption) {
                    result = getComputedValueForColumn(columnIndex, false); // sum
                }
                else {
                    // sum
                    result = getComputedValueForColumn(columnIndex, true); // mean
                }
            }
        }

        // operation sur la derniere colonne
        else if (columnIndex >= columnCount) {
            // une seule operation
            if (rowIndex == 0) {
                if (columnIndex == columnCount + 1) {
                    result = t("nuitonmatrix.gui.model.mean");
                }
                else {
                    if (sumOption) {
                        result = t("nuitonmatrix.gui.model.sum");
                    }
                    else {
                        result = t("nuitonmatrix.gui.model.mean");
                    }
                }
            }
            // cas sommes de somme deja gérée
            // par le premier if mais il faut quand meme les conditions
            // pour qu'il ne passe pas dans le else final
            else if (rowIndex == rowCount + 1) {}
            else if (rowIndex == rowCount) {}
            // deux operation (forcement mean)
            else if (columnIndex == columnCount + 1) {
                result = getComputedValueForRow(rowIndex, true); // mean
            }
            // une seule operation
            else {
                if (sumOption) {
                    result = getComputedValueForRow(rowIndex, false); // sum
                }
                else {
                    // sum
                    result = getComputedValueForRow(rowIndex, true); // mean
                }
            }
        }

        else {
            // reste du tableau
            result = delegate.getValueAt(rowIndex, columnIndex);
        }
        return result;
    }

    /**
     * Compute sum for delegate model row index.
     * 
     * @param delegateRowIndex delegate model row index
     * @return sum for row
     */
    protected Double getComputedValueForRow(int delegateRowIndex, boolean mean) {
        double sum = 0.0;
        double count = 0;
        for (int col = delegate.getAdditionalColumns() ; col < delegate.getColumnCount() ; col++) {
            sum += (Double)delegate.getValueAt(delegateRowIndex, col);
            count++;
        }
        double result = sum;
        if (mean) {
            result = sum / count;
        }
        return result;
    }
    
    /**
     * Compute sum for delegate model column index.
     * 
     * @param delegateColumnIndex delegate model column index
     * @return sum for column
     */
    protected Double getComputedValueForColumn(int delegateColumnIndex, boolean mean) {
        double sum = 0.0;
        double count = 0;
        for (int row = delegate.getAdditionalRows() ; row < delegate.getRowCount() ; row++) {
            sum += (Double)delegate.getValueAt(row, delegateColumnIndex);
            count++;
        }
        double result = sum;
        if (mean) {
            result = sum / count;
        }
        return result;
    }

    /*
     * @see javax.swing.table.TableModel#setValueAt(java.lang.Object, int, int)
     */
    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        if (transposeOption) {
            delegate.setValueAt(aValue, rowIndex, columnIndex);
        } else {
            delegate.setValueAt(aValue, columnIndex, rowIndex);
        }
    }

    /*
     * @see javax.swing.table.TableModel#addTableModelListener(javax.swing.event.TableModelListener)
     */
    @Override
    public void addTableModelListener(TableModelListener l) {
        delegate.addTableModelListener(l);
    }

    /*
     * @see javax.swing.table.TableModel#removeTableModelListener(javax.swing.event.TableModelListener)
     */
    @Override
    public void removeTableModelListener(TableModelListener l) {
        delegate.removeTableModelListener(l);
    }

    /*
     * @see org.nuiton.math.matrix.gui.MatrixTableModel#setMatrix(org.nuiton.math.matrix.MatrixND)
     */
    @Override
    public void setMatrix(MatrixND m) {
        delegate.setMatrix(m);
    }

    @Override
    public MatrixND getMatrix() {
        return delegate.getMatrix();
    }

    /*
     * @see org.nuiton.math.matrix.gui.MatrixTableModel#getMatrixCellRenderer()
     */
    @Override
    public TableCellRenderer getMatrixCellRenderer() {
        return delegate.getMatrixCellRenderer();
    }

    /*
     * @see org.nuiton.math.matrix.gui.MatrixTableModel#getAdditionalRows()
     */
    @Override
    public int getAdditionalRows() {
        int result = 0;
        if (sumOption) {
            result++;
        }
        if (meanOption) {
            result++;
        }
        return result;
    }

    /*
     * @see org.nuiton.math.matrix.gui.MatrixTableModel#getAdditionalColumns()
     */
    @Override
    public int getAdditionalColumns() {
        int result = 0;
        if (sumOption) {
            result++;
        }
        if (meanOption) {
            result++;
        }
        return result;
    }
}
