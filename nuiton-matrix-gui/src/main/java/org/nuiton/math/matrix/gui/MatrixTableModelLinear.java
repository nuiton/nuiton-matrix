/*
 * #%L
 * Nuiton Matrix :: GUI
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.math.matrix.gui;

import static org.nuiton.i18n.I18n.t;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.math.matrix.MatrixIterator;
import org.nuiton.math.matrix.MatrixND;

/**
 * MatrixTableModelLinear.
 *
 * Created: 22 mars 2006 12:11:46
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class MatrixTableModelLinear extends AbstractTableModel implements
        MatrixTableModel {

    /** serialVersionUID. */
    private static final long serialVersionUID = -7498520067143762434L;

    /** Logger for this class. */
    private static Log log = LogFactory.getLog(MatrixTableModelLinear.class);

    protected MatrixND m = null;
    protected boolean showDefault = false;
    protected double defaultValue = 0;
    protected List<Object[]> mappingRowSems = new ArrayList<Object[]>();
    protected TableCellRenderer renderer = null;

    public MatrixTableModelLinear(MatrixND m, boolean showDefault) {
        this.showDefault = showDefault;
        setMatrix(m);
    }

    /*
     * @see org.nuiton.math.matrix.gui.MatrixTableModel#getMatrix()
     */
    @Override
    public MatrixND getMatrix() {
        return m;
    }

    @Override
    public void setMatrix(MatrixND m) {
        this.m = m;
        computeMapping();
        fireTableStructureChanged();
    }

    protected void computeMapping() {
        mappingRowSems.clear();
        for (MatrixIterator i = m.iterator(); i.next();) {
            Object[] sems = i.getSemanticsCoordinates();
            double value = i.getValue();
            if (showDefault || value != defaultValue) {
                // make copy since change in getSemanticsCoordinates()
                Object[] semsCopy = Arrays.copyOf(sems, sems.length);
                mappingRowSems.add(semsCopy);
            }
        }
    }

    /**
     * @return Returns the showDefault.
     */
    public boolean isShowDefault() {
        return this.showDefault;
    }

    /**
     * @param showDefault The showDefault to set.
     */
    public void setShowDefault(boolean showDefault) {
        this.showDefault = showDefault;
        computeMapping();
        fireTableDataChanged();
    }

    /**
     * @return Returns the defaultValue.
     */
    public double getDefaultValue() {
        return this.defaultValue;
    }

    /**
     * @param defaultValue The defaultValue to set.
     */
    public void setDefaultValue(double defaultValue) {
        this.defaultValue = defaultValue;
        computeMapping();
        fireTableDataChanged();
    }

    /*
     * @see javax.swing.table.TableModel#getRowCount()
     */
    @Override
    public int getRowCount() {
        return mappingRowSems.size();
    }

    /*
     * @see javax.swing.table.TableModel#getColumnCount()
     */
    @Override
    public int getColumnCount() {
        return m.getDimCount() + 1;
    }

    /*
     * @see javax.swing.table.TableModel#getValueAt(int, int)
     */
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Object result = null;
        if (columnIndex < m.getDimCount()) {
            result = mappingRowSems.get(rowIndex)[columnIndex];
        } else {
            result = m.getValue(mappingRowSems.get(rowIndex));
        }
        return result;
    }

    /*
     * @see javax.swing.table.AbstractTableModel#isCellEditable(int, int)
     */
    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return columnIndex == m.getDimCount();
    }

    /*
     * @see javax.swing.table.AbstractTableModel#setValueAt(java.lang.Object, int, int)
     */
    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        try {
            double val = Double.parseDouble((String) aValue);
            Object[] sems = mappingRowSems.get(rowIndex);
            m.setValue(sems, val);
            fireTableDataChanged();
        } catch (Exception eee) {
            log.debug("La nouvelle valeur n'est pas convertible en double: "
                    + aValue, eee);
        }

    }

    /**
     * {@inheritDoc}
     * 
     * Return column names.
     * Try to return i18n column names if possible.
     */
    @Override
    public String getColumnName(int column) {
        String result = null;
        if (column < m.getDimCount()) {
            String columnName = m.getDimensionName(column);
            result = t(columnName);
        } else {
            result = "";
        }
        return result;
    }

    /**
     * Par defaut, la classe de la colonne est du type Function.class
     * @param column
     */
    @Override
    public Class<?> getColumnClass(int column) {
        return String.class;
    }

    @Override
    public TableCellRenderer getMatrixCellRenderer() {
        if (renderer == null) {
            renderer = new MatrixCellRenderer(this);
        }
        return renderer;
    }

    public static class MatrixCellRenderer extends DefaultTableCellRenderer {

        /** serialVersionUID. */
        private static final long serialVersionUID = 6537813058357761914L;

        protected MatrixTableModelLinear model = null;
        protected Color bg = null;
        protected Color fg = null;
        protected Font font = null;
        protected Border border = null;

        public MatrixCellRenderer(MatrixTableModelLinear model) {
            this.model = model;
            bg = getBackground();
            fg = getForeground();
            font = getFont();
            border = getBorder();
        }

        @Override
        public Component getTableCellRendererComponent(JTable table,
                Object value, boolean isSelected, boolean hasFocus, int row,
                int column) {
            super.getTableCellRendererComponent(table, value, isSelected,
                    hasFocus, row, column);
            setToolTipText(getText());

            if (column < model.m.getDimCount()) {
                if (table != null) {
                    JTableHeader header = table.getTableHeader();
                    if (header != null) {
                        setForeground(header.getForeground());
                        setBackground(header.getBackground());
                        setFont(header.getFont());
                    }
                }

                setBorder(UIManager.getBorder("TableHeader.cellBorder"));
            } else {
                setBackground(bg);
                setForeground(fg);
                setFont(font);
                setBorder(border);
            }

            return this;
        }
    }

    /*
     * @see org.nuiton.math.matrix.gui.MatrixTableModel#getAdditionalRows()
     */
    @Override
    public int getAdditionalRows() {
        return 0;
    }

    /*
     * @see org.nuiton.math.matrix.gui.MatrixTableModel#getAdditionalColumns()
     */
    @Override
    public int getAdditionalColumns() {
        return m.getDimCount();
    }
}
