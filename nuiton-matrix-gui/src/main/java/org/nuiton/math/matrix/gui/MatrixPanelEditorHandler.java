/*
 * #%L
 * Nuiton Matrix :: GUI
 * %%
 * Copyright (C) 2011 Codelutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.math.matrix.gui;

import static org.nuiton.i18n.I18n.t;

import java.awt.Event;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.util.Collection;
import java.util.HashSet;

import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;

import org.nuiton.math.matrix.MatrixFactory;
import org.nuiton.math.matrix.MatrixND;

/**
 * Handler for matrix panel editor.
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class MatrixPanelEditorHandler {

    protected Collection<MatrixPanelListener> matrixPanelListeners = new HashSet<MatrixPanelListener>();

    protected MatrixPopupMenu popupMenu = null;

    public void addMatrixPanelListener(MatrixPanelListener l) {
        matrixPanelListeners.add(l);
    }

    public void removeMatrixPanelListener(MatrixPanelListener l) {
        matrixPanelListeners.remove(l);
    }

    protected void fireEvent(MatrixPanelEditor matrixPanelEditor) {
        MatrixPanelEvent event = new MatrixPanelEvent(matrixPanelEditor);
        for (MatrixPanelListener matrixPanelListener : matrixPanelListeners) {
            matrixPanelListener.matrixChanged(event);
        }
    }

    /**
     * Init panel with current panel matrix.
     * 
     * @param matrixPanelEditor panel to init
     */
    protected void initEditor(final MatrixPanelEditor matrixPanelEditor) {
        
        MatrixND matrix = matrixPanelEditor.getMatrix();

        JTable matrixTable = null;
        if (matrix != null) {
            popupMenu = new MatrixPopupMenu(matrixPanelEditor);
            matrixTable = new JTable() {
                public void processMouseEvent(MouseEvent event) {
                    if (event.isPopupTrigger()) {
                        popupMenu.show(event.getComponent(), event.getX(), event.getY());
                    }
                    super.processMouseEvent(event);
                }
            };

            matrixTable.getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_C, Event.CTRL_MASK), "copy");
            matrixTable.getActionMap().put("copy", popupMenu.getSendToClipBoardSelectionCopyAction());
            matrixTable.getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_V, Event.CTRL_MASK), "paste");
            matrixTable.getActionMap().put("paste", popupMenu.getSendToClipBoardCurrentPasteAction());
            
            MatrixTableModel matrixTableModel = null;
            if (matrixPanelEditor.isLinearModel()) {
                matrixTableModel = new MatrixTableModelLinear(matrix, matrixPanelEditor.isLinearModelShowDefault());
            }
            else {
                matrixTableModel = new MatrixTableModelND(matrix);
            }

            // unique gestion des options par modele interposé
            if (matrixPanelEditor.isDisplayOptions()) {
                matrixTableModel = new MatrixTableModelOption(matrixTableModel,
                        matrixPanelEditor.getSumOptionCheckBox().isSelected(),
                        matrixPanelEditor.getMeanOptionCheckBox().isSelected(),
                        matrixPanelEditor.getTransposeOptionCheckBox().isSelected());
                
            }

            matrixTableModel.addTableModelListener(new TableModelListener() {
                @Override
                public void tableChanged(TableModelEvent e) {
                    matrixPanelEditor.fireEvent();
                }
            });

            matrixTable.setModel(matrixTableModel);
            matrixTable.setDefaultRenderer(String.class, matrixTableModel.getMatrixCellRenderer());
            matrixTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
            matrixTable.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
            matrixTable.setCellSelectionEnabled(true);
            matrixPanelEditor.table = matrixTable;
        }
        matrixPanelEditor.getEditArea().setViewportView(matrixTable);
        matrixPanelEditor.repaint();
    }

    /**
     * Modify matrix dimensions
     * 
     * @param matrixPanelEditor matrix panel editor
     */
    public void modifyMatrixDimension(MatrixPanelEditor matrixPanelEditor) {
        String dim = JOptionPane.showInputDialog(matrixPanelEditor,
            t("nuitonmatrix.create.matrix.message"),
            t("nuitonmatrix.create.matrix.title"));

        if (dim != null && dim.indexOf(';') != -1) {
            String[] sdim = dim.split(";");
            int[] idim = new int[sdim.length];
            for (int i = 0; i < idim.length; i++) {
                idim[i] = Integer.parseInt(sdim[i]);
            }
            MatrixND newMatrix = MatrixFactory.getInstance().create(idim);
            matrixPanelEditor.setMatrix(newMatrix);
        }
    }
}
