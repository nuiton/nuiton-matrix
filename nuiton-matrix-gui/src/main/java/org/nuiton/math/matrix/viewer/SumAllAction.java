/*
 * #%L
 * Nuiton Matrix :: GUI
 * %%
 * Copyright (C) 2011 - 2020 Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.math.matrix.viewer;

import javax.swing.Icon;

import org.nuiton.math.matrix.MatrixND;
import org.nuiton.util.Resource;

import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Sum all raw action.
 * 
 * Default matrix viewer panel action to sum all elements.
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class SumAllAction implements MatrixDimensionAction {

    @Override
    public Icon getIcon() {
        return Resource.getIcon("/icons/sum-disabled.png");
    }

    @Override
    public Icon getSelectedIcon() {
        return Resource.getIcon("/icons/fatcow/sum.png");
    }

    @Override
    public String getDescription() {
        return t("nuitonmatrix.viewer.sum.description");
    }

    @Override
    public boolean canApply(int index, List<?> semantic) {
        return true;
    }

    @Override
    public MatrixND apply(MatrixND matrix, int dim) {
        matrix = matrix.sumOverDim(dim, -1);
        matrix.setDimensionName(dim, t("nuitonmatrix.viewer.sum"));
        return matrix;
    }
}
