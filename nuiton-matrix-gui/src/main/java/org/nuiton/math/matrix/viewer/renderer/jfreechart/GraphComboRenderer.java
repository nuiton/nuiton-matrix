/*
 * #%L
 * Nuiton Matrix :: GUI
 * %%
 * Copyright (C) 2002 - 2020 Ifremer, CodeLutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.math.matrix.viewer.renderer.jfreechart;

import static org.nuiton.i18n.I18n.t;

import java.awt.Component;

import javax.swing.DefaultListCellRenderer;
import javax.swing.JList;

import org.jfree.chart.renderer.category.AreaRenderer;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.chart.renderer.category.LineAndShapeRenderer;
import org.jfree.chart.renderer.category.MinMaxCategoryRenderer;
import org.jfree.chart.renderer.category.StackedAreaRenderer;
import org.jfree.chart.renderer.category.StackedBarRenderer;

/**
 * GraphComboRenderer.
 *
 * Created: Thu Sep 12 19:55:10 2002
 *
 * @author <a href="mailto:seb.regnier@free.fr"></a>
 * @version $Revision$
 */
public class GraphComboRenderer extends DefaultListCellRenderer {

    /** serialVersionUID */
    private static final long serialVersionUID = 5439698068065934760L;

    public Component getListCellRendererComponent(JList liste, Object o,
            int attributeIndex, boolean isSelected, boolean hasFocus) {
        super.getListCellRendererComponent(liste, o, attributeIndex,
                isSelected, hasFocus);
        if (o instanceof StackedBarRenderer) {
            setText(t("nuitonmatrix.viewer.graphcomborender.bar.stacked"));
        } else if (o instanceof BarRenderer) {
            setText(t("nuitonmatrix.viewer.graphcomborender.bar"));
        } else if (o instanceof StackedAreaRenderer) {
            setText(t("nuitonmatrix.viewer.graphcomborender.surface.stacked"));
        } else if (o instanceof AreaRenderer) {
            setText(t("nuitonmatrix.viewer.graphcomborender.surface"));
        } else if (o instanceof LineAndShapeRenderer) {
            setText(t("nuitonmatrix.viewer.graphcomborender.line"));
        } else if (o instanceof MinMaxCategoryRenderer) {
            setText(t("nuitonmatrix.viewer.graphcomborender.min.max"));
        } else {
            setText((o == null) ? "" : o.toString());
        }
        return this;
    }

}// GraphComboRenderer
