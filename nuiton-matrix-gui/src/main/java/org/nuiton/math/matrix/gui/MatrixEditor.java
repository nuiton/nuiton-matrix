/*
 * #%L
 * Nuiton Matrix :: GUI
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.math.matrix.gui;

import javax.swing.JButton;
import javax.swing.JPanel;
import org.nuiton.math.matrix.MatrixND;
import javax.swing.JTable;

/**
 *
 * @author letellier
 */
public abstract class MatrixEditor extends JPanel {

    /** serialVersionUID. */
    private static final long serialVersionUID = 991329794990004265L;

    protected boolean enabled = false;
    protected boolean visible = false;

    // Return JTable component
    public abstract JTable getTable();

    // Return edit button component
    public abstract JButton getButtonEdit();

    // Fire the matrix
    protected abstract void fireEvent();

    // Return matrixND
    public abstract MatrixND getMatrix();

    // Initialize matrixND
    public abstract void setMatrix(MatrixND matrix);

    /**
     * @return Returns the linearModel.
     */
    public abstract Boolean isLinearModel();

    /**
     * @param b The linearModel to set.
     */
    public abstract void setLinearModel(Boolean b);

    /**
     * @return Returns the linearModelShowDefault.
     */
    public abstract Boolean isLinearModelShowDefault();

    /**
     * @param b The linearModelShowDefault to set.
     */
    public abstract void setLinearModelShowDefault(Boolean b);

    /**
     * Enabled component
     * @param enabled
     */
    public void setEnabled(boolean enabled){
        this.enabled = enabled;
        getTable().setEnabled(enabled);
        getButtonEdit().setEnabled(enabled);
        super.setEnabled(enabled);
    }

    /**
     * Enabled component
     * @param visible 
     */
    public void setVisible(boolean visible){
        this.visible = visible;
        getTable().setVisible(visible);
        super.setVisible(visible);
    }

}
