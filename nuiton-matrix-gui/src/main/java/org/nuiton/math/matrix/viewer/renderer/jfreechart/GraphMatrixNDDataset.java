/*
 * #%L
 * Nuiton Matrix :: GUI
 * %%
 * Copyright (C) 2002 - 2020 Ifremer, CodeLutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.math.matrix.viewer.renderer.jfreechart;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.ResourceBundle;

import org.jfree.data.category.CategoryDataset;
import org.jfree.data.general.AbstractSeriesDataset;
import org.jfree.data.resources.DataPackageResources;
import org.nuiton.math.matrix.MatrixND;

/**
 * GraphMatrixNDDataset.
 *
 * Created: Fri May 17 16:26:19 2002
 *
 * @author <a href="mailto:seb.regnier@free.fr"></a>
 * @version $Revision$
 */
public class GraphMatrixNDDataset extends AbstractSeriesDataset implements CategoryDataset {

    /** serialVersionUID. */
    private static final long serialVersionUID = -891382923646394164L;

    /** The series names. */
    protected List<String> seriesNames;

    /** The categories. */
    protected List<String> categoriesNames;

    /** Storage for the data. */
    protected MatrixND mat;

    public GraphMatrixNDDataset(MatrixND mat) {
        this(mat.getSemantic(1), mat.getSemantic(0), mat);
    }

    /**
     * Constructs a GraphMatrixNDDataset, populates it with the matrix, and uses the
     * supplied names for the series and the supplied objects for the categories.
     *
     * @param series series names.
     * @param categories categories.
     * @param mat matrix.
     */
    public GraphMatrixNDDataset(List<?> series, List<?> categories, MatrixND mat) {

        this.mat = mat;

        if (mat != null && mat.getDimCount() > 1) {

            String baseName = DataPackageResources.class.getCanonicalName();
            ResourceBundle resources = ResourceBundle.getBundle(baseName);

            int seriesCount = mat.getDim(1);
            if (seriesCount > 0) {

                // set up the series names...
                if (series != null) {
                    
                    if (series.size() != seriesCount) {
                        throw new IllegalArgumentException(
                                "DefaultCategoryDataset: the number of "
                                        + "series names does not match the number of series in the data.");
                    }

                    //since jfreechart 1.0.13 doesn't work anymore
                    //because of getRowKey that return toString
                    //so indexOf(String) in a list of entities always return -1
                    //this.seriesNames = seriesNames;
                    seriesNames = new ArrayList<String>(seriesCount);
                    for (int i = 0; i < seriesCount; i++) {
                        Object dim = mat.getSemantic(1).get(i);
                        seriesNames.add(String.valueOf(dim)); // must match getRowKey()
                    }
                } else {
                    String prefix = resources
                            .getString("series.default-prefix") + " ";
                    this.seriesNames = this.generateNames(seriesCount, prefix);
                }

                // set up the category names...
                int categoryCount = mat.getDim(0);
                if (categories != null) {
                    if (categories.size() != categoryCount) {
                        throw new IllegalArgumentException(
                                "DefaultCategoryDataset: the number of "
                                        + "categories does not match the number of categories in the data.");
                    }

                    //since jfreechart 1.0.13 doesn't work anymore
                    //because of getRowKey that return toString
                    //so indexOf(String) in a list of entities always return -1
                    //this.categoriesNames = categories;
                    categoriesNames = new ArrayList<String>(categoryCount);
                    for (int i = 0; i < categoryCount; i++) {
                        Object dim = mat.getSemantic(0).get(i);
                        categoriesNames.add(dim.toString()); // must match getRowKey()
                    }
                } else {
                    String prefix2 = resources
                            .getString("categories.default-prefix") + " ";
                    categoriesNames = this.generateNames(categoryCount, prefix2);
                }
            } else {
                this.seriesNames = null;
                this.categoriesNames = null;
            }
        } else {
            this.seriesNames = null;
            this.categoriesNames = null;
        }
    }

    /**
     * Generates an array of names, by appending a space plus an integer (starting with 1)
     * to the supplied prefix string.
     * 
     * @param count number of names required.
     * @param prefix name prefix.
     */
    protected List<String> generateNames(int count, String prefix) {
        List<String> result = new ArrayList<String>(count);
        for (int i = 0; i < count; i++) {
            String name = prefix + (i + 1);
            result.add(name);
        }
        return result;
    }

    /*
     * @see org.jfree.data.general.AbstractSeriesDataset#getSeriesCount()
     */
    @Override
    public int getSeriesCount() {
        int result = 0;
        if (mat != null && mat.getDimCount() > 1) {
            result = mat.getDim(1);
        }
        return result;
    }

    /*
     * @see org.jfree.data.general.AbstractSeriesDataset#getSeriesKey(int)
     */
    @Override
    public Comparable getSeriesKey(int series) {
        return seriesNames.get(series).toString();
    }

    /*
     * @see org.jfree.data.KeyedValues2D#getColumnIndex(java.lang.Comparable)
     */
    @Override
    public int getColumnIndex(Comparable key) {
        return categoriesNames.indexOf(key);
    }

    /*
     * @see org.jfree.data.KeyedValues2D#getColumnKey(int)
     */
    @Override
    public Comparable getColumnKey(int column) {
        return categoriesNames.get(column).toString();
    }

    /*
     * @see org.jfree.data.KeyedValues2D#getColumnKeys()
     */
    @Override
    public List getColumnKeys() {

        // the CategoryDataset interface expects a list of categories, but we've stored them in
        // an array...
        if (categoriesNames == null) {
            return Collections.EMPTY_LIST;
        } else {
            return Collections.unmodifiableList(categoriesNames);
        }
    }

    /*
     * @see org.jfree.data.KeyedValues2D#getRowIndex(java.lang.Comparable)
     */
    @Override
    public int getRowIndex(Comparable key) {
        return seriesNames.indexOf(key);
    }

    /*
     * @see org.jfree.data.KeyedValues2D#getRowKey(int)
     */
    @Override
    public Comparable getRowKey(int row) {
        return seriesNames.get(row).toString();
    }

    /*
     * @see org.jfree.data.KeyedValues2D#getRowKeys()
     */
    @Override
    public List getRowKeys() {
        return seriesNames;
    }

    /*
     * @see org.jfree.data.KeyedValues2D#getValue(java.lang.Comparable, java.lang.Comparable)
     */
    @Override
    public Number getValue(Comparable rowKey, Comparable columnKey) {
        return new Double(mat.getValue(columnKey, rowKey));
    }

    /*
     * @see org.jfree.data.Values2D#getColumnCount()
     */
    @Override
    public int getColumnCount() {
        return categoriesNames.size();
    }

    /*
     * @see org.jfree.data.Values2D#getRowCount()
     */
    @Override
    public int getRowCount() {
        return seriesNames.size();
    }

    /*
     * @see org.jfree.data.Values2D#getValue(int, int)
     */
    @Override
    public Number getValue(int row, int column) {
        Number result = new Double(mat.getValue(column, row));
        return result;
    }
} // GraphMatrixNDDataset
