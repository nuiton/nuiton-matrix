/*
 * #%L
 * Nuiton Matrix :: GUI
 * %%
 * Copyright (C) 2011 - 2020 Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.math.matrix.viewer;

import org.nuiton.math.matrix.MatrixND;

import javax.swing.Icon;
import java.util.List;

/**
 * Action de regroupement des valeurs d'une dimension, pour faire des sommes...
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public interface MatrixDimensionAction {

    /**
     * Action icon.
     * 
     * @return action icon
     */
    Icon getIcon();
    
    /**
     * Action selected icon.
     * 
     * @return action selected icon
     */
    Icon getSelectedIcon();

    /**
     * Action button description.
     */
    String getDescription();

    /**
     * Return if an action can be displayed to user. User can them select this action to apply action.
     *
     * @param dim dimension index
     * @param semantic semantic
     * @return can apply
     */
    boolean canApply(int dim, List<?> semantic);

    /**
     * Apply matrix action.
     *
     * @param matrix matrix
     * @param dim dimension index
     * @return modified matrix
     */
    MatrixND apply(MatrixND matrix, int dim);
}
