/*
 * #%L
 * Nuiton Matrix :: GUI
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.math.matrix.gui;

import java.util.Arrays;
import java.util.List;

import org.nuiton.math.matrix.AbstractMatrixTest;
import org.nuiton.math.matrix.MatrixFactory;
import org.nuiton.math.matrix.MatrixND;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * MatrixTableModelND.
 *
 * Created: 21 mars 2006 19:05:06
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class MatrixTableModelTest extends AbstractMatrixTest {

    protected MatrixTableModelND model = null;
    protected MatrixND mat = null;

    /*
     * @see TestCase#setUp()
     */
    @Before
    public void setUp() throws Exception {
        List<String> dim0 = Arrays.asList(new String[] { "dim0-0", "dim0-1", "dim0-2",
                "dim0-3" });
        List<String> dim1 = Arrays.asList(new String[] { "dim1-0", "dim1-1" });
        List<String> dim2 = Arrays.asList(new String[] { "dim2-0", "dim2-1" });
        List<String> dim3 = Arrays.asList(new String[] { "dim3-0", "dim3-1", "dim3-2",
                "dim3-3" });
        List<String> dim4 = Arrays
                .asList(new String[] { "dim4-0", "dim4-1", "dim4-2" });
        mat = MatrixFactory.getInstance().create("mat",
                new List<?>[] { dim0, dim1, dim2, dim3, dim4 },
                new String[] { "dim0", "dim1", "dim2", "dim3", "dim4" });
        model = new MatrixTableModelND(mat);
    }

    @Test
    public void testSetMatrix() {
        Assert.assertEquals(2, model.addRow);
        Assert.assertEquals(3, model.addCol);
        int[] val = model.multRowCol;
        Assert.assertEquals(true, Arrays.equals(new int[] { 6, 4, 3, 1, 1 },
                val));
    }

    /*
     * Test method for 'org.nuiton.math.matrix.gui.MatrixTableModelND.isCellEditable(int, int)'
     */
    @Test
    public void testIsCellEditable() {
        Assert.assertEquals(false, model.isCellEditable(model.addRow - 1,
                model.addCol - 1));
        Assert.assertEquals(true, model.isCellEditable(model.addRow,
                model.addCol));
        Assert.assertEquals(true, model.isCellEditable(model.addRow + 1,
                model.addCol + 1));
    }

    /*
     * Test method for 'org.nuiton.math.matrix.gui.MatrixTableModelND.tableToMatrix(int, int)'
     */
    @Test
    public void testTableToMatrix() {
        int[] val = model.tableToMatrix(5, 4);
        Assert.assertEquals(true, Arrays.equals(new int[] { 0, 1, 1, 0, 2 },
                val));
        val = model.tableToMatrix(0, 0);
        Assert.assertEquals(true, Arrays.equals(new int[] { 0, 0, 0, 0, 0 },
                val));
        val = model.tableToMatrix(23, 5);
        Assert.assertEquals(true, Arrays.equals(new int[] { 3, 1, 1, 1, 2 },
                val));
    }

    /*
     * Test method for 'org.nuiton.math.matrix.gui.MatrixTableModelND.getValue(int, int)'
     */
    @Test
    public void testGetValue() {
        Assert.assertEquals("dim0-0", model.getValue(2, 0));
        Assert.assertEquals("dim0-0", model.getValue(3, 0));
        Assert.assertEquals("dim0-0", model.getValue(4, 0));
        Assert.assertEquals("dim0-0", model.getValue(7, 0));
        Assert.assertEquals("dim0-1", model.getValue(8, 0));

        Assert.assertEquals("dim2-0", model.getValue(2, 1));
        Assert.assertEquals("dim2-0", model.getValue(3, 1));
        Assert.assertEquals("dim2-1", model.getValue(5, 1));

        Assert.assertEquals("dim4-0", model.getValue(2, 2));
        Assert.assertEquals("dim4-1", model.getValue(3, 2));
        Assert.assertEquals("dim4-2", model.getValue(4, 2));
        Assert.assertEquals("dim4-0", model.getValue(5, 2));

        Assert.assertEquals("dim1-0", model.getValue(0, 3));

        Assert.assertEquals(0.0, model.getValue(model.addRow, model.addCol));
        Assert.assertEquals(0.0, model.getValue(23 + model.addRow,
                5 + model.addCol));
    }

    /*
     * Test method for
     * 'org.nuiton.math.matrix.gui.MatrixTableModelND.getColumnName(int)'
     */
    @Test
    public void testGetColumnNameInt() {

    }

    /*
     * Test method for
     * 'org.nuiton.math.matrix.gui.MatrixTableModelND.getRowCount()'
     */
    @Test
    public void testGetRowCount() {

    }

    /*
     * Test method for
     * 'org.nuiton.math.matrix.gui.MatrixTableModelND.getColumnCount()'
     */
    @Test
    public void testGetColumnCount() {

    }

    /*
     * Test method for
     * 'org.nuiton.math.matrix.gui.MatrixTableModelND.getValueAt(int, int)'
     */
    @Test
    public void testGetValueAt() {

    }

    /*
     * Test method for
     * 'org.nuiton.math.matrix.gui.MatrixTableModelND.setValueAt(Object, int,
     * int)'
     */
    @Test
    public void testSetValueAtObjectIntInt() {

    }

}
