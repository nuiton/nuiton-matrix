/*
 * #%L
 * Nuiton Matrix :: GUI
 * %%
 * Copyright (C) 2011 Codelutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.math.matrix.gui;

import java.awt.Component;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;

import org.junit.Assert;
import org.junit.Test;
import org.nuiton.math.matrix.AbstractMatrixTest;
import org.nuiton.math.matrix.MatrixFactory;
import org.nuiton.math.matrix.MatrixHelper;
import org.nuiton.math.matrix.MatrixND;

/**
 * Test to diplay matrix in MatrixPanelEditor using differents model.
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class MatrixPanelEditorTest extends AbstractMatrixTest {

    protected MatrixND getMatrixTest(int dimCount) {
        List<?>[] dims = new List<?>[dimCount];

        List<Integer> years = new ArrayList<Integer>();
        years.add(1999);
        years.add(2000);
        years.add(2001);
        years.add(2002);
        years.add(2003);
        years.add(2004);
        years.add(2005);
        dims[0] = years;
        
        if (dimCount >= 2) {
            List<String> cities = new ArrayList<String>();
            cities.add("Nantes");
            cities.add("Paris");
            cities.add("Lyon");
            cities.add("Lille");
            cities.add("Toulouse");
            cities.add("Marseille");
            dims[1] = cities;
        }
        
        if (dimCount >= 3) {
            List<String> sectors = new ArrayList<String>();
            sectors.add("Informatique");
            sectors.add("Administration");
            sectors.add("Livraison");
            sectors.add("Achat");
            dims[2] = sectors;
        }
        
        if (dimCount >= 4) {
            List<String> persons = new ArrayList<String>();
            persons.add("Bob");
            persons.add("Joe");
            persons.add("Louis");
            persons.add("Jean");
            dims[3] = persons;
        }
        
        MatrixND matrix = MatrixFactory.getInstance().create(dims);
        matrix.setName("test matrix");
        MatrixHelper.fill(matrix, Math.random()*10000);

        return matrix;
    }

    protected void show(Component comp) {
        JFrame frame = new JFrame();
        frame.add(comp);
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);

        try {
            Thread.sleep(000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testModelNDDim2() {
        MatrixND matrix = getMatrixTest(2);
        matrix.setValue(2, 1, 1.1d);

        MatrixPanelEditor editor = new MatrixPanelEditor();
        editor.setMatrix(matrix);
        show(editor);

        Assert.assertEquals(1.1d, editor.getTable().getModel().getValueAt(3, 2));
    }

    @Test
    public void testModelNDDim3() {
        MatrixND matrix = getMatrixTest(3);
        matrix.setValue(1, 2, 3, 4.4d);

        MatrixPanelEditor editor = new MatrixPanelEditor();
        editor.setMatrix(matrix);
        show(editor);

        Assert.assertEquals(4.4d, editor.getTable().getModel().getValueAt(8, 4));
    }

    /**
     * En 2.2.2 mettre la matrice en construteur ne fonctionnait pas.
     */
    @Test
    public void testModelNDDim3MInConstructor() {
        MatrixND matrix = getMatrixTest(3);
        MatrixPanelEditor editor = new MatrixPanelEditor(matrix, false);
        Assert.assertTrue(editor.getTable().getModel() instanceof MatrixTableModel);
        show(editor);
    }
    
    @Test
    public void testModelNDDim4() {
        MatrixND matrix = getMatrixTest(4);
        MatrixPanelEditor editor = new MatrixPanelEditor();
        editor.setMatrix(matrix);
        show(editor);
    }

    @Test
    public void testModelLinearNDDim2() {
        MatrixND matrix = getMatrixTest(2);
        MatrixPanelEditor editor = new MatrixPanelEditor();
        editor.setLinearModel(true);
        editor.setMatrix(matrix);
        show(editor);
    }

    @Test
    public void testModelLinearDim3() {
        MatrixND matrix = getMatrixTest(3);
        MatrixPanelEditor editor = new MatrixPanelEditor();
        editor.setLinearModel(true);
        editor.setLinearModelShowDefault(true);
        editor.setMatrix(matrix);
        show(editor);
    }

    @Test
    public void testModelLinearDim4() {
        MatrixND matrix = getMatrixTest(4);
        matrix.setValue(0, 0, 1, 3, 5.5d);

        MatrixPanelEditor editor = new MatrixPanelEditor();
        editor.setLinearModel(true);
        editor.setMatrix(matrix);
        show(editor);

        Assert.assertEquals(1999, editor.getTable().getModel().getValueAt(7, 0));
        Assert.assertEquals("Nantes", editor.getTable().getModel().getValueAt(7, 1));
        Assert.assertEquals("Administration", editor.getTable().getModel().getValueAt(7, 2));
        Assert.assertEquals("Jean", editor.getTable().getModel().getValueAt(7, 3));
        Assert.assertEquals(5.5d, editor.getTable().getModel().getValueAt(7, 4));
    }
}
