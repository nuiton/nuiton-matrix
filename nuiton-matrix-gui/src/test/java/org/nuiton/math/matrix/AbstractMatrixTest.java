/*
 * #%L
 * Nuiton Matrix :: GUI
 * %%
 * Copyright (C) 2012 Codelutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.math.matrix;

import org.junit.Assert;
import org.junit.Assume;
import org.junit.BeforeClass;
import org.nuiton.i18n.I18n;
import org.nuiton.i18n.init.DefaultI18nInitializer;

import java.util.Locale;

/**
 * Abstract code for all test.
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class AbstractMatrixTest {

    @BeforeClass
    public static void disableTests() {
        Assume.assumeTrue(!java.awt.GraphicsEnvironment.isHeadless());
    }

    @BeforeClass
    public static void initI18n() {

        I18n.init(new DefaultI18nInitializer("nuiton-matrix-gui", null, "i18n/"), Locale.FRANCE);

        // Avant d'aller plus loin, on s'assure que I18n est bien chargé.
        Assert.assertEquals("Importer/Exporter fichier CSV", I18n.t("nuitonmatrix.menu.csv"));

    }

}
