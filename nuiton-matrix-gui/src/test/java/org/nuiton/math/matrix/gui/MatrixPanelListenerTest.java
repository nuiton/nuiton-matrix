/*
 * #%L
 * Nuiton Matrix :: GUI
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.math.matrix.gui;

import java.beans.BeanInfo;
import java.beans.EventSetDescriptor;
import java.beans.Introspector;

import org.junit.Assert;
import org.junit.Test;
import org.nuiton.math.matrix.AbstractMatrixTest;

/**
 * A test to verify that {@link MatrixPanelListener} is truly a JavaBeans
 * listener implementation in editors.
 * 
 * @author chemit
 * @since 2.0.0
 */
public class MatrixPanelListenerTest extends AbstractMatrixTest {

    public static final String MATRIX_PANEL_LISTENER_NAME = "matrixPanel";

    /**
     * Test if editors are truly JavaBeans listeners implementations
     * @throws Exception
     */
    @Test
    public void testMatrixPanelEditor() throws Exception {
        verifyMatrixPanelListener(MatrixPanelEditor.class);
    }

    protected void verifyMatrixPanelListener(Class<?> klass) throws Exception {

        BeanInfo beanInfo = Introspector.getBeanInfo(klass);

        for (EventSetDescriptor e : beanInfo.getEventSetDescriptors()) {
            if (MATRIX_PANEL_LISTENER_NAME.equals(e.getName())) {
                // the correct listener descriptor was found :)
                return;
            }
        }
        Assert.fail("could not find " + MATRIX_PANEL_LISTENER_NAME + " event listener descriptor");
    }
}
