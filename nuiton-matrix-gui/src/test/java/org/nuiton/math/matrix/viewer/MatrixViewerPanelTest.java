/*
 * #%L
 * Nuiton Matrix :: GUI
 * %%
 * Copyright (C) 2010 - 2020 Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.math.matrix.viewer;

import java.util.ArrayList;
import java.util.List;

import javax.swing.Icon;
import javax.swing.JFrame;

import org.junit.Test;
import org.nuiton.math.matrix.AbstractMatrixTest;
import org.nuiton.math.matrix.MatrixFactory;
import org.nuiton.math.matrix.MatrixHelper;
import org.nuiton.math.matrix.MatrixND;
import org.nuiton.math.matrix.MatrixProvider;
import org.nuiton.math.matrix.viewer.renderer.MatrixChartRenderer;
import org.nuiton.math.matrix.viewer.renderer.MatrixPanelRenderer;
import org.nuiton.util.Resource;

/**
 * Test for matrix viewer panel.
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class MatrixViewerPanelTest extends AbstractMatrixTest {
    
    protected MatrixND getTestMatrix(boolean proxy) {
        List<Integer> years = new ArrayList<Integer>();
        years.add(1999);
        years.add(2000);
        years.add(2001);
        years.add(2002);
        years.add(2003);
        years.add(2004);
        years.add(2005);
        
        List<String> cities = new ArrayList<String>();
        cities.add("Nantes");
        cities.add("Paris");
        cities.add("Lyon");
        cities.add("Lille");
        cities.add("Toulouse");
        cities.add("Marseille");
        
        MatrixND matrix = null;
        if (proxy) {
            matrix = MatrixFactory.getInstance().createProxy("test matrix", new List<?>[]{years, cities}, new String[]{"Years", "Cities"}, new MatrixProvider() {
                @Override
                public void fillValues(MatrixND matrix) {
                    MatrixHelper.fill(matrix, Math.random()*10000);
                    matrix.setValue(0, 0, Math.random()*10000);
                }
            });
        }
        else {
            matrix = MatrixFactory.getInstance().create("test matrix", new List<?>[]{years, cities}, new String[]{"Years", "Cities"});
            MatrixHelper.fill(matrix, Math.random()*10000);
            matrix.setValue(1, 0, Math.random()*10000);
            matrix.setValue(0, 4, Math.random()*10000);
        }
        return matrix;
    }

    /**
     * Test d'un affichage simple, par defaut.
     */
    @Test
    public void testViewerPanel() {
        JFrame frame = new JFrame();

        MatrixViewerPanel panel = new MatrixViewerPanel();
        MatrixND testMatrix = getTestMatrix(false);
        panel.setMatrix(testMatrix);
        panel.addMatrixRenderer(new MatrixChartRenderer());
        MatrixPanelRenderer panelRenderer = new MatrixPanelRenderer();
        panelRenderer.getEditor().setDisplayOptions(true);
        panel.addMatrixRenderer(panelRenderer);
        frame.add(panel);

        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);

        /*try {
            Thread.sleep(60000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }*/
    }
    
    /**
     * Test d'un affichage simple, par defaut (cas avec un proxy).
     */
    @Test
    public void testViewerPanelProxy() {
        JFrame frame = new JFrame();

        MatrixViewerPanel panel = new MatrixViewerPanel();
        MatrixND testMatrix = getTestMatrix(true);
        panel.addMatrixRenderer(new MatrixPanelRenderer());
        panel.addMatrixRenderer(new MatrixChartRenderer());
        panel.addMatrixDimentionAction(new MatrixDimensionAction() {
            @Override
            public Icon getSelectedIcon() {
                return null;
            }
            @Override
            public Icon getIcon() {
                return Resource.getIcon("/icons/fatcow/table.png");
            }
            @Override
            public boolean canApply(int index, List<?> semantic) {
                return index == 1;
            }
            @Override
            public MatrixND apply(MatrixND matrix, int dim) {
                return matrix.sumOverDim(dim, 2);
            }
            @Override
            public String getDescription() {
                return null;
            }
        });
        panel.addMatrixDimentionAction(new MatrixDimensionAction() {
            @Override
            public Icon getSelectedIcon() {
                return null;
            }
            @Override
            public Icon getIcon() {
                return Resource.getIcon("/icons/fatcow/chart_curve.png");
            }
            @Override
            public boolean canApply(int index, List<?> semantic) {
                return semantic.get(0) instanceof Integer;
            }
            @Override
            public MatrixND apply(MatrixND matrix, int dim) {
                return matrix.sumOverDim(dim, 3);
            }
            @Override
            public String getDescription() {
                return null;
            }
        });
        panel.setMatrix(testMatrix);
        frame.add(panel);

        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);

        /*try {
            Thread.sleep(60000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }*/
    }
}
